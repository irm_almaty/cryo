package com.cryobank.postgres.dao;

import java.util.List;

import com.cryobank.postgres.domain.News;

public interface NewsDao {

	public int createNews(News news);

	public News updateNews(News news);

	public void deleteNews(int id);

	public List<News> getAllNews();
	
	public List<News> getLastNews();

	public News getNews(int id);

}
