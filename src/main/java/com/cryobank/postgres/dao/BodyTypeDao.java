package com.cryobank.postgres.dao;

import java.util.List;

import com.cryobank.postgres.domain.BodyType;

public interface BodyTypeDao {
	
	public List<BodyType> getAllBodyTypes();

}
