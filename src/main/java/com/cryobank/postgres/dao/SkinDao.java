package com.cryobank.postgres.dao;

import java.util.List;

import com.cryobank.postgres.domain.Skin;

public interface SkinDao {
	
	public List<Skin> getAllSkin();

}
