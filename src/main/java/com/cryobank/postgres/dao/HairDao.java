
package com.cryobank.postgres.dao;

import java.util.List;

import com.cryobank.postgres.domain.Hair;

public interface HairDao {
	
	public List<Hair> getAllHairs();

}
