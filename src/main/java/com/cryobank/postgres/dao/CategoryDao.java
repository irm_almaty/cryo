package com.cryobank.postgres.dao;

import java.util.List;

import com.cryobank.postgres.domain.Category;

public interface CategoryDao {
	
	public List<Category> getAllCategory();

}
