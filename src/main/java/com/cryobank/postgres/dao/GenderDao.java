package com.cryobank.postgres.dao;

import java.util.List;

import com.cryobank.postgres.domain.Gender;

public interface GenderDao {

	public List<Gender> getAllGender();

}
