package com.cryobank.postgres.dao;

import java.util.List;

import com.cryobank.postgres.domain.PatientPostgre;
import com.cryobank.postgres.domain.PatientStatus;

public interface PatientPostgreDao {

	public int createPatientPostgre(PatientPostgre patientPostgre);

	public PatientPostgre updatePatientPostgre(PatientPostgre patientPostgre);

	public PatientPostgre getPatientPostgre(int id);

	public List<PatientPostgre> getAllPatientPostgre();

	public List<PatientPostgre> getAllPatientPostgre(PatientStatus status);

	public List<PatientPostgre> getPatientPostgreByEmail(String email);

	PatientPostgre getPatientPostgreByIuserId(Integer iuserId);

}
