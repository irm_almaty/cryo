package com.cryobank.postgres.dao;

import java.util.List;

import com.cryobank.postgres.domain.Photo;

public interface PhotoDao {

	public void savePhoto(Photo photo);

	public int createPhoto(Photo photo);

	public Photo updatePhoto(Photo photo);

	public void deletePhotoById(int id);

	public Photo getPhoto(int id);

	public List<Photo> getAllPhoto();

}
