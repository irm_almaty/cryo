package com.cryobank.postgres.dao;

import java.util.List;
import com.cryobank.postgres.domain.Voice;

public interface VoiceDao {

	public void savePhoto(Voice voice);

	public int createVoice(Voice voice);

	public Voice updatePhoto(Voice voice);

	public void deleteVoiceById(int id);

	public Voice getVoice(int id);

	public List<Voice> getAllVoice();

}
