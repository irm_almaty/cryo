package com.cryobank.postgres.dao;

import java.util.List;

import com.cryobank.postgres.domain.OrderStatus;



public interface OrderStatusDao {
	
	public List<OrderStatus> getAllOrderStatus();

}
