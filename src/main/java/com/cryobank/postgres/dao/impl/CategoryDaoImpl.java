package com.cryobank.postgres.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cryobank.postgres.dao.CategoryDao;
import com.cryobank.postgres.domain.Article;
import com.cryobank.postgres.domain.Category;
import com.cryobank.util.HibernateUtil;

@Repository
public class CategoryDaoImpl implements CategoryDao {

	@Autowired
	private HibernateUtil hibernateUtil;
	
	@Override
	public List<Category> getAllCategory() {
		
	return hibernateUtil.fetchAll(Category.class);
	}

	
}
