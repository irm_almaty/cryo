package com.cryobank.postgres.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cryobank.postgres.dao.PriceDao;
import com.cryobank.postgres.domain.Price;
import com.cryobank.util.HibernateUtil;


@Repository
public class PriceDaoImpl implements PriceDao {

	
	@Autowired
	HibernateUtil hibernateUtil;
	
	
	@Override
	public List<Price> getAllPrice() {
		
		return hibernateUtil.fetchAll(Price.class);
	
	}

}
