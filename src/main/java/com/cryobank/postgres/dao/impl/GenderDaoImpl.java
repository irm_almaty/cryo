package com.cryobank.postgres.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cryobank.postgres.dao.GenderDao;
import com.cryobank.postgres.domain.Gender;
import com.cryobank.util.HibernateUtil;

@Repository
public class GenderDaoImpl implements GenderDao {

	@Autowired
	private HibernateUtil hibernateUtil;

	@Override
	public List<Gender> getAllGender() {

		return hibernateUtil.fetchAll(Gender.class);
	}

}
