package com.cryobank.postgres.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cryobank.postgres.dao.PhotoDao;
import com.cryobank.postgres.domain.Photo;
import com.cryobank.util.HibernateUtil;

@Repository
public class PhotoDaoImpl implements PhotoDao {

	@Autowired
	public SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	private Session getSession() {
		Session sess = getSessionFactory().getCurrentSession();
		if (sess == null) {
			sess = getSessionFactory().openSession();
		}
		return sess;
	}
	
	private SessionFactory getSessionFactory() {
		return sessionFactory;
	}


	@Autowired
	private HibernateUtil hibernateUtil;

	@Override
	public void savePhoto(Photo photo) {
		getSession().merge(photo);

	}

	@Override
	public int createPhoto(Photo photo) {
		return (int) hibernateUtil.create(photo);
	}

	@Override
	public Photo updatePhoto(Photo photo) {
		return hibernateUtil.update(photo);
	}

	@Override
	public void deletePhotoById(int id) {
		Photo photo = getPhoto(id);

		if (null != photo) {
			getSession().delete(photo);
		}


	}

	@Override
	public Photo getPhoto(int id) {
		return (Photo) getSession().get(Photo.class, id);
	}

	@Override
	public List<Photo> getAllPhoto() {
		return hibernateUtil.fetchAllSQL("select * from public.photo where donor_id=1", Photo.class);
	}

}
