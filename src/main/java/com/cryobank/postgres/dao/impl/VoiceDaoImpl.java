package com.cryobank.postgres.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cryobank.postgres.dao.VoiceDao;
import com.cryobank.postgres.domain.Voice;
import com.cryobank.util.HibernateUtil;


@Repository
public class VoiceDaoImpl implements VoiceDao{
	
	
	@Autowired
	public SessionFactory sessionFactory;
	

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	private Session getSession() {
		Session sess = getSessionFactory().getCurrentSession();
		if (sess == null) {
			sess = getSessionFactory().openSession();
		}
		return sess;
	}
	
	private SessionFactory getSessionFactory() {
		return sessionFactory;
	}


	@Autowired
	private HibernateUtil hibernateUtil;

	@Override
	public void savePhoto(Voice voice) {
		getSession().merge(voice);
		
	}

	@Override
	public int createVoice(Voice voice) {
		return (int) hibernateUtil.create(voice);
	}

	@Override
	public Voice updatePhoto(Voice voice) {
		return hibernateUtil.update(voice);
	}

	@Override
	public void deleteVoiceById(int id) {
		Voice voice = getVoice(id);

		if (null != voice) {
			getSession().delete(voice);
		}

	}

	@Override
	public Voice getVoice(int id) {
		return (Voice) getSession().get(Voice.class, id);
	}

	@Override
	public List<Voice> getAllVoice() {
		return hibernateUtil.fetchAllSQL("select * from public.photo where donor_id=1", Voice.class);
	}

	

}
