package com.cryobank.postgres.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cryobank.postgres.dao.SubscriptDao;
import com.cryobank.postgres.domain.Subscript;
import com.cryobank.util.HibernateUtil;

@Repository
public class SubscriptDaoImpl implements SubscriptDao {

	@Autowired
	private HibernateUtil hibernateUtil;

	@Autowired
	public SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	private Session getSession() {
		Session sess = getSessionFactory().getCurrentSession();
		if (sess == null) {
			sess = getSessionFactory().openSession();
		}
		return sess;
	}

	private SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	
		
	
	@Override
	public Subscript getSubscriptById(int id) {
		
		return hibernateUtil.fetchById(id, Subscript.class);
		
	}

	@Override
	public int createSubscript(Subscript subscript) {
		
		return (int)hibernateUtil.create(subscript);
	}

	@Override
	public List<Subscript> getAllSubscript() {
		
		return hibernateUtil.fetchAll(Subscript.class);
	}

	@Override
	public List<Subscript> getSubscriptEmail(String userEmail) {
		//return getSession().createSQLQuery("select donor_id, donor_name, donor_sex from public.donor").addEntity(User.class);
		
		List<Subscript> subscriptList= getSession().createSQLQuery("select * from public.subscript where email ='"+ userEmail +"'").addEntity(Subscript.class).list();
		
		/*select * from public.user1 where user_name='Shah';*/
		
		Subscript subscript=subscriptList.get(0);
		
		
		if(subscript!=null){
			return subscriptList;
		}
		
		return null;
	}
	
}