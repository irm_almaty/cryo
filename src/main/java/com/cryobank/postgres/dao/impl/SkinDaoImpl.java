package com.cryobank.postgres.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.cryobank.postgres.dao.SkinDao;
import com.cryobank.postgres.domain.Skin;
import com.cryobank.util.HibernateUtil;

public class SkinDaoImpl implements SkinDao {

	@Autowired
	private HibernateUtil hibernateUtil;

	@Override
	public List<Skin> getAllSkin() {
		return hibernateUtil.fetchAll(Skin.class);
	}

}
