package com.cryobank.postgres.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cryobank.medialog.domain.CountValue;
import com.cryobank.postgres.dao.PatientPostgreDao;
import com.cryobank.postgres.domain.PatientPostgre;
import com.cryobank.postgres.domain.PatientStatus;
import com.cryobank.util.HibernateUtil;

@Repository
public class PatientPostgreDaoImpl implements PatientPostgreDao {

	@Autowired
	HibernateUtil hibernateUtil;

	@Override
	public int createPatientPostgre(PatientPostgre patientPostgre) {

		return (int) hibernateUtil.create(patientPostgre);
	}

	@Override
	public PatientPostgre updatePatientPostgre(PatientPostgre patientPostgre) {
		return hibernateUtil.update(patientPostgre);
	}

	@Override
	public PatientPostgre getPatientPostgre(int id) {

		return hibernateUtil.fetchById(id, PatientPostgre.class);
	}

	@Override
	public List<PatientPostgre> getAllPatientPostgre() {

		return hibernateUtil.fetchAll(PatientPostgre.class);
	}

	@Override
	public List<PatientPostgre> getPatientPostgreByEmail(String email) {
		String query = "select * from patient where email like '%" + email + "%'";
		return hibernateUtil.fetchAllSQL(query, PatientPostgre.class);
	}

	@Override
	public PatientPostgre getPatientPostgreByIuserId(Integer iuserId) {
		String query = "select * from patient where iuserid = " + iuserId;
		List<PatientPostgre> list = hibernateUtil.fetchAllSQL(query, PatientPostgre.class);
		if (list != null && !list.isEmpty()) {
			PatientPostgre patient = list.get(0);
			return patient;
		}
		return null;
	}

	@Override
	public List<PatientPostgre> getAllPatientPostgre(PatientStatus status) {
		StringBuilder query = new StringBuilder();
		query.append("select * from patient where patient_status = ");
		query.append("'" + status.toString() + "'");
		query.append(" order by createdate DESC");
		return hibernateUtil.fetchAllSQL(query.toString(), PatientPostgre.class);
	}

	

}
