package com.cryobank.postgres.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cryobank.postgres.dao.DonorManPostgreDao;
import com.cryobank.postgres.domain.DonorManPostgre;
import com.cryobank.postgres.domain.Order;
import com.cryobank.util.HibernateUtil;

@Repository
public class DonorManPostgreDaoImpl implements DonorManPostgreDao {

	@Autowired
	public SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	private Session getSession() {
		Session sess = getSessionFactory().getCurrentSession();
		if (sess == null) {
			sess = getSessionFactory().openSession();
		}
		return sess;
	}

	private SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	@Autowired
	private HibernateUtil hibernateUtil;

	@Override
	public void saveDonor(DonorManPostgre donor) {

		getSession().merge(donor);

	}

	@Override
	public DonorManPostgre getDonor(int id) {
		return (DonorManPostgre) getSession().get(DonorManPostgre.class, id);
	}

	@Override
	public void deleteDonorById(int id) {

		DonorManPostgre donor = getDonor(id);

		if (null != donor) {
			getSession().delete(donor);
		}

	}

	@Override
	public List<DonorManPostgre> getAllDonors() {
		return getSession().createCriteria(DonorManPostgre.class).list();

	}
	
	
	
	
	
	

	@Override
	public int createDonor(DonorManPostgre donor) {

		return (int) hibernateUtil.create(donor);
	}
	

	@Override
	public DonorManPostgre updateDonor(DonorManPostgre donor) {

		return hibernateUtil.update(donor);
	}

	
	@Override
	public List<DonorManPostgre> getAllMaleDonors() {

		return hibernateUtil.fetchAllSQL("select * from public.donor where gender_id=1", DonorManPostgre.class);
	}

	@Override
	public List<DonorManPostgre> getAllFemaleDonors() {
		
		return hibernateUtil.fetchAllSQL("select * from public.donor where gender_id=2", DonorManPostgre.class);
	}

	@Override
	public List<Order> searchFromAllDonors(String queryDonor) {
	
		String query="select * from public.donor "+queryDonor;
		
		return hibernateUtil.fetchAllSQL(query, DonorManPostgre.class);
		
		
	}

	
	
	
	
	
	/*
	 * @Override public List<Donor> getAllMaleDonors() { return
	 * getSession().createSQLQuery(
	 * "select * from public.donor where gender_id=1")
	 * .addEntity(Donor.class).list(); }
	 */

}
