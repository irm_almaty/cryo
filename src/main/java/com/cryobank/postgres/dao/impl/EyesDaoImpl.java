package com.cryobank.postgres.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cryobank.postgres.dao.EyesDao;
import com.cryobank.postgres.domain.Eyes;
import com.cryobank.util.HibernateUtil;

@Repository
public class EyesDaoImpl implements EyesDao {

	@Autowired
	private HibernateUtil hibernateUtil;

	@Override
	public List<Eyes> getAllEyes() {
		
		return hibernateUtil.fetchAll(Eyes.class);
	}
	

}
