package com.cryobank.postgres.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cryobank.postgres.dao.OrderDao;
import com.cryobank.postgres.domain.Order;
import com.cryobank.postgres.domain.OrderStatus;
import com.cryobank.postgres.domain.QueryValue;
import com.cryobank.util.Constants;
import com.cryobank.util.HibernateUtil;

@Repository
public class OrderDaoImpl implements OrderDao {

	@Autowired
	private HibernateUtil hibernateUtil;

	@Override
	public List<Order> getAllOrders() {
		return hibernateUtil.getAll(Order.class);
	}

	@Override
	public int createOrder(Order order) {
		return (int) hibernateUtil.create(order);
	}

	@Override
	public List<Order> getAllOrdersByUser(int iuserId) {
		String queryOrder = "select * from public.order_donor where iuser_id=" + iuserId;
		return hibernateUtil.fetchAllSQL(queryOrder, Order.class);

	}

	@Override
	public List<Order> getAllOrdersByStatusNewOrder(int statusId) {
		String queryOrder = "select * from public.order_donor where order_status_id=" + statusId;
		return hibernateUtil.fetchAllSQL(queryOrder, Order.class);
	}

	@Override
	public List<Order> getAllOrdersByUserByStatus(int iuserId, int statusId) {
		String queryOrder = "select * from public.order_donor where order_status_id=" + statusId
				+ " and order_iuser_id=" + iuserId;
		return hibernateUtil.fetchAllSQL(queryOrder, Order.class);
	}

	@Override
	public List<Order> searchFromAllOrders(String querySh) {
		String queryOrder = "select * from public.order_donor " + querySh;
		return hibernateUtil.fetchAllSQL(queryOrder, Order.class);

	}

	@Override
	public Order getOrderById(int id) {
		return hibernateUtil.fetchById(id, Order.class);
	}

	@Override
	public Order updateOrder(Order order) {
		return hibernateUtil.update(order);
	}

	@Override
	public List<Order> getAllOrdersByIuserByStatus(int iuserId, String status) {
		StringBuilder query = new StringBuilder();
		query.append("select * from public.order_donor where iuser_id=");
		query.append(iuserId);
		query.append(" AND ");
		query.append("orderstatus =" + "'" + status + "'");
		query.append(" order by order_id DESC");
		return hibernateUtil.fetchAllSQL(query.toString(), Order.class);
	}

	@Override
	public List<Order> getAllOrdersByIuserByStatusByDonorId(int iuserId, String status, int donorId) {
		StringBuilder query = new StringBuilder();
		query.append("select * from public.order_donor where iuser_id=");
		query.append(iuserId);
		query.append(" AND ");
		query.append("orderstatus =" + "'" + status + "'");
		query.append(" AND ");
		query.append("donor_id =" + donorId);
		query.append(" order by order_id DESC ");
		return hibernateUtil.fetchAllSQL(query.toString(), Order.class);
	}

	@Override
	public List<Order> getAllOrdersByStatus(String status) {
		StringBuilder query = new StringBuilder();
		query.append("select * from public.order_donor where ");
		query.append("orderstatus =" + "'" + status + "'");
		query.append(" order by order_id DESC ");
		return hibernateUtil.fetchAllSQL(query.toString(), Order.class);
	}

	@Override
	public QueryValue getOrderCounts() {
		StringBuilder query = new StringBuilder();
		query.append("select");
		query.append("(select count(order_id) from order_donor where orderstatus='WAITS') as waits_value, ");
		query.append("(select count(order_id) from order_donor where orderstatus='CONSIDER') as consider_value, ");
		query.append("(select count(order_id) from order_donor where orderstatus='DOSSIER') as dossier_value, ");
		query.append("(select count(order_id) from order_donor where orderstatus='DISAGREE') as disagree_value, ");
		query.append("(select count(order_id) from order_donor where orderstatus='ACCEPTED') as accepted_value,");
		query.append("(select count(order_id) from order_donor where orderstatus='APPROVED') as approved_value,");
		query.append("(select count(order_id) from order_donor where orderstatus='REJECTED') as rejected_value,");
		query.append("(select count(order_id) from order_donor where orderstatus='CANCEL') as cancel_value,");
		query.append("(select count(order_id) from order_donor where orderstatus='ARCHIVED') as archived_value");
		return (QueryValue) hibernateUtil.getByValue(query.toString(), QueryValue.class);
	}

}
