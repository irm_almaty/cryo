package com.cryobank.postgres.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cryobank.postgres.dao.BloodTypeDao;
import com.cryobank.postgres.domain.BloodType;
import com.cryobank.util.HibernateUtil;

@Repository
public class BloodTypeDaoImpl implements BloodTypeDao{

	@Autowired
	private HibernateUtil hibernateUtil;

	@Override
	public List<BloodType> getAllBloodTypes() {
		
		return hibernateUtil.fetchAll(BloodType.class);
	}
	
	
	
}
