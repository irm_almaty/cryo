package com.cryobank.postgres.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cryobank.postgres.dao.ArticleDao;
import com.cryobank.postgres.domain.Article;
import com.cryobank.util.HibernateUtil;

@Repository
public class ArticleDaoImpl implements ArticleDao {

	@Autowired
	private HibernateUtil hibernateUtil;

	@Override
	public int createArticle(Article article) {
		return (int) hibernateUtil.create(article);
	}

	@Override
	public Article updateArticle(Article article) {

		return hibernateUtil.update(article);
	}

	@Override
	public void deleteArticle(int id) {
		Article article = new Article();
		article.setId(id);
		hibernateUtil.delete(article);

	}

	@Override
	public List<Article> getAllArticle() {
		return hibernateUtil.fetchAll(Article.class);
	}

	@Override
	public Article getArticle(int id) {
		return hibernateUtil.fetchById(id, Article.class);
	}

	@Override
	public List<Article> getLastArticle() {
		
		return hibernateUtil.fetchAllSQL("select * from article order by create_date DESC limit 3", Article.class);
	}

}
