package com.cryobank.postgres.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cryobank.postgres.dao.BloodTypeDao;
import com.cryobank.postgres.dao.BodyTypeDao;
import com.cryobank.postgres.domain.BloodType;
import com.cryobank.postgres.domain.BodyType;
import com.cryobank.util.HibernateUtil;

@Repository
public class BodyTypeDaoImpl implements BodyTypeDao {

	@Autowired
	private HibernateUtil hibernateUtil;

	@Override
	public List<BodyType> getAllBodyTypes() {
		
		return hibernateUtil.fetchAll(BodyType.class);
	}
	
	
	
	

}
