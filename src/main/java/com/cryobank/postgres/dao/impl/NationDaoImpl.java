package com.cryobank.postgres.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cryobank.postgres.dao.NationDao;
import com.cryobank.postgres.domain.Nation;
import com.cryobank.util.HibernateUtil;

@Repository
public class NationDaoImpl implements NationDao {

	@Autowired
	private HibernateUtil hibernateUtil;

	@Override
	public List<Nation> getAllNations() {
		 return hibernateUtil.fetchAll(Nation.class);
	}

}
