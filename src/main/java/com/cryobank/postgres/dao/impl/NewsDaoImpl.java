package com.cryobank.postgres.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cryobank.postgres.dao.NewsDao;
import com.cryobank.postgres.domain.News;
import com.cryobank.util.HibernateUtil;

@Repository
public class NewsDaoImpl implements NewsDao {

	@Autowired
	private HibernateUtil hibernateUtil;

	@Override
	public int createNews(News news) {

		return (int) hibernateUtil.create(news);
	}

	@Override
	public News updateNews(News news) {

		return hibernateUtil.update(news);
	}

	@Override
	public void deleteNews(int id) {
		News news = new News();
		news.setNewsId(id);
		hibernateUtil.delete(news);

	}

	@Override
	public List<News> getAllNews() {

		return hibernateUtil.fetchAll(News.class);
	}

	@Override
	public News getNews(int id) {

		return hibernateUtil.fetchById(id, News.class);
	}

	@Override
	public List<News> getLastNews() {
		
		return hibernateUtil.fetchAllSQL("select * from news order by create_date DESC limit 3", News.class);
	}

}
