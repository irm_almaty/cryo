package com.cryobank.postgres.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cryobank.postgres.dao.OrderStatusDao;
import com.cryobank.postgres.domain.OrderStatus;
import com.cryobank.util.HibernateUtil;

@Repository
public class OrderStatusDaoImpl implements OrderStatusDao{
   
	@Autowired
	private HibernateUtil hibernateUtil;
	
	@Override
	public List<OrderStatus> getAllOrderStatus() {
		
		return hibernateUtil.getAll(OrderStatus.class);
	}

	
	
}
