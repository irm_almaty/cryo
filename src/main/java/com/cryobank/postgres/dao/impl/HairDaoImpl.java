package com.cryobank.postgres.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cryobank.postgres.dao.HairDao;
import com.cryobank.postgres.domain.Hair;
import com.cryobank.util.HibernateUtil;

@Repository
public class HairDaoImpl implements HairDao {

	@Autowired
	private HibernateUtil hibernateUtil;

	@Override
	public List<Hair> getAllHairs() {

		return hibernateUtil.fetchAll(Hair.class);
	}

}
