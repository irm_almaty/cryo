package com.cryobank.postgres.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.cryobank.postgres.dao.ManDao;
import com.cryobank.postgres.domain.Man;
import com.cryobank.util.HibernateUtil;

@Repository
public class ManDaoImpl implements ManDao {
    
	/*
	@Autowired
    @Qualifier("sessionFactoryPostgres")
    private SessionFactory sessionFactory;
	
	public ManDaoImpl() {
    	System.out.println("EmployeeDAOImpl");
    }*/
	
	@Autowired
    private HibernateUtil hibernateUtil;
	
	
	@Override
	public long createMan(Man man) {
		 return (long) hibernateUtil.create(man);
	}

	@Override
	public Man updateMan(Man man) {
		return hibernateUtil.update(man);
	}

	@Override
	public void deleteMan(long id) {
		Man man = new Man();
        man.setId(id);
        hibernateUtil.delete(man);
		
	}

	/*
	@Override
	public List<Man> getAllMan() {
		  return sessionFactory.getCurrentSession().createQuery("from Man").list();
	}
	*/
	
	
	@Override
	public List<Man> getAllMan() {
		return hibernateUtil.fetchAll(Man.class);
	}
	

	@Override
	public Man getMan(long id) {
		 return hibernateUtil.fetchById(id, Man.class);
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public List<Man> getAllMan(String manName) {
		
		List<Man> mans = new ArrayList<Man>();
		return mans;	
		
	}
	
	

}
