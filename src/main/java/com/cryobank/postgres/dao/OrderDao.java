package com.cryobank.postgres.dao;

import java.util.List;

import com.cryobank.postgres.domain.DonorManPostgre;
import com.cryobank.postgres.domain.Order;
import com.cryobank.postgres.domain.QueryValue;

public interface OrderDao {

	public List<Order> getAllOrders();

	public List<Order> getAllOrdersByUser(int iuserId);

	public List<Order> getAllOrdersByUserByStatus(int iuserId, int statusId);

	public List<Order> getAllOrdersByIuserByStatus(int iuserId, String status);

	public List<Order> getAllOrdersByStatusNewOrder(int statusId);

	public List<Order> searchFromAllOrders(String querySh);

	public int createOrder(Order order);

	public Order getOrderById(int id);

	public Order updateOrder(Order order);

	public List<Order> getAllOrdersByIuserByStatusByDonorId(int iuserId, String status, int donorId);

	List<Order> getAllOrdersByStatus(String status);

	QueryValue getOrderCounts();

}
