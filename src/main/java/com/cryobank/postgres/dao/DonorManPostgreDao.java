package com.cryobank.postgres.dao;

import java.util.List;

import com.cryobank.postgres.domain.DonorManPostgre;
import com.cryobank.postgres.domain.Order;

public interface DonorManPostgreDao {
	
	public void saveDonor(DonorManPostgre donor);
	
	public int createDonor(DonorManPostgre donor);
	
	public DonorManPostgre updateDonor(DonorManPostgre donor);
     
	public void deleteDonorById(int id);
    
    public DonorManPostgre getDonor(int id);
    
    public List<DonorManPostgre> getAllDonors();
    
    public List<DonorManPostgre> getAllFemaleDonors();
    
    public List<DonorManPostgre> getAllMaleDonors();
    
    public List<Order> searchFromAllDonors(String queryDonor);
    

}
