package com.cryobank.postgres.dao;

import java.util.List;

import com.cryobank.postgres.domain.Price;

public interface PriceDao {

	 public List<Price> getAllPrice();
	
}
