package com.cryobank.postgres.dao;

import java.util.List;

import com.cryobank.postgres.domain.Article;


public interface ArticleDao {

	public int createArticle(Article article);
    public Article updateArticle(Article article);
    public void deleteArticle(int id);
    public Article getArticle(int id);
    public List<Article> getAllArticle();
    public List<Article> getLastArticle();

    
}
