package com.cryobank.postgres.dao;

import java.util.List;

import com.cryobank.postgres.domain.Eyes;

public interface EyesDao {
	
	public List<Eyes> getAllEyes();

}
