package com.cryobank.postgres.dao;

import java.util.List;

import com.cryobank.postgres.domain.Nation;



public interface NationDao {
	
	public List<Nation> getAllNations();

}
