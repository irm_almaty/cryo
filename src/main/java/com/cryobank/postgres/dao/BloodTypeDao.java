package com.cryobank.postgres.dao;

import java.util.List;

import com.cryobank.postgres.domain.BloodType;

public interface BloodTypeDao {
	
	public List<BloodType> getAllBloodTypes();

}
