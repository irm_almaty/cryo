package com.cryobank.postgres.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class QueryValue {

	@Id
	@Column(name = "waits_value")
	private Long waitsValue;

	@Column(name = "consider_value")
	private Long considerValue;

	@Column(name = "dossier_value")
	private Long dossierValue;

	@Column(name = "disagree_value")
	private Long disagreeValue;

	@Column(name = "accepted_value")
	private Long acceptedValue;

	@Column(name = "approved_value")
	private Long approvedValue;

	@Column(name = "rejected_value")
	private Long rejectedValue;

	@Column(name = "cancel_value")
	private Long cancelValue;

	@Column(name = "archived_value")
	private Long archivedValue;

	public Long getWaitsValue() {
		return waitsValue;
	}

	public void setWaitsValue(Long waitsValue) {
		this.waitsValue = waitsValue;
	}

	public Long getConsiderValue() {
		return considerValue;
	}

	public void setConsiderValue(Long considerValue) {
		this.considerValue = considerValue;
	}

	public Long getDossierValue() {
		return dossierValue;
	}

	public void setDossierValue(Long dossierValue) {
		this.dossierValue = dossierValue;
	}

	public Long getDisagreeValue() {
		return disagreeValue;
	}

	public void setDisagreeValue(Long disagreeValue) {
		this.disagreeValue = disagreeValue;
	}

	public Long getAcceptedValue() {
		return acceptedValue;
	}

	public void setAcceptedValue(Long acceptedValue) {
		this.acceptedValue = acceptedValue;
	}

	public Long getApprovedValue() {
		return approvedValue;
	}

	public void setApprovedValue(Long approvedValue) {
		this.approvedValue = approvedValue;
	}

	public Long getRejectedValue() {
		return rejectedValue;
	}

	public void setRejectedValue(Long rejectedValue) {
		this.rejectedValue = rejectedValue;
	}

	public Long getCancelValue() {
		return cancelValue;
	}

	public void setCancelValue(Long cancelValue) {
		this.cancelValue = cancelValue;
	}

	public Long getArchivedValue() {
		return archivedValue;
	}

	public void setArchivedValue(Long archivedValue) {
		this.archivedValue = archivedValue;
	}

}
