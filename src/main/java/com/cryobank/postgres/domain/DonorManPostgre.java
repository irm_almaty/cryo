package com.cryobank.postgres.domain;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDate;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "donor")
public class DonorManPostgre {

	@Id
	@Column(name = "donor_id", nullable = false)
	private int donorId;

	@Size(min = 3, max = 50)
	@Column(name = "name")
	private String donorName;


	@OneToMany(fetch=FetchType.LAZY, cascade = CascadeType.ALL, mappedBy="donor")
    private List<Photo> donorPhotos=new ArrayList<Photo>();
	
	@OneToMany(fetch=FetchType.LAZY, cascade = CascadeType.ALL, mappedBy="donor")
    private List<Voice> donorVoice=new ArrayList<Voice>();
	

	public List<Photo> getDonorPhotos() {
		return donorPhotos;
	}

	public void setDonorPhotos(List<Photo> donorPhotos) {
		this.donorPhotos = donorPhotos;
	}

	public List<Voice> getDonorVoice() {
		return donorVoice;
	}

	public void setDonorVoice(List<Voice> donorVoice) {
		this.donorVoice = donorVoice;
	}

	public List<Photo> getDonorPhoto() {
		return donorPhotos;
	}

	public void setDonorPhoto(List<Photo> donorPhotos) {
		this.donorPhotos = donorPhotos;
	}

	public int getDonorId() {
		return donorId;
	}

	public void setDonorId(int donorId) {
		this.donorId = donorId;
	}

	public String getDonorName() {
		return donorName;
	}

	public void setDonorName(String donorName) {
		this.donorName = donorName;
	}



}
