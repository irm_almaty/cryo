package com.cryobank.postgres.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="hair")
public class Hair {
	
	@Id
	@Column(name="hair_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int hairId;
	
	@Column(name="type")
	private String hairType;
	
	@Column(name="color")
	private String hairColor;

	public int getHairId() {
		return hairId;
	}

	public void setHairId(int hairId) {
		this.hairId = hairId;
	}

	public String getHairType() {
		return hairType;
	}

	public void setHairType(String hairType) {
		this.hairType = hairType;
	}

	public String getHairColor() {
		return hairColor;
	}

	public void setHairColor(String hairColor) {
		this.hairColor = hairColor;
	}

	
	
	

}
