package com.cryobank.postgres.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "nation")
public class Nation {
	
	@Id
	@Column(name = "nation_id", nullable = false)
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int nationId;
	
	@Column(name = "name", nullable = false)
	private String nationName;

	public int getNationId() {
		return nationId;
	}

	public void setNationId(int nationId) {
		this.nationId = nationId;
	}

	public String getNationName() {
		return nationName;
	}

	public void setNationName(String nationName) {
		this.nationName = nationName;
	}

	
	
	

}
