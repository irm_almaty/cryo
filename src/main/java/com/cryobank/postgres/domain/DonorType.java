package com.cryobank.postgres.domain;

public enum DonorType {

	SPERM, OOCYTE, SURMOM

}
