package com.cryobank.postgres.domain;

public enum OrderStatus {
	WAITS, CONSIDER, DOSSIER, DISAGREE, ACCEPTED, APPROVED, REJECTED, CANCEL, ARCHIVED
}
