package com.cryobank.postgres.domain.dto;

import java.util.List;

import com.cryobank.postgres.domain.Order;

public class OrderDTO {

	private List<Order> orders;

	private String action;

	public OrderDTO() {
	}

	public OrderDTO(List<Order> orders, String action) {
		super();
		this.orders = orders;
		this.action = action;
	}

	public List<Order> getOrders() {
		return orders;
	}

	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

}
