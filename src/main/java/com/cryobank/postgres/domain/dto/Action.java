package com.cryobank.postgres.domain.dto;

import com.cryobank.postgres.domain.OrderStatus;

public class Action {

	private String actionCode;

	private String actionName;

	public Action() {

	}

	public Action(String actionCode, String actionName) {
		super();
		this.actionCode = actionCode;
		this.actionName = actionName;
	}

	public String getActionCode() {
		return actionCode;
	}

	public void setActionCode(String actionCode) {
		this.actionCode = actionCode;
	}

	public String getActionName() {
		return actionName;
	}

	public void setActionName(String actionName) {
		this.actionName = actionName;
	}

}
