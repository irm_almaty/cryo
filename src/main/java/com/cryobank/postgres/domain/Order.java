package com.cryobank.postgres.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;
import org.springframework.format.annotation.DateTimeFormat;

import com.cryobank.medialog.domain.DonorManMedialog;
import com.cryobank.medialog.domain.DonorWomanMedialog;
import com.cryobank.medialog.domain.Iuser;

@Entity
@Table(name = "order_donor")
public class Order {

	@Id
	@Column(name = "order_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "iuser_id")
	private Integer iuserId;

	@Column(name = "iuser_email")
	private String iuserEmail;

	@Column(name = "iuser_name")
	private String iuserName;

	@Enumerated(EnumType.STRING)
	private DonorType donorType;

	@Column(name = "donor_id")
	private Integer donorId;

	@Column(name = "donor_code")
	private String donorCode;

	@Column(name = "order_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date orderDate = new Date();

	@Column(name = "approved_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date approvedDate;

	private String description;

	@Enumerated(EnumType.STRING)
	private OrderStatus orderStatus;

	public Order() {

	}

	public Order(DonorManMedialog donorManMedialog, Iuser iuser, OrderStatus orderStatus) {
		this.iuserId = iuser.getUserId();
		this.iuserEmail = iuser.getUserLogin();
		this.iuserName = iuser.getUserName();
		this.donorType = DonorType.SPERM;
		this.donorId = donorManMedialog.getDonorManId();
		this.donorCode = donorManMedialog.getDonorCode();
		this.orderStatus = orderStatus;
	}

	public Order(DonorWomanMedialog donorWomanMedialog, Iuser iuser, OrderStatus orderStatus) {
		this.iuserId = iuser.getUserId();
		this.iuserEmail = iuser.getUserLogin();
		this.iuserName = iuser.getUserName();
		this.donorType = DonorType.OOCYTE;
		this.donorId = donorWomanMedialog.getDonorWomanId();
		this.donorCode = donorWomanMedialog.getDonorCode();
		this.orderStatus = orderStatus;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDonorCode() {
		return donorCode;
	}

	public void setDonorCode(String donorCode) {
		this.donorCode = donorCode;
	}

	public Integer getIuserId() {
		return iuserId;
	}

	public void setIuserId(Integer iuserId) {
		this.iuserId = iuserId;
	}

	public Integer getDonorId() {
		return donorId;
	}

	public void setDonorId(Integer donorId) {
		this.donorId = donorId;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public Date getApprovedDate() {
		return approvedDate;
	}

	public void setApprovedDate(Date approvedDate) {
		this.approvedDate = approvedDate;
	}

	public String getIuserEmail() {
		return iuserEmail;
	}

	public void setIuserEmail(String iuserEmail) {
		this.iuserEmail = iuserEmail;
	}

	public String getIuserName() {
		return iuserName;
	}

	public void setIuserName(String iuserName) {
		this.iuserName = iuserName;
	}

	public DonorType getDonorType() {
		return donorType;
	}

	public void setDonorType(DonorType donorType) {
		this.donorType = donorType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public OrderStatus getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(OrderStatus orderStatus) {
		this.orderStatus = orderStatus;
	}

}
