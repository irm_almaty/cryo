package com.cryobank.postgres.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.cryobank.medialog.domain.Iuser;

@Entity
@Table(name="logger_detail")
public class LoggerDetail {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private Date createDate=new Date();

	@Column(name = "action", nullable = false)
	private String action;

	private String description;

	private Iuser user;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Iuser getUser() {
		return user;
	}

	public void setUser(Iuser user) {
		this.user = user;
	}

}
