package com.cryobank.postgres.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "voice")
public class Voice {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int voiceId;
	
	@Column(name = "voice_path", nullable = false)
	private String voicePath;
	
	@Column(name = "create_date", nullable = false)
	@DateTimeFormat(pattern="dd/MM/yyyy") 
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	private LocalDateTime voiceCreateDate;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name ="donor_id")
	private DonorManPostgre donor;

	public int getVoiceId() {
		return voiceId;
	}

	public DonorManPostgre getDonor() {
		return donor;
	}

	public void setDonor(DonorManPostgre donor) {
		this.donor = donor;
	}

	public void setVoiceId(int voiceId) {
		this.voiceId = voiceId;
	}

	public String getVoicePath() {
		return voicePath;
	}

	public void setVoicePath(String voicePath) {
		this.voicePath = voicePath;
	}

	public LocalDateTime getVoiceCreateDate() {
		return voiceCreateDate;
	}

	public void setVoiceCreateDate(LocalDateTime voiceCreateDate) {
		this.voiceCreateDate = voiceCreateDate;
	}

	
	
	
}
