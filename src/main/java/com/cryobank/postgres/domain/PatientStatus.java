package com.cryobank.postgres.domain;

public enum PatientStatus {
	ACTIVE,
	EXPECT,
	ARCHIVED
}
