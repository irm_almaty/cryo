package com.cryobank.postgres.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.cryobank.medialog.domain.Iuser;
import com.cryobank.medialog.domain.PatientMedialog;
import com.cryobank.util.Constants;

@Entity
@Table(name = "patient")
public class PatientPostgre {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	private int patientId;

	private String lastname;

	private String firstname;

	private String patroname;

	private String email;

	private String country;

	private String city;

	private String street;

	private String home;

	private String phone;

	private String access;

	private Integer iuserId;

	private String registryFrom;

	@Transient
	private String fio;

	public String getFio() {
		String fioNew;
		fioNew = this.lastname + " " + this.firstname + " " + this.patroname;
		return fioNew;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "patient_status")
	private PatientStatus patientStatus = PatientStatus.EXPECT;

	private Date createDate = new Date();

	private Date birthday;

	public PatientPostgre() {

	}

	public PatientPostgre(PatientMedialog patientMedialog) {
		super();
		this.lastname = patientMedialog.getLastname();
		this.firstname = patientMedialog.getFirstname();
		this.patroname = patientMedialog.getPatroname();
		this.country = patientMedialog.getCountry();
		this.phone = patientMedialog.getPhone();
		this.email = patientMedialog.getEmail();
		this.city = patientMedialog.getCity();
		this.iuserId = patientMedialog.getId();
		this.registryFrom = Constants.USER_MEDIALOG;
		this.patientStatus = PatientStatus.ACTIVE;
		this.createDate = new Date();
	}

	public PatientStatus getPatientStatus() {
		return patientStatus;
	}

	public void setPatientStatus(PatientStatus patientStatus) {
		this.patientStatus = patientStatus;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPatientId() {
		return patientId;
	}

	public void setPatientId(int patientId) {
		this.patientId = patientId;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getPatroname() {
		return patroname;
	}

	public void setPatroname(String patroname) {
		this.patroname = patroname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getHome() {
		return home;
	}

	public void setHome(String home) {
		this.home = home;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Integer getIuserId() {
		return iuserId;
	}

	public void setIuserId(Integer iuserId) {
		this.iuserId = iuserId;
	}

	public String getAccess() {
		return access;
	}

	public void setAccess(String access) {
		this.access = access;
	}

	public String getRegistryFrom() {
		return registryFrom;
	}

	public void setRegistryFrom(String registryFrom) {
		this.registryFrom = registryFrom;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

}
