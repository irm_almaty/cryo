package com.cryobank.postgres.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "eyes")
public class Eyes {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name ="eyes_id")
	private int eyesId;
	
	@Column(name= "color")
	private String eyesColor;

	public int getEyesId() {
		return eyesId;
	}

	public void setEyesId(int eyesId) {
		this.eyesId = eyesId;
	}

	public String getEyesColor() {
		return eyesColor;
	}

	public void setEyesColor(String eyesColor) {
		this.eyesColor = eyesColor;
	}

	

}
