package com.cryobank.postgres.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="skin")
public class Skin {
	
	@Id
	@Column(name="skin_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int skinId;
	
	@Column(name="type",nullable = false)
	private String skinType;

	public int getSkinId() {
		return skinId;
	}

	public void setSkinId(int skinId) {
		this.skinId = skinId;
	}

	public String getSkinType() {
		return skinType;
	}

	public void setSkinType(String skinType) {
		this.skinType = skinType;
	}

	
	
	
	

}
