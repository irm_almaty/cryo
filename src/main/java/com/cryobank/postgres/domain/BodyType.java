package com.cryobank.postgres.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="body_type")
public class BodyType  {
	
	@Id
	@Column(name="body_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int bodyId;
	
	@Column(name="type",nullable = false)
	private String bodyTypeName;

	public int getBodyId() {
		return bodyId;
	}

	public void setBodyId(int bodyId) {
		this.bodyId = bodyId;
	}

	public String getBodyTypeName() {
		return bodyTypeName;
	}

	public void setBodyTypeName(String bodyTypeName) {
		this.bodyTypeName = bodyTypeName;
	}
	
}
