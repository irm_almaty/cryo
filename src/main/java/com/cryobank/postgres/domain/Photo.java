package com.cryobank.postgres.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "photo")
public class Photo {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int photoId;
	
	@Column(name = "photo_path", nullable = false)
	private String photoPath;
	
	@Column(name = "create_date", nullable = false)
	@DateTimeFormat(pattern="dd/MM/yyyy") 
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	private LocalDateTime photoCreateDate;

	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name ="donor_id")
	private DonorManPostgre donor;
	
	
	public DonorManPostgre getDonor() {
		return donor;
	}

	public void setDonor(DonorManPostgre donor) {
		this.donor = donor;
	}

	public int getPhotoId() {
		return photoId;
	}

	public void setPhotoId(int photoId) {
		this.photoId = photoId;
	}

	public String getPhotoPath() {
		return photoPath;
	}

	public void setPhotoPath(String photoPath) {
		this.photoPath = photoPath;
	}

	public LocalDateTime getPhotoCreateDate() {
		return photoCreateDate;
	}

	public void setPhotoCreateDate(LocalDateTime photoUploadDate) {
		this.photoCreateDate = photoUploadDate;
	}
	
	

}
