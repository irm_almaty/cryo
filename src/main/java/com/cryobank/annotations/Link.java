package com.cryobank.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by Shah on 28.10.2017.
 */
@Retention(value = RetentionPolicy.RUNTIME)
public @interface Link {

    String title() default "";
    String description() default "";
    String keywords() default "";
    boolean index() default true;

}