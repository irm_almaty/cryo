package com.cryobank.interceptor;

import com.cryobank.annotations.Link;
import com.cryobank.util.meta.MetaConstants;
import com.cryobank.util.meta.MetaData;
import com.cryobank.util.meta.MetaDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.List;


/**
 * Created by Shah on 28.10.2017.
 */
public class MetaDataInterceptor extends HandlerInterceptorAdapter {

    @Autowired
    private MetaDataService metaDataService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        boolean isHaveMetaData = false;
        if (handler instanceof HandlerMethod) {
            Annotation[] declaredAnnotations = getDeclaredAnnotationsForHandler(handler);
            HttpSession session = request.getSession();
            for (Annotation annotation : declaredAnnotations) {
                if (annotation.annotationType().equals(Link.class)) {
                    setMetaData(session, annotation);
                    isHaveMetaData = true;
                }
            }
            if (!isHaveMetaData) {
                setDefaultMetaData(request.getSession());
            }
            return true;
        }

        return true;
    }

    private void setMetaData(HttpSession session, Annotation annotation) {

        Link link = (Link) annotation;

        List<MetaData> metaDatas = metaDataService.getMetaDatas(link);
        session.setAttribute(MetaConstants.metaAttribute, metaDatas);

        String title = metaDataService.getTitle(link);
        session.setAttribute(MetaConstants.metaTitle, title);
    }

    private void setDefaultMetaData(HttpSession session) {

        List<MetaData> metaDatas = metaDataService.getDefaultMetaDatas();
        session.setAttribute(MetaConstants.metaAttribute, metaDatas);

        String title = metaDataService.getDefaultTitle();
        session.setAttribute(MetaConstants.metaTitle, title);
    }

    private Annotation[] getDeclaredAnnotationsForHandler(Object handler) {
        HandlerMethod handlerMethod = (HandlerMethod) handler;
        Method method = handlerMethod.getMethod();
        Annotation[] declaredAnnotations = method.getDeclaredAnnotations();
        return declaredAnnotations;
    }


}
