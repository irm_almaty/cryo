package com.cryobank.util;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class PaginationService {

	public List<PaginationItem> pags(int count, int maxResult, int page) {
		List<PaginationItem> pags = new ArrayList<>();
		int startOffset = 0;
		int tagsCount = count / maxResult;
		int residue = count % maxResult;

		if (residue != 0) {
			tagsCount = tagsCount + 1;
		}
		for (int i = 1; i <= tagsCount; i++) {

			PaginationItem paginationItem = new PaginationItem();
			paginationItem.setPageNumber(i);
			paginationItem.setOffset(startOffset);
			startOffset = startOffset + maxResult;
			paginationItem.setButtoneDescription("disable");
			if (paginationItem.getPageNumber() == page) {
				paginationItem.setButtoneDescription("active");

			}
			pags.add(paginationItem);

		}
		return pags;

	}

	public int offsetCount(int maxResult, int page) {
		int offsetCount;
		if (page == 0 || page == 1) {
			offsetCount = 0;
			return offsetCount;
		}
		offsetCount = maxResult * (page - 1);

		return offsetCount;
	}

	public PaginationItem getNextPreviousPage(List<PaginationItem> pags, int count, int maxResult) {

		int pageCount = count / maxResult;
		int residue = count % maxResult;
		if (residue != 0) {
			pageCount = pageCount + 1;
		}

		PaginationItem paginationItem = new PaginationItem();
		for (PaginationItem pag : pags) {
			String buttoneDescription = "active";
			if (buttoneDescription.equals(pag.getButtoneDescription())) {
				int page = pag.getPageNumber();

				if (page == 1) {
					paginationItem.setPreviousPage(page);
				} else {
					paginationItem.setPreviousPage(page - 1);
				}

				if (page == pageCount) {
					paginationItem.setNextPage(page);
				} else {
					paginationItem.setNextPage(page + 1);
				}
			}
		}
		return paginationItem;
	}

	public Pagination getPagination(int count, int maxResult, int page) {

		Pagination pagination = new Pagination();

		List<PaginationItem> pagItems = pags(count, maxResult, page);
		int offsetCount = offsetCount(maxResult, page);

		PaginationItem pagItem = getNextPreviousPage(pagItems, count, maxResult);

		pagination.setPagItems(pagItems);
		pagination.setOffsetCount(offsetCount);
		pagination.setPagItem(pagItem);
		pagination.setShowPagination(true);
		if (count < maxResult) {
			pagination.setShowPagination(false);
		}

		return pagination;
	}

}
