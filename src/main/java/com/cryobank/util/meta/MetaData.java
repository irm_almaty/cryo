package com.cryobank.util.meta;

/**
 * Created by Suleiman on 29.10.2017.
 */
public class MetaData {

    private MetaDataName name;

    private String content;

    public MetaData() {

    }

    public MetaData(MetaDataName name, String content) {
        this.name = name;
        this.content = content;
    }

    public MetaDataName getMetaDataName() {
        return name;
    }

    public void setMetaDataName(MetaDataName name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

}
