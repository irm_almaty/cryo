package com.cryobank.util.meta;

import com.cryobank.annotations.Link;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Created by Suleiman on 29.10.2017.
 */
@Component
public class MetaDataService {

    @Autowired
    private Properties metaProperty;

    private List<MetaData> defaultMetaDatas;

    private String defaultTitle;


    public String getDefaultTitle() {
        if (StringUtils.isEmpty(defaultTitle)) {
            this.defaultTitle = metaProperty.getProperty(MetaConstants.Default.title);
        }
        return this.defaultTitle;
    }

    public String getTitle(Link link) {
        if (!StringUtils.isEmpty(link)) {
            String title = metaProperty.getProperty(link.title());
            return title;
        }
        return this.defaultTitle;
    }

    public List<MetaData> getDefaultMetaDatas() {
        if (CollectionUtils.isEmpty(defaultMetaDatas)) {

            this.defaultMetaDatas = new ArrayList<>();

            MetaData description = new MetaData(MetaDataName.DESCRIPTION, metaProperty.getProperty(MetaConstants.Default.description));
            MetaData keywords = new MetaData(MetaDataName.KEYWORDS, metaProperty.getProperty(MetaConstants.Default.keywords));

            this.defaultMetaDatas.add(description);
            this.defaultMetaDatas.add(keywords);

            return this.defaultMetaDatas;
        }
        return this.defaultMetaDatas;
    }


    public List<MetaData> getMetaDatas(Link link) {

        List<MetaData> metaDatas = new ArrayList<>();
        if (!link.index()) {
            MetaData metaData = new MetaData(MetaDataName.ROBOTS, metaProperty.getProperty(MetaConstants.Default.noIndex));
            metaDatas.add(metaData);
        }

        addMetaData(MetaDataName.KEYWORDS, link.keywords(), metaDatas);
        addMetaData(MetaDataName.DESCRIPTION, link.description(), metaDatas);

        return metaDatas;
    }


    private void addMetaData(MetaDataName name, String content, List<MetaData> metaDatas) {
        MetaData metaData = null;
        if (StringUtils.isEmpty(content)) {
            if (MetaConstants.defaultMetaMap.containsKey(name)) {
                metaData = new MetaData(name, metaProperty.getProperty(MetaConstants.defaultMetaMap.get(name)));
            }
        } else {
            metaData = new MetaData(name, metaProperty.getProperty(content));
        }

        if (metaData != null) {
            metaDatas.add(metaData);
        }
    }

}
