package com.cryobank.util.meta;

/**
 * Created by Suleiman on 29.10.2017.
 */
public enum MetaDataName {

    DESCRIPTION("description"),
    KEYWORDS("keywords"),
    ROBOTS("robots");

    private String value;

    MetaDataName(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
