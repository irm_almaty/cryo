package com.cryobank.util.meta;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Suleiman on 22.10.2017.
 */
public class MetaConstants {

    public static final String metaTitle = "title";

    public static final String metaAttribute = "metaDatas";

    public static final String canonical = "canonical";

    public static final Map<MetaDataName, String> defaultMetaMap;

    static {
        Map<MetaDataName, String> aMap = new HashMap<>();
        aMap.put(MetaDataName.KEYWORDS, Default.keywords);
        aMap.put(MetaDataName.DESCRIPTION, Default.description);
        defaultMetaMap = Collections.unmodifiableMap(aMap);
    }

    public class Default {
        public final static String noIndex = "default.noindex";
        public final static String title = "default.page.title";
        public final static String description = "default.page.meta.description";
        public final static String keywords = "default.page.meta.keywords";
    }

    public class Contact {
        public final static String title = "contact.title";
        public final static String description = "contact.description";
    }

    public class DonorSperm {
        public final static String title = "d.sperm.list.title";
        public final static String description = "d.sperm.list.description";
        public final static String keywords = "d.sperm.list.keywords";

        public class Form {
            public final static String title = "d.sperm.form.title";
            public final static String description = "d.sperm.form.description";
        }
    }

    public class DonorOocyte {
        public final static String title = "d.oocyte.list.title";
        public final static String description = "d.oocyte.list.description";
        public final static String keywords = "d.oocyte.list.keywords";

        public class Form {
            public final static String title = "d.oocyte.form.title";
            public final static String description = "d.oocyte.form.description";
        }
    }

    public class Surmom {
        public final static String title = "d.surmom.list.title";
        public final static String description = "d.surmom.list.description";
        public final static String keywords = "d.surmom.list.keywords";

        public class Form {
            public final static String title = "d.surmom.form.title";
            public final static String description = "d.surmom.form.description";
        }
    }

    public class Canonical {
        public final static String spermList = "/donor/sperm/list";
        public final static String oocyteList = "/donor/oocyte/list";
        public final static String surmomList = "/donor/surmom/list";

    }

}
