package com.cryobank.util;

public class PaginationItem {

	private int pageNumber;

	private int nextPage;

	private int previousPage;

	private int offset;

	private int maxResult;

	private String buttoneDescription;

	public int getPageNumber() {
		return pageNumber;
	}

	public int getNextPage() {
		return nextPage;
	}

	public void setNextPage(int nextPage) {
		this.nextPage = nextPage;
	}

	public int getPreviousPage() {
		return previousPage;
	}

	public void setPreviousPage(int previousPage) {
		this.previousPage = previousPage;
	}

	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public int getMaxResult() {
		return maxResult;
	}

	public void setMaxResult(int maxResult) {
		this.maxResult = maxResult;
	}

	public String getButtoneDescription() {
		return buttoneDescription;
	}

	public void setButtoneDescription(String buttoneDescription) {
		this.buttoneDescription = buttoneDescription;
	}

}
