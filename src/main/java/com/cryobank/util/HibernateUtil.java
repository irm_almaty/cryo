package com.cryobank.util;

import java.io.Serializable;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.cryobank.postgres.domain.DonorManPostgre;

@Repository
public class HibernateUtil {

	@Autowired
	@Qualifier("sessionFactory")
	private SessionFactory sessionFactory;

	public <T> Serializable create(final T entity) {
		return sessionFactory.getCurrentSession().save(entity);
	}

	public <T> T update(final T entity) {
		sessionFactory.getCurrentSession().update(entity);
		return entity;
	}

	public <T> void delete(final T entity) {
		sessionFactory.getCurrentSession().delete(entity);
	}

	public <T> void delete(Serializable id, Class<T> entityClass) {
		T entity = fetchById(id, entityClass);
		delete(entity);
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> fetchAll(Class<T> entityClass) {
		return sessionFactory.getCurrentSession().createQuery(" FROM " + entityClass.getName()).list();
	}

	@SuppressWarnings("rawtypes")
	public <T> List fetchAll(String query) {
		return sessionFactory.getCurrentSession().createSQLQuery(query).list();
	}

	@SuppressWarnings("unchecked")
	public <T> T fetchById(Serializable id, Class<T> entityClass) {
		return (T) sessionFactory.getCurrentSession().get(entityClass, id);
	}

	@SuppressWarnings("rawtypes")
	public <T> List fetchAllSQL(String query, Class<T> entityClass) {
		return sessionFactory.getCurrentSession().createSQLQuery(query).addEntity(entityClass).list();
	}

	@SuppressWarnings("rawtypes")
	public <T> T getByValue(String query, Class<T> entityClass) {
		return (T) sessionFactory.getCurrentSession().createSQLQuery(query).addEntity(entityClass).uniqueResult();
	}

	@SuppressWarnings("rawtypes")
	public <T> List getAll(Class<T> entityClass) {
		return sessionFactory.getCurrentSession().createCriteria(entityClass).list();
	}

}
