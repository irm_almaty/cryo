package com.cryobank.util.sessionDTO;

import com.cryobank.medialog.domain.Iuser;
import com.cryobank.postgres.domain.PatientPostgre;

public class RegDTO {

	private PatientPostgre patientPostgre;

	private Iuser iuser;

	private String error;

	public PatientPostgre getPatientPostgre() {
		return patientPostgre;
	}

	public void setPatientPostgre(PatientPostgre patientPostgre) {
		this.patientPostgre = patientPostgre;
	}

	public Iuser getIuser() {
		return iuser;
	}

	public void setIuser(Iuser iuser) {
		this.iuser = iuser;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

}
