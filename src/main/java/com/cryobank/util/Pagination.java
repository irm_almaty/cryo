package com.cryobank.util;

import java.util.List;

public class Pagination {

	private List<PaginationItem> pagItems;
	private PaginationItem pagItem;
	private int offsetCount;
	private boolean showPagination;

	public List<PaginationItem> getPagItems() {
		return pagItems;
	}

	public void setPagItems(List<PaginationItem> pagItems) {
		this.pagItems = pagItems;
	}

	public PaginationItem getPagItem() {
		return pagItem;
	}

	public void setPagItem(PaginationItem pagItem) {
		this.pagItem = pagItem;
	}

	public int getOffsetCount() {
		return offsetCount;
	}

	public void setOffsetCount(int offsetCount) {
		this.offsetCount = offsetCount;
	}

	public boolean isShowPagination() {
		return showPagination;
	}

	public void setShowPagination(boolean showPagination) {
		this.showPagination = showPagination;
	}

}
