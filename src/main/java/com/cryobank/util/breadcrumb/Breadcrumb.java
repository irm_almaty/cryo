package com.cryobank.util.breadcrumb;

/**
 * Created by Suleiman on 07.11.2017.
 */
public class Breadcrumb {

    public static final String ATTRIBUTE = "breadcrumb";

    private String name;
    private String url;
    private String code;
    private int index;

    public Breadcrumb() {
    }

    public Breadcrumb(String name) {
        this.name = name;
    }

    public Breadcrumb(String name, String url) {
        this.name = name;
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
