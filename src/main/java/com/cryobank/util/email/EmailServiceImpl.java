package com.cryobank.util.email;

import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.core.io.InputStreamSource;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.sun.mail.util.MailSSLSocketFactory;
import com.cryobank.util.email.EmailService;

@Service
public class EmailServiceImpl implements EmailService {

	private String protocol = "smtp";

	private String host = "mail.irm.kz";

	private String port = "587";

	private String login = "cryobank@irm.kz";

	private String password = "CRyo123";

	private String from = "cryobank@irm.kz";

	public void sendMultipartEmail(Email email, CommonsMultipartFile attachFile) throws MessagingException {
		Properties props = getProperties();
		Session mailSession = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(login, password);
			}
		});
		mailSession.setDebug(true); // Enable the debug mode
		MimeMessage msg = new MimeMessage(mailSession);
		MimeMessageHelper messageHelper = new MimeMessageHelper(msg, true, "UTF-8");
		messageHelper.setTo(email.getAddresses().get(0));
		messageHelper.setSubject(email.getSubject());
		messageHelper.setText(email.getText());
		// determines if there is an upload file, attach it to the
		// e-mail
		String attachName = attachFile.getOriginalFilename();
		if (!attachFile.equals("") && !attachFile.isEmpty() && attachFile != null) {
			messageHelper.addAttachment(attachName, new InputStreamSource() {
				@Override
				public InputStream getInputStream() throws IOException {
					return attachFile.getInputStream();
				}
			});
		}

	}

	public void sendSingleEmail(Email email) {

		Properties props = getProperties();
		Session mailSession = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(login, password);
			}
		});

		mailSession.setDebug(true); // Enable the debug mode
		MimeMessage msg = new MimeMessage(mailSession);

		// --[ Set the FROM, TO, DATE and SUBJECT fields
		try {
			msg.setFrom(new InternetAddress("cryobank@irm.kz"));
		} catch (AddressException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		}
		try {
			msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email.getAddresses().get(0)));
		} catch (AddressException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		}
		try {
			msg.setSubject(email.getSubject());
		} catch (MessagingException e) {
			e.printStackTrace();
		}
		try {
			msg.setContent(email.getText(), "text/html; charset=utf-8");
		} catch (MessagingException e) {
			e.printStackTrace();
		}

		try {
			Transport.send(msg);
		} catch (MessagingException e) {
			e.printStackTrace();
		}

	}

	public void sendMultipleEmail(Email email) {

		Properties props = getProperties();
		Session mailSession = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(login, password);
			}
		});

		mailSession.setDebug(true); // Enable the debug mode
		MimeMessage msg = new MimeMessage(mailSession);

		// --[ Set the FROM, TO, DATE and SUBJECT fields
		try {
			msg.setFrom(new InternetAddress("cryobank@irm.kz"));
		} catch (AddressException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		}
		try {

			msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email.getAddresses().get(0)));

		} catch (AddressException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		}
		try {
			msg.setSubject(email.getSubject());
		} catch (MessagingException e) {
			e.printStackTrace();
		}
		try {
			msg.setContent(email.getText(), "text/html; charset=utf-8");
		} catch (MessagingException e) {
			e.printStackTrace();
		}

		try {
			Transport.send(msg);
		} catch (MessagingException e) {
			e.printStackTrace();
		}

	}

	public Properties getProperties() {
		Properties props = new Properties();
		props.put("mail.transport.protocol", protocol);
		props.put("mail.smtp.host", host);
		props.put("mail.smtp.auth", "true");
		props.put("mail.debug", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.port", port);
		props.put("mail.smtp.socketFactory.port", port);
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.socketFactory.fallback", "true");
		MailSSLSocketFactory sf = null;
		try {
			sf = new MailSSLSocketFactory();
		} catch (GeneralSecurityException e1) {

			e1.printStackTrace();
		}
		sf.setTrustAllHosts(true);

		props.put("mail.smtp.ssl.socketFactory", sf);

		return props;
	}

}
