package com.cryobank.util.email;

import javax.mail.MessagingException;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

public interface EmailService {

	void sendSingleEmail(Email email);

	void sendMultipleEmail(Email email);

	void sendMultipartEmail(Email email, CommonsMultipartFile attachFile) throws MessagingException;

}
