package com.cryobank.util.email;

import java.util.ArrayList;
import java.util.List;

public class Email {

	private List<String> addresses;

	private String text;

	private String subject;

	public Email() {

	}

	public Email(String email, String subject, String text) {
		List<String> list=new ArrayList<>();
		list.add(email);
		this.text = text;
		this.subject = subject;
		this.addresses=list;
	}

	public Email(String text, String subject) {
		this.text = text;
		this.subject = subject;
	}

	public List<String> getAddresses() {
		return addresses;
	}

	public void setAddresses(List<String> addresses) {
		this.addresses = addresses;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

}
