package com.cryobank.util;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

@Repository
public class HibernateMedialogUtil {

	@Autowired
	@Qualifier("sessionFactoryMedialog")
	private SessionFactory sessionFactory;

	public <T> Serializable create(final T entity) {
		return sessionFactory.getCurrentSession().save(entity);
	}

	public <T> T update(final T entity) {
		sessionFactory.getCurrentSession().update(entity);
		return entity;
	}

	public <T> void delete(final T entity) {
		sessionFactory.getCurrentSession().delete(entity);

	}

	public <T> void delete(Serializable id, Class<T> entityClass) {
		T entity = fetchById(id, entityClass);
		delete(entity);
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> fetchAll(Class<T> entityClass) {
		return sessionFactory.getCurrentSession().createQuery(" FROM " + entityClass.getName()).list();
	}

	@SuppressWarnings("rawtypes")
	public <T> List fetchAll(String query) {
		return sessionFactory.getCurrentSession().createSQLQuery(query).list();
	}

	@SuppressWarnings("unchecked")
	public <T> T fetchById(Serializable id, Class<T> entityClass) {
		return (T) sessionFactory.getCurrentSession().get(entityClass, id);
	}

	@SuppressWarnings("rawtypes")
	public <T> List fetchAllSQL(String query, Class<T> entityClass) {
		return sessionFactory.getCurrentSession().createSQLQuery(query).addEntity(entityClass).list();
	}

	@SuppressWarnings("rawtypes")
	public <T> T getCount(String query) {
		return (T) sessionFactory.getCurrentSession().createSQLQuery(query).uniqueResult();
	}

	@SuppressWarnings("unchecked")
	public <T> T fetchFirstResultSQL(String query, Class<T> entityClass) {

		try {
			List<T> result = sessionFactory.getCurrentSession().createSQLQuery(query).addEntity(entityClass).list();
			T entity = result.get(0);
			return entity;
		} catch (IndexOutOfBoundsException ex) {
			ex.printStackTrace();
		}
		return null;
	}

	@SuppressWarnings("rawtypes")
	public <T> List getAll(Class<T> entityClass) {
		return sessionFactory.getCurrentSession().createCriteria(entityClass).list();
	}

	@SuppressWarnings("rawtypes")
	public <T> List getAllPaginate(Class<T> entityClass, int offset, int maxResult) {
		return sessionFactory.getCurrentSession().createCriteria(entityClass).setFirstResult(offset)
				.setMaxResults(maxResult).list();
	}

	@SuppressWarnings("rawtypes")
	public <T> List getAllPaginateByQuery(Class<T> entityClass, int offset, int maxResult, String query) {
		return sessionFactory.getCurrentSession().createSQLQuery(query).addEntity(entityClass).setFirstResult(offset)
				.setMaxResults(maxResult).list();
	}

	public boolean checkSession() {
		boolean check = sessionFactory.getCurrentSession().isOpen();
		if (check == false) {
			sessionFactory.close();
		}
		return check;
	}
	
	@SuppressWarnings("rawtypes")
	public <T> T getByValue(String query, Class<T> entityClass) {
		return (T) sessionFactory.getCurrentSession().createSQLQuery(query).addEntity(entityClass).uniqueResult();
	}

}
