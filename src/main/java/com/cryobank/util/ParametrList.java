package com.cryobank.util;

import java.util.List;

public class ParametrList {

	private List<String> nations;

	private List<String> hairs;

	private List<String> blood;

	private List<String> eyes;

	public List<String> getNations() {
		return nations;
	}

	public void setNations(List<String> nations) {
		this.nations = nations;
	}

	public List<String> getHairs() {
		return hairs;
	}

	public void setHairs(List<String> hairs) {
		this.hairs = hairs;
	}

	public List<String> getBlood() {
		return blood;
	}

	public void setBlood(List<String> blood) {
		this.blood = blood;
	}

	public List<String> getEyes() {
		return eyes;
	}

	public void setEyes(List<String> eyes) {
		this.eyes = eyes;
	}

}
