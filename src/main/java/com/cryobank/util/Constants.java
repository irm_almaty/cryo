package com.cryobank.util;

public class Constants {

	/*
	 * Actions
	 */
	public static final String ACTION_CREATE_NEW_USER = "ACTION_CREATE_NEW_USER";

	public static final String ACTION_SEND_EMAIL = "ACTION_SEND_EMAIL";

	public static final String ACTION_SEND_AUTORIZATION_INFO = "ACTION_SEND_AUTORIZATION_INFO";

	/*
	 * SQL
	 */
	public static final String SQL_DONOR_NATION = "nation";

	public static final String SQL_DONOR_HAIR_COLOR = "cvet_volos";

	public static final String SQL_DONOR_EYES_COLOR = "cvet_glaz";

	public static final String SQL_DONOR_BLOOD_TYPE = "gruppa_krovi";

	public static final String SQL_DONOR_CODE = "code_name";

	public static final String SQL_DONOR_ID = "donor_id";

	public static final Integer IUSER_STATUS_ACTIVE = 1;

	public static final Integer IUSER_STATUS_ARCHIVED = 2;

	public static final String IUSER_IGNORE = "1,2";

	/*
	 * SQL
	 */
	public static final String URL_DONOR_OOCYTE_LIST = "";

	public static final String URL_DONOR_SPERM_LIST = "";

	public static final String URL_DONOR_SURMOM_LIST = "";

	public static final String URL_DONOR_OOCYTE_REFRESH = "/donor/oocyte/list/refresh";

	public static final String URL_DONOR_SPERM_REFRESH = "";

	public static final String URL_DONOR_SURMOM_REFRESH = "";

	/*
	 * Search parameters for searching donor by criteria
	 */
	public static final String SEARCH_PARAMETERS_OOCYTE = "search_oocyte";

	public static final String SEARCH_PARAMETERS_SPERM = "";

	public static final String SEARCH_PARAMETERS_SURMOM = "";

	public final static String USER_ANONYMOUS = "anonymousUser";

	public static final String USER_MEDIALOG = "medialog";

	public static final String USER_WEB = "web";

}
