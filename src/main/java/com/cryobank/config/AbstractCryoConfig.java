package com.cryobank.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

/**
 * Created by Suleiman on 22.10.2017.
 */
abstract class AbstractCryoConfig {

    @Autowired
    private Environment environment;

    public String getProperty(String code) {
        String result = environment.getProperty(code);
        return result;
    }

}
