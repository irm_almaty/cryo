package com.cryobank.medialog.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity
public class CountValue {

	@Id
	@Column(name = "active_value")
	private Long activeValue;

	@Column(name = "archive_value")
	private Long archivedValue;

	@Transient
	private Long expectValue;

	public Long getActiveValue() {
		return activeValue;
	}

	public void setActiveValue(Long activeValue) {
		this.activeValue = activeValue;
	}

	public Long getArchivedValue() {
		return archivedValue;
	}

	public void setArchivedValue(Long archivedValue) {
		this.archivedValue = archivedValue;
	}

	public Long getExpectValue() {
		return expectValue;
	}

	public void setExpectValue(Long expectValue) {
		this.expectValue = expectValue;
	}

}
