package com.cryobank.medialog.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDate;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "donor_child")
public class DonorChildMedialog {

	@Id
	@Column(name = "child_id")
	private int childId;

	@Column(name = "parent_id")
	private String parentId;

	@Column(name = "birthday")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
	private LocalDate birthday;

	@Column(name = "pol")
	private String pol;
	
	@Column(name = "ves")
	private String ves;
	
	@Column(name = "cvet_glaz")
	private String cvetGlaz;
	
	@Column(name = "cvet_volos")
	private String cvetVolos;
	
	public int getChildId() {
		return childId;
	}

	public void setChildId(int childId) {
		this.childId = childId;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public LocalDate getBirthday() {
		return birthday;
	}

	public void setBirthday(LocalDate birthday) {
		this.birthday = birthday;
	}

	public String getPol() {
		return pol;
	}

	public void setPol(String pol) {
		this.pol = pol;
	}

	public String getVes() {
		return ves;
	}

	public void setVes(String ves) {
		this.ves = ves;
	}

	public String getCvetGlaz() {
		return cvetGlaz;
	}

	public void setCvetGlaz(String cvetGlaz) {
		this.cvetGlaz = cvetGlaz;
	}

	public String getCvetVolos() {
		return cvetVolos;
	}

	public void setCvetVolos(String cvetVolos) {
		this.cvetVolos = cvetVolos;
	}

	
}
