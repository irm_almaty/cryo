package com.cryobank.medialog.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "family_tree")
public class FamilyTree {

	@Id
	@Column(name = "donor_id", nullable = false)
	private int donorManId;

	@Column(name = "vid_rodstvenika")
	private String vidRodstva;

	@Column(name = "tip_rodstva")
	private String tidRodstva;

	@Column(name = "vozrast",nullable=true)
	private String age;

	@Column(name = "alive")
	private String alive;

	@Column(name = "rasa")
	private String rasa;

	@Column(name = "nation")
	private String nation;

	@Column(name = "obrazovanie")
	private String obrazovanie;

	@Column(name = "profession")
	private String profession;

	@Column(name = "rost",nullable=true)
	private String rost;

	@Column(name = "ves",nullable=true)
	private String ves;

	@Column(name = "cvet_volos")
	private String cvetVolos;

	@Column(name = "cvet_glaz")
	private String cvetGlaz;

	@Column(name = "zdorovie")
	private String zdorovie;

	@Column(name = "prich_smerti")
	private String prichSmerti;

	public int getDonorManId() {
		return donorManId;
	}

	public void setDonorManId(int donorManId) {
		this.donorManId = donorManId;
	}

	public String getVidRodstva() {
		return vidRodstva;
	}

	public void setVidRodstva(String vidRodstva) {
		this.vidRodstva = vidRodstva;
	}

	public String getTidRodstva() {
		return tidRodstva;
	}

	public void setTidRodstva(String tidRodstva) {
		this.tidRodstva = tidRodstva;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getAlive() {
		return alive;
	}

	public void setAlive(String alive) {
		this.alive = alive;
	}

	public String getRasa() {
		return rasa;
	}

	public void setRasa(String rasa) {
		this.rasa = rasa;
	}

	public String getNation() {
		return nation;
	}

	public void setNation(String nation) {
		this.nation = nation;
	}

	public String getObrazovanie() {
		return obrazovanie;
	}

	public void setObrazovanie(String obrazovanie) {
		this.obrazovanie = obrazovanie;
	}

	public String getProfession() {
		return profession;
	}

	public void setProfession(String profession) {
		this.profession = profession;
	}

	public String getRost() {
		return rost;
	}

	public void setRost(String rost) {
		this.rost = rost;
	}

	public String getVes() {
		return ves;
	}

	public void setVes(String ves) {
		this.ves = ves;
	}

	public String getCvetVolos() {
		return cvetVolos;
	}

	public void setCvetVolos(String cvetVolos) {
		this.cvetVolos = cvetVolos;
	}

	public String getCvetGlaz() {
		return cvetGlaz;
	}

	public void setCvetGlaz(String cvetGlaz) {
		this.cvetGlaz = cvetGlaz;
	}

	public String getZdorovie() {
		return zdorovie;
	}

	public void setZdorovie(String zdorovie) {
		this.zdorovie = zdorovie;
	}

	public String getPrichSmerti() {
		return prichSmerti;
	}

	public void setPrichSmerti(String prichSmerti) {
		this.prichSmerti = prichSmerti;
	}

}
