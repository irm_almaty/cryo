package com.cryobank.medialog.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDate;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "cryo_patient")
public class PatientMedialog implements java.io.Serializable {

	@Id
	@Column(name = "patient_id")
	private int id;

	@Column(name = "iin")
	private String iin;

	@Column(name = "udv")
	private String udv;

	@Column(name = "document")
	private String document;

	@Column(name = "document_nomer")
	private String documentNumber;

	@Column(name = "document_kem_vidan")
	private String documentKemVidan;

	@Column(name = "document_date")
	private Date documentDate;

	@Column(name = "email")
	private String email;

	@Column(name = "phone_other")
	private String phoneOther;

	@Column(name = "phone_work")
	private String phoneWork;

	@Column(name = "phone")
	private String phone;

	@Column(name = "lasttname")
	private String lastname;

	@Column(name = "firstname")
	private String firstname;

	@Column(name = "patroname")
	private String patroname;

	@Column(name = "name")
	private String name;

	@Column(name = "birthday")
	private Date birthday;

	@Column(name = "country")
	private String country;

	@Column(name = "city")
	private String city;

	@Column(name = "oblast")
	private String oblast;

	@Column(name = "street")
	private String street;

	@Column(name = "home")
	private String home;

	@Column(name = "apartment")
	private String apartment;

	@Column(name = "address")
	private String address;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIin() {
		return iin;
	}

	public void setIin(String iin) {
		this.iin = iin;
	}

	public String getUdv() {
		return udv;
	}

	public void setUdv(String udv) {
		this.udv = udv;
	}

	public String getDocument() {
		return document;
	}

	public void setDocument(String document) {
		this.document = document;
	}

	public String getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	public String getDocumentKemVidan() {
		return documentKemVidan;
	}

	public void setDocumentKemVidan(String documentKemVidan) {
		this.documentKemVidan = documentKemVidan;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneOther() {
		return phoneOther;
	}

	public void setPhoneOther(String phoneOther) {
		this.phoneOther = phoneOther;
	}

	public String getPhoneWork() {
		return phoneWork;
	}

	public void setPhoneWork(String phoneWork) {
		this.phoneWork = phoneWork;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getPatroname() {
		return patroname;
	}

	public void setPatroname(String patroname) {
		this.patroname = patroname;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getOblast() {
		return oblast;
	}

	public void setOblast(String oblast) {
		this.oblast = oblast;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getHome() {
		return home;
	}

	public void setHome(String home) {
		this.home = home;
	}

	public String getApartment() {
		return apartment;
	}

	public void setApartment(String apartment) {
		this.apartment = apartment;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getDocumentDate() {
		return documentDate;
	}

	public void setDocumentDate(Date documentDate) {
		this.documentDate = documentDate;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

}
