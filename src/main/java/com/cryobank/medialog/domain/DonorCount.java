package com.cryobank.medialog.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class DonorCount {

	@Id
	@Column(name = "sperm_count")
	private Long spermCount;

	@Column(name = "oocyte_count")
	private Long oocyteCount;

	@Column(name = "surmom_count")
	private Long surmomCount;

	public DonorCount() {

	}

	public DonorCount(Long spermCount, Long oocyteCount, Long surmomCount) {
		this.spermCount = spermCount;
		this.oocyteCount = oocyteCount;
		this.surmomCount = surmomCount;
	}

	public Long getSpermCount() {
		return spermCount;
	}

	public void setSpermCount(Long spermCount) {
		this.spermCount = spermCount;
	}

	public Long getOocyteCount() {
		return oocyteCount;
	}

	public void setOocyteCount(Long oocyteCount) {
		this.oocyteCount = oocyteCount;
	}

	public Long getSurmomCount() {
		return surmomCount;
	}

	public void setSurmomCount(Long surmomCount) {
		this.surmomCount = surmomCount;
	}

}
