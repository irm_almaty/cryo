package com.cryobank.medialog.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.Type;
import org.joda.time.LocalDate;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "donor_woman")
public class DonorWomanMedialog {

	@Id
	@Column(name = "donor_id")
	private int donorWomanId;

	@Column(name = "name_donor")
	private String donorName;

	@Column(name = "phone_number")
	private String phoneNumber;

	@Column(name = "code_name")
	private String donorCode;

	@Column(name = "vozrast")
	private String vozrast;

	@Column(name = "citizen")
	private String citizen;

	@Column(name = "nation")
	private String nation;

	@Column(name = "rod")
	private String rod;

	@Column(name = "zodiak")
	private String zodiak;

	@Column(name = "obrazovanie")
	private String obrazovanie;

	@Column(name = "profession")
	private String profession;

	@Column(name = "gruppa_krovi")
	private String grKrovi;

	@Column(name = "rezus_faktor")
	private String rezus;

	@Column(name = "fam_poloj")
	private String famPoloj;

	@Column(name = "rost")
	private String rost;

	@Column(name = "ves")
	private String ves;

	@Column(name = "foot_size")
	private String footSize;

	@Column(name = "razmer_odejdi")
	private String clothSize;

	@Column(name = "skin_color")
	private String skinColor;

	@Column(name = "body_type")
	private String bodyType;

	@Column(name = "tip_volos")
	private String tipVolos;

	@Column(name = "cvet_volos")
	private String cvetVolos;

	@Column(name = "lico")
	private String lico;

	@Column(name = "lob")
	private String lob;

	@Column(name = "razrez_glaz")
	private String razrezGlaz;

	@Column(name = "cvet_glaz")
	private String cvetGlaz;

	@Column(name = "skuli")
	private String skuli;

	@Column(name = "nos")
	private String nos;

	@Column(name = "gubi")
	private String gubi;

	@Column(name = "alcogol")
	private String alcogol;

	@Column(name = "kurenie")
	private String kurenie;

	@Column(name = "zrenie")
	private String zrenie;

	@Column(name = "noshenie_ochkov")
	private String ochki;

	@Column(name = "glav_ruka")
	private String glavHand;

	@Column(name = "harak_sebya")
	private String harakSebya;

	@Column(name = "sil_harak")
	private String silHarak;

	@Column(name = "slab_harak")
	private String slabHarak;

	@Column(name = "sport")
	private String sport;

	@Column(name = "hobbi")
	private String hobbi;

	@Column(name = "lub_cvet")
	private String lubCvet;

	@Column(name = "igra_instr")
	private String igraNaInstr;

	@Column(name = "lub_blyudo")
	private String lubBlyudo;

	@Column(name = "cennosti_live")
	private String cennostiLive;

	@Column(name = "cel_live")
	private String celLive;

	@Column(name = "allergiya")
	private String allergiya;

	@Column(name = "abort")
	private String abort;

	@Column(name = "perenes_zabol")
	private String perenesZabol;

	@Column(name = "perenes_zppp")
	private String perenesZPPP;

	@Column(name = "precendent")
	private String precendent;

	@Column(name = "date_zapoln")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
	private LocalDate zapolnDate;

	@Column(name = "date_create")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
	private LocalDate createDate;

	public int getDonorWomanId() {
		return donorWomanId;
	}

	public void setDonorWomanId(int donorWomanId) {
		this.donorWomanId = donorWomanId;
	}

	public String getDonorName() {
		return donorName;
	}

	public void setDonorName(String donorName) {
		this.donorName = donorName;
	}

	
	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getDonorCode() {
		return donorCode;
	}

	public void setDonorCode(String donorCode) {
		this.donorCode = donorCode;
	}

	public String getVozrast() {
		return vozrast;
	}

	public void setVozrast(String vozrast) {
		this.vozrast = vozrast;
	}

	public String getCitizen() {
		return citizen;
	}

	public void setCitizen(String citizen) {
		this.citizen = citizen;
	}

	public String getNation() {
		return nation;
	}

	public void setNation(String nation) {
		this.nation = nation;
	}

	public String getRod() {
		return rod;
	}

	public void setRod(String rod) {
		this.rod = rod;
	}

	public String getZodiak() {
		return zodiak;
	}

	public void setZodiak(String zodiak) {
		this.zodiak = zodiak;
	}

	public String getObrazovanie() {
		return obrazovanie;
	}

	public void setObrazovanie(String obrazovanie) {
		this.obrazovanie = obrazovanie;
	}

	public String getProfession() {
		return profession;
	}

	public void setProfession(String profession) {
		this.profession = profession;
	}

	public String getGrKrovi() {
		return grKrovi;
	}

	public void setGrKrovi(String grKrovi) {
		this.grKrovi = grKrovi;
	}

	public String getRezus() {
		return rezus;
	}

	public void setRezus(String rezus) {
		this.rezus = rezus;
	}

	public String getFamPoloj() {
		return famPoloj;
	}

	public void setFamPoloj(String famPoloj) {
		this.famPoloj = famPoloj;
	}

	public String getRost() {
		return rost;
	}

	public void setRost(String rost) {
		this.rost = rost;
	}

	public String getVes() {
		return ves;
	}

	public void setVes(String ves) {
		this.ves = ves;
	}

	public String getFootSize() {
		return footSize;
	}

	public void setFootSize(String footSize) {
		this.footSize = footSize;
	}

	public String getClothSize() {
		return clothSize;
	}

	public void setClothSize(String clothSize) {
		this.clothSize = clothSize;
	}

	public String getSkinColor() {
		return skinColor;
	}

	public void setSkinColor(String skinColor) {
		this.skinColor = skinColor;
	}

	public String getBodyType() {
		return bodyType;
	}

	public void setBodyType(String bodyType) {
		this.bodyType = bodyType;
	}

	public String getTipVolos() {
		return tipVolos;
	}

	public void setTipVolos(String tipVolos) {
		this.tipVolos = tipVolos;
	}

	public String getCvetVolos() {
		return cvetVolos;
	}

	public void setCvetVolos(String cvetVolos) {
		this.cvetVolos = cvetVolos;
	}

	public String getLico() {
		return lico;
	}

	public void setLico(String lico) {
		this.lico = lico;
	}

	public String getLob() {
		return lob;
	}

	public void setLob(String lob) {
		this.lob = lob;
	}

	public String getRazrezGlaz() {
		return razrezGlaz;
	}

	public void setRazrezGlaz(String razrezGlaz) {
		this.razrezGlaz = razrezGlaz;
	}

	public String getCvetGlaz() {
		return cvetGlaz;
	}

	public void setCvetGlaz(String cvetGlaz) {
		this.cvetGlaz = cvetGlaz;
	}

	public String getSkuli() {
		return skuli;
	}

	public void setSkuli(String skuli) {
		this.skuli = skuli;
	}

	public String getNos() {
		return nos;
	}

	public void setNos(String nos) {
		this.nos = nos;
	}

	public String getGubi() {
		return gubi;
	}

	public void setGubi(String gubi) {
		this.gubi = gubi;
	}

	public String getAlcogol() {
		return alcogol;
	}

	public void setAlcogol(String alcogol) {
		this.alcogol = alcogol;
	}

	public String getKurenie() {
		return kurenie;
	}

	public void setKurenie(String kurenie) {
		this.kurenie = kurenie;
	}

	public String getZrenie() {
		return zrenie;
	}

	public void setZrenie(String zrenie) {
		this.zrenie = zrenie;
	}

	public String getOchki() {
		return ochki;
	}

	public void setOchki(String ochki) {
		this.ochki = ochki;
	}

	public String getGlavHand() {
		return glavHand;
	}

	public void setGlavHand(String glavHand) {
		this.glavHand = glavHand;
	}

	public String getHarakSebya() {
		return harakSebya;
	}

	public void setHarakSebya(String harakSebya) {
		this.harakSebya = harakSebya;
	}

	public String getSilHarak() {
		return silHarak;
	}

	public void setSilHarak(String silHarak) {
		this.silHarak = silHarak;
	}

	public String getSlabHarak() {
		return slabHarak;
	}

	public void setSlabHarak(String slabHarak) {
		this.slabHarak = slabHarak;
	}

	public String getSport() {
		return sport;
	}

	public void setSport(String sport) {
		this.sport = sport;
	}

	public String getHobbi() {
		return hobbi;
	}

	public void setHobbi(String hobbi) {
		this.hobbi = hobbi;
	}

	public String getLubCvet() {
		return lubCvet;
	}

	public void setLubCvet(String lubCvet) {
		this.lubCvet = lubCvet;
	}

	public String getIgraNaInstr() {
		return igraNaInstr;
	}

	public void setIgraNaInstr(String igraNaInstr) {
		this.igraNaInstr = igraNaInstr;
	}

	public String getLubBlyudo() {
		return lubBlyudo;
	}

	public void setLubBlyudo(String lubBlyudo) {
		this.lubBlyudo = lubBlyudo;
	}

	public String getCennostiLive() {
		return cennostiLive;
	}

	public void setCennostiLive(String cennostiLive) {
		this.cennostiLive = cennostiLive;
	}

	public String getCelLive() {
		return celLive;
	}

	public void setCelLive(String celLive) {
		this.celLive = celLive;
	}

	public String getAllergiya() {
		return allergiya;
	}

	public void setAllergiya(String allergiya) {
		this.allergiya = allergiya;
	}

	public String getAbort() {
		return abort;
	}

	public void setAbort(String abort) {
		this.abort = abort;
	}

	public String getPerenesZabol() {
		return perenesZabol;
	}

	public void setPerenesZabol(String perenesZabol) {
		this.perenesZabol = perenesZabol;
	}

	public String getPerenesZPPP() {
		return perenesZPPP;
	}

	public void setPerenesZPPP(String perenesZPPP) {
		this.perenesZPPP = perenesZPPP;
	}

	public String getPrecendent() {
		return precendent;
	}

	public void setPrecendent(String precendent) {
		this.precendent = precendent;
	}

	public LocalDate getZapolnDate() {
		return zapolnDate;
	}

	public void setZapolnDate(LocalDate zapolnDate) {
		this.zapolnDate = zapolnDate;
	}

	public LocalDate getCreateDate() {
		return createDate;
	}

	public void setCreateDate(LocalDate createDate) {
		this.createDate = createDate;
	}

}
