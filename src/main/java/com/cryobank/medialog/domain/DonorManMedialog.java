package com.cryobank.medialog.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "donor_man_test")
public class DonorManMedialog {
		
	@Id
	@Column(name = "donor_id", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int donorManId;

	@Column(name = "name_donor")
	private String donorName;
	
	@Column(name = "code_name")
	private String donorCode;
	
	@Column(name = "cvet_volos")
	private String cvetVolos;
	
	@Column(name = "tip_volos")
	private String tipVolos;
	
	@Column(name = "lico")
	private String lico;
	
	@Column(name = "lob")
	private String lob;
	
	@Column(name = "skuli")
	private String skuli;
	
	@Column(name = "nos")
	private String nos;
	
	@Column(name = "gubi")
	private String gubi;
	
	@Column(name = "cvet_glaz")
	private String cvetGlaz;
	
	@Column(name = "razrez_glaz")
	private String razrezGlaz;
	
	@Column(name = "rost")
	private String rost;
	
	@Column(name = "ves")
	private String ves;
		
	@Column(name = "cvet_koji")
	private String cvetKoji;
	
	@Column(name = "razmer_odejdi")
	private String razmerOdejdi;
	
	@Column(name = "razmer_obuv")
	private String razmerObuvi;
	
	@Column(name = "nation")
	private String nation;
	
	@Column(name = "rod")
	private String rod;
	
	@Column(name = "zodiak")
	private String zodiak;
	
	@Column(name = "obrazovanie")
	private String obrazovanie;
	
	@Column(name = "profession")
	private String profession;
	
	@Column(name = "gruppa_krovi")
	private String gruppaKrovi;
	
	@Column(name = "rezus_faktor")
	private String rezusFaktor;
	
	@Column(name = "info_donor")
	private String infoDonor;
	

	@Column(name = "sostoyanie_zreniya")
	private String sostZreniya;
	
	@Column(name = "noshenie_ochkov")
	private String noshenieOchkov;
	
	@Column(name = "glav_ruka")
	private String glavRuka;
	
	@Column(name = "sluzhba_armiya")
	private String sluzhbaArmiya;
	
	@Column(name = "kto_ponature")
	private String ktoPoNature;
	
	@Column(name = "slova_osebe")
	private String slovaOSebe;
	
	@Column(name = "harakter_silnii")
	private String harakterSilnii;
	
	@Column(name = "harakter_slabii")
	private String harakterSlabii;
	
	@Column(name = "sport_detstvo")
	private String sportDetstvo;
	
	@Column(name = "hobbi")
	private String hobbi;
	
	@Column(name = "lub_cvet")
	private String lubimCvet;
	
	@Column(name = "igra_muz")
	private String igraMuz;
	
	@Column(name = "lub_animal")
	private String lubimAnimal;
	
	@Column(name = "lub_blyudo")
	private String lubimBlyudo;
	
	@Column(name = "lub_napravmuz")
	private String lubimNapravMuz;
	
	@Column(name = "cennosti_live")
	private String cennostiLive;
	
	@Column(name = "prof_predkov")
	private String profPredkov;
	
	@Column(name = "alkogol")
	private String alkogol;
	
	@Column(name = "kurenie")
	private String kurenie;
	
	@Column(name = "allergiya")
	private String allergiya;
	
	@Column(name = "kariotip")
	private String kariotip;
	
	@Column(name = "zakl_terapevt")
	private String zaklTerapevt;
	
	@Column(name = "zakl_urolog")
	private String zaklUrolog;

	
	public String getSostZreniya() {
		return sostZreniya;
	}

	public void setSostZreniya(String sostZreniya) {
		this.sostZreniya = sostZreniya;
	}

	public String getAllergiya() {
		return allergiya;
	}

	public void setAllergiya(String allergiya) {
		this.allergiya = allergiya;
	}

	public String getDonorName() {
		return donorName;
	}

	public void setDonorName(String donorName) {
		this.donorName = donorName;
	}

	
	public String getDonorCode() {
		return donorCode;
	}

	public void setDonorCode(String donorCode) {
		this.donorCode = donorCode;
	}

	public int getDonorManId() {
		return donorManId;
	}

	public void setDonorManId(int donorManId) {
		this.donorManId = donorManId;
	}

	public String getCvetVolos() {
		return cvetVolos;
	}

	public void setCvetVolos(String cvetVolos) {
		this.cvetVolos = cvetVolos;
	}

	public String getTipVolos() {
		return tipVolos;
	}

	public void setTipVolos(String tipVolos) {
		this.tipVolos = tipVolos;
	}

	public String getLico() {
		return lico;
	}

	public void setLico(String lico) {
		this.lico = lico;
	}

	public String getLob() {
		return lob;
	}

	public void setLob(String lob) {
		this.lob = lob;
	}

	public String getSkuli() {
		return skuli;
	}

	public void setSkuli(String skuli) {
		this.skuli = skuli;
	}

	public String getNos() {
		return nos;
	}

	public void setNos(String nos) {
		this.nos = nos;
	}

	public String getGubi() {
		return gubi;
	}

	public void setGubi(String gubi) {
		this.gubi = gubi;
	}

	public String getCvetGlaz() {
		return cvetGlaz;
	}

	public void setCvetGlaz(String cvetGlaz) {
		this.cvetGlaz = cvetGlaz;
	}

	public String getRazrezGlaz() {
		return razrezGlaz;
	}

	public void setRazrezGlaz(String razrezGlaz) {
		this.razrezGlaz = razrezGlaz;
	}

	public String getRost() {
		return rost;
	}

	public void setRost(String rost) {
		this.rost = rost;
	}

	public String getVes() {
		return ves;
	}

	public void setVes(String ves) {
		this.ves = ves;
	}

	public String getCvetKoji() {
		return cvetKoji;
	}

	public void setCvetKoji(String cvetKoji) {
		this.cvetKoji = cvetKoji;
	}

	public String getRazmerOdejdi() {
		return razmerOdejdi;
	}

	public void setRazmerOdejdi(String razmerOdejdi) {
		this.razmerOdejdi = razmerOdejdi;
	}

	public String getRazmerObuvi() {
		return razmerObuvi;
	}

	public void setRazmerObuvi(String razmerObuvi) {
		this.razmerObuvi = razmerObuvi;
	}

	public String getNation() {
		return nation;
	}

	public void setNation(String nation) {
		this.nation = nation;
	}

	public String getRod() {
		return rod;
	}

	public void setRod(String rod) {
		this.rod = rod;
	}

	public String getZodiak() {
		return zodiak;
	}

	public void setZodiak(String zodiak) {
		this.zodiak = zodiak;
	}

	public String getObrazovanie() {
		return obrazovanie;
	}

	public void setObrazovanie(String obrazovanie) {
		this.obrazovanie = obrazovanie;
	}

	public String getProfession() {
		return profession;
	}

	public void setProfession(String profession) {
		this.profession = profession;
	}

	public String getGruppaKrovi() {
		return gruppaKrovi;
	}

	public void setGruppaKrovi(String gruppaKrovi) {
		this.gruppaKrovi = gruppaKrovi;
	}

	public String getRezusFaktor() {
		return rezusFaktor;
	}

	public void setRezusFaktor(String rezusFaktor) {
		this.rezusFaktor = rezusFaktor;
	}

	public String getInfoDonor() {
		return infoDonor;
	}

	public void setInfoDonor(String infoDonor) {
		this.infoDonor = infoDonor;
	}

	

	public String getNoshenieOchkov() {
		return noshenieOchkov;
	}

	public void setNoshenieOchkov(String noshenieOchkov) {
		this.noshenieOchkov = noshenieOchkov;
	}

	public String getGlavRuka() {
		return glavRuka;
	}

	public void setGlavRuka(String glavRuka) {
		this.glavRuka = glavRuka;
	}

	public String getSluzhbaArmiya() {
		return sluzhbaArmiya;
	}

	public void setSluzhbaArmiya(String sluzhbaArmiya) {
		this.sluzhbaArmiya = sluzhbaArmiya;
	}

	public String getKtoPoNature() {
		return ktoPoNature;
	}

	public void setKtoPoNature(String ktoPoNature) {
		this.ktoPoNature = ktoPoNature;
	}

	public String getSlovaOSebe() {
		return slovaOSebe;
	}

	public void setSlovaOSebe(String slovaOSebe) {
		this.slovaOSebe = slovaOSebe;
	}

	public String getHarakterSilnii() {
		return harakterSilnii;
	}

	public void setHarakterSilnii(String harakterSilnii) {
		this.harakterSilnii = harakterSilnii;
	}

	public String getHarakterSlabii() {
		return harakterSlabii;
	}

	public void setHarakterSlabii(String harakterSlabii) {
		this.harakterSlabii = harakterSlabii;
	}

	public String getSportDetstvo() {
		return sportDetstvo;
	}

	public void setSportDetstvo(String sportDetstvo) {
		this.sportDetstvo = sportDetstvo;
	}

	public String getHobbi() {
		return hobbi;
	}

	public void setHobbi(String hobbi) {
		this.hobbi = hobbi;
	}

	public String getLubimCvet() {
		return lubimCvet;
	}

	public void setLubimCvet(String lubimCvet) {
		this.lubimCvet = lubimCvet;
	}

	public String getIgraMuz() {
		return igraMuz;
	}

	public void setIgraMuz(String igraMuz) {
		this.igraMuz = igraMuz;
	}

	public String getLubimAnimal() {
		return lubimAnimal;
	}

	public void setLubimAnimal(String lubimAnimal) {
		this.lubimAnimal = lubimAnimal;
	}

	public String getLubimBlyudo() {
		return lubimBlyudo;
	}

	public void setLubimBlyudo(String lubimBlyudo) {
		this.lubimBlyudo = lubimBlyudo;
	}

	public String getLubimNapravMuz() {
		return lubimNapravMuz;
	}

	public void setLubimNapravMuz(String lubimNapravMuz) {
		this.lubimNapravMuz = lubimNapravMuz;
	}

	public String getCennostiLive() {
		return cennostiLive;
	}

	public void setCennostiLive(String cennostiLive) {
		this.cennostiLive = cennostiLive;
	}

	public String getProfPredkov() {
		return profPredkov;
	}

	public void setProfPredkov(String profPredkov) {
		this.profPredkov = profPredkov;
	}

	public String getAlkogol() {
		return alkogol;
	}

	public void setAlkogol(String alkogol) {
		this.alkogol = alkogol;
	}

	public String getKurenie() {
		return kurenie;
	}

	public void setKurenie(String kurenie) {
		this.kurenie = kurenie;
	}

	

	public String getKariotip() {
		return kariotip;
	}

	public void setKariotip(String kariotip) {
		this.kariotip = kariotip;
	}

	public String getZaklTerapevt() {
		return zaklTerapevt;
	}

	public void setZaklTerapevt(String zaklTerapevt) {
		this.zaklTerapevt = zaklTerapevt;
	}

	public String getZaklUrolog() {
		return zaklUrolog;
	}

	public void setZaklUrolog(String zaklUrolog) {
		this.zaklUrolog = zaklUrolog;
	}
	

}
