package com.cryobank.medialog.dao;

import com.cryobank.medialog.domain.DonorCount;

public interface CryoDao {

	DonorCount getDonorCount();

}
