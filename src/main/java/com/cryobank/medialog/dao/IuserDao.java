package com.cryobank.medialog.dao;

import java.util.Collections;
import java.util.List;

import com.cryobank.medialog.domain.CountValue;
import com.cryobank.medialog.domain.Iuser;
import com.cryobank.postgres.domain.PatientStatus;
import com.cryobank.util.Constants;

public interface IuserDao {

	public Iuser getIuserByName(String iuserName);

	public Iuser getEnableIuserByName(String iuserName);

	List<Iuser> getIuserListByStatus(PatientStatus patientStatus);

	Iuser getIuserById(Integer iuserId);

	CountValue getByIuserStatusCount();

}
