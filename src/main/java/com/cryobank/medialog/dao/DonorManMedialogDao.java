package com.cryobank.medialog.dao;

import java.util.List;

import com.cryobank.medialog.domain.DonorManMedialog;
import com.cryobank.medialog.domain.Parametr;
import com.cryobank.postgres.domain.Order;

public interface DonorManMedialogDao {

	DonorManMedialog getDonor(int id);

	List<DonorManMedialog> getAllDonors();

	List<DonorManMedialog> getAllDonorsPagination(int offset, int maxResult);
	
	List<DonorManMedialog> getAllDonorsPaginationByDonorCode(int offset, int maxResult,String code);

	List<Parametr> getAllSpermDonorBloodType();

	List<Parametr> getAllSpermDonorHairColor();

	List<Parametr> getAllSpermDonorEyesColor();

	List<Parametr> getAllSpermDonorNation();

	int getAllSpermCountBySearchParametr(String searchParametr);

	List<DonorManMedialog> getAllSpermDonorsPaginationBySearchParametr(int offset, int maxResult, String searchParametr);
}
