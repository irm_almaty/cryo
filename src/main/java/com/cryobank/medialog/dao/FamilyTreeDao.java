package com.cryobank.medialog.dao;

import com.cryobank.medialog.domain.FamilyTree;

public interface FamilyTreeDao {

	FamilyTree getMother(int id);

	FamilyTree getMotherGrandma(int id);

	FamilyTree getMotherGrandpa(int id);

	FamilyTree getFather(int id);

	FamilyTree getFatherGrandma(int id);

	FamilyTree getFatherGrandpa(int id);

	FamilyTree getBrother(int id);

	FamilyTree getSister(int id);

}
