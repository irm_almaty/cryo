package com.cryobank.medialog.dao;

import java.util.List;

import com.cryobank.medialog.domain.CountValue;
import com.cryobank.medialog.domain.PatientMedialog;

public interface PatientMedialogDao {
	
	public PatientMedialog getPatientMedialog(int id);

	public List<PatientMedialog> getAllPatientMedialog();
	
	public List<PatientMedialog> getPatientMedialogByEmail(String email);
	
	public List<PatientMedialog> getPatientMedialogByPhone(String phone);

	public List<PatientMedialog> getPatientMedialogByName(String firstname,String lastname);

	PatientMedialog getPatientMedialogByIuserId(Integer iuserId);

	CountValue getByPatientStatusCount();

}
