package com.cryobank.medialog.dao;

import java.util.List;

import com.cryobank.medialog.domain.DonorChildMedialog;

public interface DonorChildMedialogDao {
	
	List<DonorChildMedialog> listChilds(int donorId);

}
