package com.cryobank.medialog.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.cryobank.medialog.dao.CryoDao;
import com.cryobank.medialog.domain.CountValue;
import com.cryobank.medialog.domain.DonorCount;
import com.cryobank.util.HibernateMedialogUtil;

@Repository
@Transactional
public class CryoDaoImpl implements CryoDao {

	@Autowired
	HibernateMedialogUtil hibernateUtil;

	@Override
	public DonorCount getDonorCount() {
		StringBuilder query = new StringBuilder();
		query.append("select");
		query.append("(select count(*) from donor_man_test) as sperm_count,");
		query.append("(select count(*) from donor_woman dw where dw.donor_type in (1,4)) as oocyte_count,");
		query.append("(select count(*) from donor_woman dw where dw.donor_type in (3,4)) as surmom_count");
		return (DonorCount) hibernateUtil.getByValue(query.toString(), DonorCount.class);
	}

}
