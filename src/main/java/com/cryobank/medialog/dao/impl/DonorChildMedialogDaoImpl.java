package com.cryobank.medialog.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cryobank.medialog.dao.DonorChildMedialogDao;
import com.cryobank.medialog.domain.DonorChildMedialog;
import com.cryobank.util.HibernateMedialogUtil;

@Repository
public class DonorChildMedialogDaoImpl implements DonorChildMedialogDao {

	@Autowired
	private HibernateMedialogUtil hibernateMedialogUtil;

	@SuppressWarnings("unchecked")
	@Override
	public List<DonorChildMedialog> listChilds(int donorId) {

		return hibernateMedialogUtil.fetchAllSQL("select * from donor_child where parent_id=" + donorId,DonorChildMedialog.class);

	}

}
