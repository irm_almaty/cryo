package com.cryobank.medialog.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cryobank.medialog.dao.PatientMedialogDao;
import com.cryobank.medialog.domain.CountValue;
import com.cryobank.medialog.domain.PatientMedialog;
import com.cryobank.util.HibernateMedialogUtil;

@Repository
public class PatientMedialogDaoImpl implements PatientMedialogDao {

	@Autowired
	HibernateMedialogUtil hibernateMedialogUtil;

	@Override
	public PatientMedialog getPatientMedialog(int id) {

		return hibernateMedialogUtil.fetchById(id, PatientMedialog.class);
	}

	@Override
	public List<PatientMedialog> getAllPatientMedialog() {

		return hibernateMedialogUtil.fetchAll(PatientMedialog.class);
	}

	@Override
	public List<PatientMedialog> getPatientMedialogByEmail(String email) {
		String query = "select* from cryo_patient where email like '%" + email + "%'";
		return hibernateMedialogUtil.fetchAllSQL(query, PatientMedialog.class);
	}

	public List<PatientMedialog> getPatientMedialogById(int patientId) {
		String query = "select* from cryo_patient where patient_id =" + patientId;
		return hibernateMedialogUtil.fetchAllSQL(query, PatientMedialog.class);
	}

	@Override
	public List<PatientMedialog> getPatientMedialogByPhone(String phone) {
		String query = "select * from cryo_patient where phone like '%" + phone + "%'";
		return hibernateMedialogUtil.fetchAllSQL(query, PatientMedialog.class);
	}

	@Override
	public PatientMedialog getPatientMedialogByIuserId(Integer iuserId) {
		String query = "select * from cryo_patient where patient_id = " + iuserId;
		List<PatientMedialog> list = hibernateMedialogUtil.fetchAllSQL(query, PatientMedialog.class);
		if (!list.isEmpty()) {
			PatientMedialog patientMedialog = list.get(0);
			return patientMedialog;
		}
		return null;
	}

	@Override
	public List<PatientMedialog> getPatientMedialogByName(String firstname, String lastname) {
		String query = "select * from cryo_patient where firstname like '%" + firstname + "%' "
				+ "and lasttname like '%" + lastname + "%'";
		return hibernateMedialogUtil.fetchAllSQL(query, PatientMedialog.class);
	}

	@Override
	public CountValue getByPatientStatusCount() {
		StringBuilder query = new StringBuilder();
		query.append("select ( ");
		query.append("select count(*) from cryo_user where enable_iuser=1) as active_value,");
		query.append("(select count(*) from cryo_user where enable_iuser=2) as archive_value");
		return (CountValue) hibernateMedialogUtil.getByValue(query.toString(), CountValue.class);
	}

}
