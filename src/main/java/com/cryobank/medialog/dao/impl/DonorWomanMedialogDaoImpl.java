package com.cryobank.medialog.dao.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.cryobank.medialog.dao.DonorWomanMedialogDao;
import com.cryobank.medialog.domain.DonorManMedialog;
import com.cryobank.medialog.domain.DonorWomanMedialog;
import com.cryobank.medialog.domain.Parametr;
import com.cryobank.util.HibernateMedialogUtil;

@Repository
public class DonorWomanMedialogDaoImpl implements DonorWomanMedialogDao {

	@Autowired
	private HibernateMedialogUtil hibernateMedialogUtil;

	private final static String DONOR_TYPE_OOCYTE = "(1,4)";
	private final static String DONOR_TYPE_SURMOM = "(3,4)";
	private final static int DONOR_TYPE_OOCYTE_SURMOM = 4;

	@Override
	public DonorWomanMedialog getDonor(int id) {
		return hibernateMedialogUtil.fetchById(id, DonorWomanMedialog.class);
	}

	@Override
	public List<DonorWomanMedialog> getAllOocyteDonors() {
		String query = "select * from donor_woman where donor_type in "+DONOR_TYPE_OOCYTE;
		return hibernateMedialogUtil.fetchAllSQL(query, DonorWomanMedialog.class);

	}

	@Override
	public List<DonorWomanMedialog> getAllSurMomDonors() {
		String query = "select * from donor_woman where donor_type in "+DONOR_TYPE_SURMOM;
		return hibernateMedialogUtil.fetchAllSQL(query, DonorWomanMedialog.class);
	}

	@Override
	public List<DonorWomanMedialog> getAllSurMomDonorsPagination(int offset, int maxResult) {
		String query = "select * from donor_woman where donor_type in " + DONOR_TYPE_SURMOM;
		return hibernateMedialogUtil.getAllPaginateByQuery(DonorWomanMedialog.class, offset, maxResult, query);
	}

	@Override
	public List<DonorWomanMedialog> getAllSurMomDonorsPaginationByDonorCode(int offset, int maxResult, String code) {
		if (code == null) {
			code = "";
		}
		return hibernateMedialogUtil.getAllPaginateByQuery(DonorWomanMedialog.class, offset, maxResult,
				"select * from donor_woman where code_name like '%" + code + "%' and donor_type in" + DONOR_TYPE_SURMOM);
	}

	@Override
	public List<DonorWomanMedialog> getAllOocyteDonorsPagination(int offset, int maxResult) {
		String query = "select * from donor_woman where donor_type in " + DONOR_TYPE_OOCYTE;
		return hibernateMedialogUtil.getAllPaginateByQuery(DonorWomanMedialog.class, offset, maxResult, query);
	}

	@Override
	public List<DonorWomanMedialog> getAllOocyteDonorsPaginationBySearchParametr(int offset, int maxResult,
			String searchParametr) {
		if (searchParametr == null) {
			searchParametr = "";
		}
		return hibernateMedialogUtil.getAllPaginateByQuery(DonorWomanMedialog.class, offset, maxResult,
				"select * from donor_woman where donor_type in " + DONOR_TYPE_OOCYTE + searchParametr);
	}

	@Override
	public int getAllOocyteCountBySearchParametr(String searchParametr) {
		if (searchParametr == null) {
			searchParametr = "";
		}
		String query = "select * from donor_woman where donor_type in " + DONOR_TYPE_OOCYTE + searchParametr;
		return hibernateMedialogUtil.fetchAllSQL(query, DonorWomanMedialog.class).size();

	}

	@Override
	public Integer getAllSurMomCountBySearchParametr(String searchParametr) {
		if (searchParametr == null) {
			searchParametr = "";
		}
		String query = "select count(*) from donor_woman where donor_type in " + DONOR_TYPE_SURMOM + searchParametr;
		return (Integer) hibernateMedialogUtil.getCount(query);
	}

	@Override
	public List<DonorWomanMedialog> getAllSurMomDonorsPaginationBySearchParametr(int offset, int maxResult,
			String searchParametr) {
		if (searchParametr == null) {
			searchParametr = "";
		}
		return hibernateMedialogUtil.getAllPaginateByQuery(DonorWomanMedialog.class, offset, maxResult,
				"select * from donor_woman where donor_type in " + DONOR_TYPE_SURMOM + searchParametr);
	}

	@Override
	public List<DonorWomanMedialog> getAllOocyteDonorsPaginationByDonorCode(int offset, int maxResult, String code) {
		if (code == null) {
			code = "";
		}
		return hibernateMedialogUtil.getAllPaginateByQuery(DonorWomanMedialog.class, offset, maxResult,
				"select * from donor_woman where code_name like '%" + code + "%' and donor_type in " + DONOR_TYPE_OOCYTE);
	}

	// select nation from [Embrio_670].[dbo].[donor_woman] where nation is not
	// null group by nation order by nation ASC

	@Override
	public List<Parametr> getAllOocyteDonorNation() {
		String query = " select LOWER(nation) as code from [Embrio_670].[dbo].[donor_woman] where nation is not null  group by nation order by nation ASC";
		return hibernateMedialogUtil.fetchAllSQL(query, Parametr.class);

	}

	@Override
	public List<Parametr> getAllOocyteDonorHairColor() {
		String query = "select LOWER(cvet_volos) as code from [Embrio_670].[dbo].[donor_woman] where cvet_volos is not null  group by cvet_volos order by cvet_volos ASC";
		return hibernateMedialogUtil.fetchAllSQL(query, Parametr.class);

	}

	@Override
	public List<Parametr> getAllOocyteDonorBloodType() {
		String query = "select UPPER(gruppa_krovi) as code from [Embrio_670].[dbo].[donor_woman] where gruppa_krovi is not null  group by gruppa_krovi order by gruppa_krovi ASC";
		return hibernateMedialogUtil.fetchAllSQL(query, Parametr.class);

	}

	@Override
	public List<Parametr> getAllOocyteDonorEyesColor() {
		String query = "select LOWER(cvet_glaz) as code from [Embrio_670].[dbo].[donor_woman] where cvet_glaz is not null  group by cvet_glaz order by cvet_glaz ASC";
		return hibernateMedialogUtil.fetchAllSQL(query, Parametr.class);

	}

}
