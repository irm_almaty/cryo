package com.cryobank.medialog.dao.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import com.cryobank.medialog.dao.IuserDao;
import com.cryobank.medialog.domain.CountValue;
import com.cryobank.medialog.domain.Iuser;
import com.cryobank.postgres.domain.PatientStatus;
import com.cryobank.util.Constants;
import com.cryobank.util.HibernateMedialogUtil;

@Repository
public class IuserDaoImpl implements IuserDao {

	@Autowired
	private HibernateMedialogUtil hibernateUtil;

	/*
	 * @Override public Iuser getIuserByName(String iuserName) {
	 * 
	 * String queryIuser="select * from public.iuser where user_name='"+
	 * iuserName +"'";
	 * 
	 * List<Iuser> iuserList=hibernateUtil.fetchAllSQL(queryIuser,Iuser.class);
	 * 
	 * if(iuserList.size()==1){ Iuser iuserFindByName= iuserList.get(0);
	 * 
	 * return iuserFindByName; }
	 * 
	 * return null;
	 * 
	 * 
	 * }
	 * 
	 */

	@Override
	public Iuser getIuserByName(String iuserName) {

		String queryIuser = "select * from cryo_user where iuser_login='" + iuserName + "'";

		List<Iuser> iuserList = hibernateUtil.fetchAllSQL(queryIuser, Iuser.class);

		if (iuserList.size() == 1) {
			Iuser iuserFindByName = iuserList.get(0);

			return iuserFindByName;
		}

		return null;

	}

	/*
	 * @Override public Iuser getEnableIuserByName(String iuserName) { String
	 * queryIuser=
	 * "select * from public.iuser where user_enabled=true and user_name='"+
	 * iuserName +"'";
	 * 
	 * List<Iuser> iuserList=hibernateUtil.fetchAllSQL(queryIuser,Iuser.class);
	 * 
	 * if(iuserList.size()==1){ Iuser iuserFindByName= iuserList.get(0);
	 * 
	 * return iuserFindByName; }
	 * 
	 * return null; }
	 * 
	 */
	@Override
	public Iuser getEnableIuserByName(String iuserName) {
		String queryIuser = "select * from cryo_user where enable_iuser=1 and iuser_login='" + iuserName + "'";
		List<Iuser> iuserList = hibernateUtil.fetchAllSQL(queryIuser, Iuser.class);
		if (iuserList.size() == 1) {
			Iuser iuserFindByName = iuserList.get(0);
			return iuserFindByName;
		}
		return null;
	}

	@Override
	public Iuser getIuserById(Integer iuserId) {
		String queryIuser = "select * from cryo_user where  iuser_id=" + iuserId;
		List<Iuser> iuserList = hibernateUtil.fetchAllSQL(queryIuser, Iuser.class);
		if (iuserList.size() == 1) {
			Iuser iuserFindById = iuserList.get(0);
			return iuserFindById;
		}
		return null;
	}

	@Override
	public List<Iuser> getIuserListByStatus(PatientStatus patientStatus) {
		StringBuilder query = new StringBuilder();
		query.append("select * from cryo_user where enable_iuser=");
		List<Iuser> iusers;
		if (PatientStatus.ACTIVE.equals(patientStatus)) {
			query.append(Constants.IUSER_STATUS_ACTIVE);
		}
		if (PatientStatus.ARCHIVED.equals(patientStatus)) {
			query.append(Constants.IUSER_STATUS_ARCHIVED);
		}
		query.append("and iuser_id not in (" + Constants.IUSER_IGNORE + ")");
		query.append(" order by srok_pass DESC");
		iusers = hibernateUtil.fetchAllSQL(query.toString(), Iuser.class);
		if (iusers.isEmpty() && iusers == null) {
			return Collections.EMPTY_LIST;
		}
		return iusers;

	}

	@Override
	public CountValue getByIuserStatusCount() {
		StringBuilder query = new StringBuilder();
		query.append("select ( ");
		query.append(
				"select count(*) from cryo_user where enable_iuser=1  and iuser_id not in(1,2) ) as active_value,");
		query.append("(select count(*) from cryo_user where enable_iuser=2) as archive_value");
		return (CountValue) hibernateUtil.getByValue(query.toString(), CountValue.class);
	}

}
