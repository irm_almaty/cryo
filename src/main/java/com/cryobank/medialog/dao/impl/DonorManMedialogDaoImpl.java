package com.cryobank.medialog.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.cryobank.medialog.dao.DonorManMedialogDao;
import com.cryobank.medialog.domain.DonorManMedialog;
import com.cryobank.medialog.domain.DonorWomanMedialog;
import com.cryobank.medialog.domain.Parametr;
import com.cryobank.util.HibernateMedialogUtil;

@Repository
public class DonorManMedialogDaoImpl implements DonorManMedialogDao {

	@Autowired
	private HibernateMedialogUtil hibernateMedialogUtil;

	@Override
	public DonorManMedialog getDonor(int id) {

		return hibernateMedialogUtil.fetchById(id, DonorManMedialog.class);
	}

	@Override
	public List<DonorManMedialog> getAllDonors() {

		return hibernateMedialogUtil.fetchAll(DonorManMedialog.class);
	}

	@Override
	public List<DonorManMedialog> getAllDonorsPagination(int offset, int maxResult) {

		return hibernateMedialogUtil.getAllPaginate(DonorManMedialog.class, offset, maxResult);

	}

	@Override
	public List<DonorManMedialog> getAllDonorsPaginationByDonorCode(int offset, int maxResult, String code) {
		if (code == null) {
			code = "";
		}
		return hibernateMedialogUtil.getAllPaginateByQuery(DonorManMedialog.class, offset, maxResult,
				"select * from donor_man_test where donor_code like '%" + code + "%'");
	}

	@Override
	public List<Parametr> getAllSpermDonorNation() {
		String query = "select LOWER(nation) as code from [Embrio_670].[dbo].[donor_man_test] where nation is not null  group by nation order by nation ASC";
		return hibernateMedialogUtil.fetchAllSQL(query, Parametr.class);
	}

	@Override
	public List<Parametr> getAllSpermDonorEyesColor() {
		String query = "select LOWER(cvet_glaz) as code from [Embrio_670].[dbo].[donor_man_test] where cvet_glaz is not null  group by cvet_glaz order by cvet_glaz ASC";
		return hibernateMedialogUtil.fetchAllSQL(query, Parametr.class);
	}

	@Override
	public List<Parametr> getAllSpermDonorHairColor() {
		String query = "select LOWER(cvet_volos) as code from [Embrio_670].[dbo].[donor_man_test] where cvet_volos is not null  group by cvet_volos order by cvet_volos ASC";
		return hibernateMedialogUtil.fetchAllSQL(query, Parametr.class);
	}

	@Override
	public List<Parametr> getAllSpermDonorBloodType() {
		String query = " select UPPER(gruppa_krovi) as code from [Embrio_670].[dbo].[donor_man_test] where gruppa_krovi is not null  group by gruppa_krovi order by gruppa_krovi ASC";
		return hibernateMedialogUtil.fetchAllSQL(query, Parametr.class);
	}

	@Override
	public List<DonorManMedialog> getAllSpermDonorsPaginationBySearchParametr(int offset, int maxResult,
			String searchParametr) {
		if (searchParametr == null) {
			searchParametr = "";
		}
		return hibernateMedialogUtil.getAllPaginateByQuery(DonorManMedialog.class, offset, maxResult,
				"select * from donor_man_test where donor_id is not null" + searchParametr);
	}

	@Override
	public int getAllSpermCountBySearchParametr(String searchParametr) {
		if (searchParametr == null) {
			searchParametr = "";
		}
		String query = "select * from donor_man_test where donor_id is not null" + searchParametr;
		return hibernateMedialogUtil.fetchAllSQL(query, DonorManMedialog.class).size();

	}

}
