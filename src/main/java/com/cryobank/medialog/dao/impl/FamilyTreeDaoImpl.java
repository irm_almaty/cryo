package com.cryobank.medialog.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.cryobank.medialog.dao.FamilyTreeDao;
import com.cryobank.medialog.domain.FamilyTree;
import com.cryobank.util.HibernateMedialogUtil;

@Repository
public class FamilyTreeDaoImpl implements FamilyTreeDao {

	@Autowired
	private HibernateMedialogUtil hibernateMedialogUtil;

	@Override
	public FamilyTree getMother(int id) {

		return hibernateMedialogUtil.fetchFirstResultSQL(
				"select * from family_tree where vid_rodstvenika like '����' and donor_id=" + id, FamilyTree.class);
	}

	@Override
	public FamilyTree getMotherGrandma(int id) {

		return hibernateMedialogUtil.fetchFirstResultSQL(
				"select * from family_tree where vid_rodstvenika like '������� �� ������' and donor_id=" + id, FamilyTree.class);
	}

	@Override
	public FamilyTree getMotherGrandpa(int id) {

		return hibernateMedialogUtil.fetchFirstResultSQL(
				"select * from family_tree where vid_rodstvenika like '������� �� ������' and donor_id=" + id,
				FamilyTree.class);
	}

	@Override
	public FamilyTree getFather(int id) {

		return hibernateMedialogUtil.fetchFirstResultSQL(
				"select * from family_tree where vid_rodstvenika like '����' and donor_id=" + id, FamilyTree.class);

	}

	@Override
	public FamilyTree getFatherGrandma(int id) {
		return hibernateMedialogUtil.fetchFirstResultSQL(
				"select * from family_tree where vid_rodstvenika like '������� �� ����' and donor_id=" + id, FamilyTree.class);
	}

	@Override
	public FamilyTree getFatherGrandpa(int id) {
		
		return hibernateMedialogUtil.fetchFirstResultSQL(
				"select * from family_tree where vid_rodstvenika like '������� �� ����' and donor_id=" + id, FamilyTree.class);
	
	}

	@Override
	public FamilyTree getBrother(int id) {
		
		return hibernateMedialogUtil.fetchFirstResultSQL(
				"select * from family_tree where vid_rodstvenika like '����' and donor_id=" + id, FamilyTree.class);
	}

	@Override
	public FamilyTree getSister(int id) {
		return hibernateMedialogUtil.fetchFirstResultSQL(
				"select * from family_tree where vid_rodstvenika like '������' and donor_id=" + id, FamilyTree.class);
	}

}
