package com.cryobank.medialog.dao;

import java.util.List;

import com.cryobank.medialog.domain.DonorManMedialog;
import com.cryobank.medialog.domain.DonorWomanMedialog;
import com.cryobank.medialog.domain.Parametr;

public interface DonorWomanMedialogDao {

	DonorWomanMedialog getDonor(int id);
	
    List<DonorWomanMedialog> getAllSurMomDonors();
    
    List<DonorWomanMedialog> getAllSurMomDonorsPagination(int offset, int maxResult);
    
    List<DonorWomanMedialog> getAllSurMomDonorsPaginationByDonorCode(int offset, int maxResult, String code);

	List<DonorWomanMedialog> getAllOocyteDonors();
	
	List<DonorWomanMedialog> getAllOocyteDonorsPagination(int offset, int maxResult);
	
	List<DonorWomanMedialog> getAllOocyteDonorsPaginationByDonorCode(int offset, int maxResult,String code);

	List<DonorWomanMedialog> getAllOocyteDonorsPaginationBySearchParametr(int offset, int maxResult, String searchParametr);

	int getAllOocyteCountBySearchParametr(String searchParametr);

	List<Parametr> getAllOocyteDonorNation();

	List<Parametr> getAllOocyteDonorHairColor();

	List<Parametr> getAllOocyteDonorEyesColor();

	List<Parametr> getAllOocyteDonorBloodType();

	List<DonorWomanMedialog> getAllSurMomDonorsPaginationBySearchParametr(int offset, int maxResult, String searchParametr);

	Integer getAllSurMomCountBySearchParametr(String searchParametr);

}
