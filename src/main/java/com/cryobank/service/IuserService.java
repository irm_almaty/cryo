package com.cryobank.service;

import java.util.List;

import com.cryobank.medialog.domain.CountValue;
import com.cryobank.medialog.domain.Iuser;
import com.cryobank.postgres.domain.PatientStatus;

public interface IuserService {

	public Iuser getIuserByName(String iuserName);
	
	public Iuser getEnableIuserByName(String iuserName);

	List<Iuser> getIuserListByStatus(PatientStatus patientStatus);

	Iuser getIuserById(Integer iuserId);

	CountValue getIuserCountByStatus();
	
}
