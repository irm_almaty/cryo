package com.cryobank.service;

import java.util.List;

import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;

import com.cryobank.medialog.domain.DonorManMedialog;
import com.cryobank.medialog.domain.FamilyTree;
import com.cryobank.postgres.domain.Photo;
import com.cryobank.postgres.domain.Voice;

public interface DonorManService {

	public DonorManMedialog getDonor(int id);

	public List<DonorManMedialog> getAllDonors();

	public void savePhoto(Photo photo);

	public int createPhoto(Photo photo);

	public Photo updatePhoto(Photo photo);

	public void deletePhotoById(int id);

	public Photo getPhoto(int id);

	public List<Photo> getAllPhoto();

	public int createVoice(Voice voice);

	public void deleteVoiceById(int id);

	public FamilyTree getMother(int id);

	public FamilyTree getMotherGrandma(int id);

	public FamilyTree getMotherGrandpa(int id);

	public FamilyTree getFather(int id);

	public FamilyTree getFatherGrandma(int id);

	public FamilyTree getFatherGrandpa(int id);

	public FamilyTree getBrother(int id);

	public FamilyTree getSister(int id);

	//@PreAuthorize("hasRole('ROLE_ADMIN')")
	List<DonorManMedialog> getAllDonorsPagination(int offset, int maxResult);
	
	List<DonorManMedialog> getAllDonorsPaginationByCode(int offset, int maxResult,String code);

	int getAllSpermDonorCountParametr(String searchParametr);

	List<DonorManMedialog> getAllSpermDonorPaginationBySearchParametr(int offset, int maxResult, String searchParametr);

}
