package com.cryobank.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.StringUtils;

import com.cryobank.medialog.domain.Parametr;
import com.cryobank.medialog.domain.DTO.DonorSearch;
import com.cryobank.util.Constants;

public interface DonorSearchService {

	String getSearchQuery(DonorSearch donorSearch);

	List<String> getAllOocyteDonorNation();

	List<String> getAllOocyteHairColor();

	List<String> getAllOocyteEyesColor();

	List<String> getAllOocyteBloodType();

	List<String> getAllManNation();

	List<String> getAllManBloodType();

	List<String> getAllManHairColor();

	List<String> getAllManEyesColor();

	

}
