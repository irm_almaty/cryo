package com.cryobank.service;

import java.util.List;

import com.cryobank.postgres.domain.Man;

public interface ManService {
	
	public long createMan(Man man);
    public Man updateMan(Man man);
    public void deleteMan(long id);
    public List<Man> getAllMan();
    public Man getMan(long id);	
	public List<Man> getAllMans(String mansName);

}
