package com.cryobank.service;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;

import com.cryobank.medialog.domain.Iuser;

public interface UserContextService {

	Iuser getUserContext();

	void setContext(Iuser user);

}
