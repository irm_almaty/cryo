package com.cryobank.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.cryobank.medialog.domain.FamilyTree;
import com.cryobank.postgres.domain.BloodType;
import com.cryobank.postgres.domain.BodyType;
import com.cryobank.postgres.domain.Category;
import com.cryobank.postgres.domain.DonorManPostgre;
import com.cryobank.postgres.domain.Eyes;
import com.cryobank.postgres.domain.Gender;
import com.cryobank.postgres.domain.Hair;
import com.cryobank.postgres.domain.Nation;
import com.cryobank.postgres.domain.Order;
import com.cryobank.postgres.domain.OrderStatus;

public interface DonorService {

	public int createDonor(DonorManPostgre donor);

	public DonorManPostgre updateDonor(DonorManPostgre donor);

	public void saveDonor(DonorManPostgre donor);

	public void deleteDonorById(int id);

	public DonorManPostgre getDonor(int id);

	public List<DonorManPostgre> getAllDonors();

	public List<Gender> getAllGender();

	public List<BloodType> getAllBloodTypes();

	public List<Nation> getAllNations();

	public List<Eyes> getAllEyes();

	public List<Hair> getAllHairs();

	public List<DonorManPostgre> getAllFemaleDonors();

	public List<DonorManPostgre> getAllMaleDonors();

	public List<OrderStatus> getAllOrderStatus();

	public List<BodyType> getAllBodyTypes();

	public List<Order> searchFromAllDonors(String queryDonor);
	
	
	
	public FamilyTree getMother(int id);

	public FamilyTree getMotherGrandma(int id);

	public FamilyTree getMotherGrandpa(int id);

	public FamilyTree getFather(int id);

	public FamilyTree getFatherGrandma(int id);

	public FamilyTree getFatherGrandpa(int id);

	public FamilyTree getBrother(int id);

	public FamilyTree getSister(int id);
	

}
