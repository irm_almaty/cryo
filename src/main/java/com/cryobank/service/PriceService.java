package com.cryobank.service;

import java.util.List;

import com.cryobank.postgres.domain.Price;

public interface PriceService {

	 public List<Price> getAllPrice();
	
}
