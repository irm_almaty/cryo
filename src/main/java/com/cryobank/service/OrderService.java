package com.cryobank.service;

import java.util.List;

import com.cryobank.postgres.domain.Order;
import com.cryobank.postgres.domain.OrderStatus;
import com.cryobank.postgres.domain.QueryValue;

public interface OrderService {

	public List<Order> getAllOrders();

	public List<Order> getAllOrdersByUser(int iuserId);

	public List<Order> getAllOrdersByStatusNewOrder(int statusId);

	public List<Order> getAllOrdersByUserByStatus(int iuserId, int statusId);

	public List<Order> getAllOrdersByUserByStatus(int iuserId, String status);

	public int createOrder(Order order);

	public List<Order> searchFromAllOrders(String querySh);

	public List<OrderStatus> allOrderStatusList();

	public Order getOrderById(int id);

	public Order updateOrder(Order order);

	List<Order> getAllOrdersByUserByStatusByDonor(int iuserId, String status, int donorId);

	List<Order> getAllOrdersByStatus(String status);

	QueryValue getOrderCounts();

}
