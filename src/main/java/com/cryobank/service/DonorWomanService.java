package com.cryobank.service;

import java.util.List;

import com.cryobank.medialog.domain.DonorChildMedialog;
import com.cryobank.medialog.domain.DonorManMedialog;
import com.cryobank.medialog.domain.DonorWomanMedialog;
import com.cryobank.medialog.domain.Parametr;

public interface DonorWomanService {

	DonorWomanMedialog getDonor(int id);

	List<DonorWomanMedialog> getAllOoocyteDonors();

	List<DonorWomanMedialog> getAllSurmomDonors();

	List<DonorChildMedialog> listChilds(int donorId);

	List<DonorWomanMedialog> getAllSurMomDonorsPagination(int offset, int maxResult);

	List<DonorWomanMedialog> getAllSurMomDonorsPaginationByCode(int offset, int maxResult, String code);

	List<DonorWomanMedialog> getAllOocyteDonorsPagination(int offset, int maxResult);

	List<DonorWomanMedialog> getAllOocyteDonorsPaginationByCode(int offset, int maxResult, String code);

	List<DonorWomanMedialog> getAllOocyteDonorsPaginationBySearchParametr(int offset, int maxResult, String searchParametr);

	int getAllOocyteDonorsCountBySearchParametr(String searchParametr);

	List<Parametr> getAllOocyteDonorNation();

	Integer getAllSurMomDonorsCountBySearchParametr(String searchParametr);

	List<DonorWomanMedialog> getAllSurMomDonorsPaginationBySearchParametr(int offset, int maxResult, String searchParametr);

}
