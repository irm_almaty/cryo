package com.cryobank.service;

import java.util.List;

import com.cryobank.postgres.domain.Subscript;

public interface SubscriptService {

	public Subscript getSubscriptById(int id);

	public int createSubscript(Subscript subscript);

	public List<Subscript> getAllSubscript();

	public List<Subscript> getSubscriptEmail(String userEmail);

}
