package com.cryobank.service;

import java.util.List;

import com.cryobank.postgres.domain.Category;

public interface CategoryService {

	public List<Category> getAllCategory();
}
