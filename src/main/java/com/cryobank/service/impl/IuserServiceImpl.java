package com.cryobank.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cryobank.medialog.dao.IuserDao;
import com.cryobank.medialog.domain.CountValue;
import com.cryobank.medialog.domain.Iuser;
import com.cryobank.postgres.domain.PatientStatus;
import com.cryobank.service.IuserService;

@Service
@Transactional
public class IuserServiceImpl implements IuserService {

	@Autowired
	private IuserDao iuserDao;

	@Override
	public Iuser getIuserByName(String iuserName) {

		return iuserDao.getIuserByName(iuserName);
	}

	@Override
	public Iuser getEnableIuserByName(String iuserName) {
		// TODO Auto-generated method stub
		return iuserDao.getEnableIuserByName(iuserName);
	}

	public Iuser getUserFromContext(Iuser user) {
		if (user != null) {
			throw new RuntimeException("�� �� ������������!!!");
		}
		return user;
	}

	@Override
	public Iuser getIuserById(Integer iuserId) {
		return iuserDao.getIuserById(iuserId);
	}

	@Override
	public List<Iuser> getIuserListByStatus(PatientStatus patientStatus) {
		return iuserDao.getIuserListByStatus(patientStatus);
	}

	@Override
	public CountValue getIuserCountByStatus() {
		return iuserDao.getByIuserStatusCount();
	}
	

}
