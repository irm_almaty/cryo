package com.cryobank.service.impl;

import com.cryobank.medialog.dao.DonorManMedialogDao;
import com.cryobank.medialog.dao.DonorWomanMedialogDao;
import com.cryobank.medialog.domain.Parametr;
import com.cryobank.medialog.domain.DTO.DonorSearch;
import com.cryobank.service.DonorSearchService;
import com.cryobank.util.Constants;
import com.cryobank.util.SearchParametr;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

@Service
@Transactional
public class DonorSearchServiceImpl implements DonorSearchService {

	@Autowired
	DonorWomanMedialogDao donorWomanMedialogDao;

	@Autowired
	DonorManMedialogDao donorManMedialogDao;

	private final static String SQL_AND = " and ";

	private final static String SQL_WHITE_SPACE = " ";

	private final static String SQL_LIKE_OPEN = " like '%";

	private final static String SQL_LIKE_CLOSED = "%' ";

	private final static String SQL_EQUAL = "=";

	private final static String SQL_QUOTE_OPEN = "'";

	private final static String SQL_QUOTE_CLOSED = "'";

	@Override
	public String getSearchQuery(DonorSearch donorSearch) {

		StringBuilder query = new StringBuilder();
		SearchParametr nation = new SearchParametr(donorSearch.getNation(), Constants.SQL_DONOR_NATION);
		SearchParametr hairColor = new SearchParametr(donorSearch.getHairColor(), Constants.SQL_DONOR_HAIR_COLOR);
		SearchParametr eyesColor = new SearchParametr(donorSearch.getEyesColor(), Constants.SQL_DONOR_EYES_COLOR);
		SearchParametr bloodType = new SearchParametr(donorSearch.getBloodType(), Constants.SQL_DONOR_BLOOD_TYPE);

		if (!StringUtils.isEmpty(donorSearch.getDonorCode()) || !StringUtils.isEmpty(donorSearch.getDonorId())) {
			query = new StringBuilder();
			query.append(SQL_AND);
			if (!StringUtils.isEmpty(donorSearch.getDonorCode())) {
				query.append(Constants.SQL_DONOR_CODE);
			} else if (!StringUtils.isEmpty(donorSearch.getDonorId())) {
				query.append(Constants.SQL_DONOR_ID);
			}
			query.append(SQL_EQUAL);
			query.append(SQL_QUOTE_OPEN);
			if (!StringUtils.isEmpty(donorSearch.getDonorCode())) {
				query.append(donorSearch.getDonorCode());
			} else if (!StringUtils.isEmpty(donorSearch.getDonorId())) {
				query.append(donorSearch.getDonorId());
			}
			query.append(SQL_QUOTE_CLOSED);
			return query.toString();
		}

		List<SearchParametr> allParametrs = new ArrayList<>();
		List<SearchParametr> queryParametrs = new ArrayList<>();

		allParametrs.add(nation);
		allParametrs.add(hairColor);
		allParametrs.add(eyesColor);
		allParametrs.add(bloodType);

		for (SearchParametr parametr : allParametrs) {
			if (!StringUtils.isEmpty(parametr.getCode())) {
				queryParametrs.add(parametr);
			}
		}

		if (queryParametrs == null || queryParametrs.isEmpty()) {
			return null;
		}

		for (int i = 0; i <= queryParametrs.size() - 1; i++) {
			if (Constants.SQL_DONOR_NATION.equals(queryParametrs.get(i).getDescription())) {
				query.append(SQL_AND);
				query.append(queryParametrs.get(i).getDescription());
				query.append(SQL_LIKE_OPEN);
				query.append(queryParametrs.get(i).getCode());
				query.append(SQL_LIKE_CLOSED);
			} else {
				if (i == 0) {
					query.append(SQL_AND);
					query.append(queryParametrs.get(i).getDescription());
					query.append(SQL_EQUAL);
					query.append(SQL_QUOTE_OPEN);
					query.append(queryParametrs.get(i).getCode());
					query.append(SQL_QUOTE_CLOSED);
				} else if (i != 0 && i != queryParametrs.size() - 1) {
					query.append(SQL_AND);
					query.append(queryParametrs.get(i).getDescription());
					query.append(SQL_EQUAL);
					query.append(SQL_QUOTE_OPEN);
					query.append(queryParametrs.get(i).getCode());
					query.append(SQL_QUOTE_CLOSED);
				}
				if (i != 0 && i == queryParametrs.size() - 1) {
					query.append(SQL_AND);
					query.append(queryParametrs.get(i).getDescription());
					query.append(SQL_EQUAL);
					query.append(SQL_QUOTE_OPEN);
					query.append(queryParametrs.get(i).getCode());
					query.append(SQL_QUOTE_CLOSED);
				}
			}
		}
		if (!query.toString().isEmpty()) {
			return query.toString();
		}
		return null;
	}

	@Override
	public List<String> getAllOocyteDonorNation() {
		List<Parametr> parametrs = donorWomanMedialogDao.getAllOocyteDonorNation();
		List<String> list = new ArrayList<>();
		for (Parametr item : parametrs) {
			list.add(item.getCode());
		}
		return list;
	}

	@Override
	public List<String> getAllOocyteHairColor() {
		List<Parametr> parametrs = donorWomanMedialogDao.getAllOocyteDonorHairColor();
		List<String> list = new ArrayList<>();
		for (Parametr item : parametrs) {
			list.add(item.getCode());
		}
		return list;
	}

	@Override
	public List<String> getAllOocyteBloodType() {
		List<Parametr> parametrs = donorWomanMedialogDao.getAllOocyteDonorBloodType();
		List<String> list = new ArrayList<>();
		for (Parametr item : parametrs) {
			list.add(item.getCode());
		}
		return list;
	}

	@Override
	public List<String> getAllOocyteEyesColor() {
		List<Parametr> parametrs = donorWomanMedialogDao.getAllOocyteDonorEyesColor();
		List<String> list = new ArrayList<>();
		for (Parametr item : parametrs) {
			list.add(item.getCode());
		}
		return list;
	}

	@Override
	public List<String> getAllManEyesColor() {
		List<Parametr> parametrs = donorManMedialogDao.getAllSpermDonorEyesColor();
		List<String> list = new ArrayList<>();
		for (Parametr item : parametrs) {
			list.add(item.getCode());
		}
		return list;
	}

	@Override
	public List<String> getAllManHairColor() {
		List<Parametr> parametrs = donorManMedialogDao.getAllSpermDonorHairColor();
		List<String> list = new ArrayList<>();
		for (Parametr item : parametrs) {
			list.add(item.getCode());
		}
		return list;
	}

	@Override
	public List<String> getAllManBloodType() {
		List<Parametr> parametrs = donorManMedialogDao.getAllSpermDonorBloodType();
		List<String> list = new ArrayList<>();
		for (Parametr item : parametrs) {
			list.add(item.getCode());
		}
		return list;
	}

	@Override
	public List<String> getAllManNation() {
		List<Parametr> parametrs = donorManMedialogDao.getAllSpermDonorNation();
		List<String> list = new ArrayList<>();
		for (Parametr item : parametrs) {
			list.add(item.getCode());
		}
		return list;
	}

}
