package com.cryobank.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cryobank.medialog.dao.DonorManMedialogDao;
import com.cryobank.medialog.dao.FamilyTreeDao;
import com.cryobank.medialog.domain.DonorManMedialog;
import com.cryobank.medialog.domain.FamilyTree;
import com.cryobank.postgres.dao.PhotoDao;
import com.cryobank.postgres.dao.VoiceDao;
import com.cryobank.postgres.domain.Photo;
import com.cryobank.postgres.domain.Voice;
import com.cryobank.service.DonorManService;

@Service
@Transactional
public class DonorManServiceImpl implements DonorManService {

	@Autowired
	public DonorManMedialogDao donorManMedialogDao;

	@Autowired
	private FamilyTreeDao familyTreeDao;

	@Autowired
	public PhotoDao photoDao;

	@Autowired
	public VoiceDao voiceDao;

	@Override
	public FamilyTree getMotherGrandma(int id) {
		return familyTreeDao.getMotherGrandma(id);
	}

	@Override
	public FamilyTree getMotherGrandpa(int id) {

		return familyTreeDao.getMotherGrandpa(id);
	}

	@Override
	public FamilyTree getFather(int id) {
		// TODO Auto-generated method stub
		return familyTreeDao.getFather(id);
	}

	@Override
	public FamilyTree getFatherGrandpa(int id) {

		return familyTreeDao.getFatherGrandpa(id);
	}

	@Override
	public FamilyTree getBrother(int id) {
		// TODO Auto-generated method stub
		return familyTreeDao.getBrother(id);
	}

	@Override
	public FamilyTree getSister(int id) {

		return familyTreeDao.getSister(id);
	}

	@Override
	public FamilyTree getMother(int id) {

		return familyTreeDao.getMother(id);
	}

	@Override
	public FamilyTree getFatherGrandma(int id) {

		return familyTreeDao.getFatherGrandma(id);
	}

	@Override
	public DonorManMedialog getDonor(int id) {

		return donorManMedialogDao.getDonor(id);
	}

	@Override
	public List<DonorManMedialog> getAllDonors() {

		return donorManMedialogDao.getAllDonors();
	}

	@Override
	public void savePhoto(Photo photo) {
		photoDao.savePhoto(photo);

	}

	@Override
	public int createPhoto(Photo photo) {

		return photoDao.createPhoto(photo);
	}

	@Override
	public Photo updatePhoto(Photo photo) {

		return updatePhoto(photo);
	}

	@Override
	public void deletePhotoById(int id) {
		photoDao.deletePhotoById(id);

	}

	@Override
	public Photo getPhoto(int id) {

		return getPhoto(id);
	}

	@Override
	public List<Photo> getAllPhoto() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int createVoice(Voice voice) {

		return voiceDao.createVoice(voice);
	}

	@Override
	public void deleteVoiceById(int id) {
		voiceDao.deleteVoiceById(id);

	}

	@Override
	public List<DonorManMedialog> getAllDonorsPagination(int offset, int maxResult) {

		return donorManMedialogDao.getAllDonorsPagination(offset, maxResult);
	}

	@Override
	public List<DonorManMedialog> getAllDonorsPaginationByCode(int offset, int maxResult, String code) {
		return donorManMedialogDao.getAllDonorsPaginationByDonorCode(offset, maxResult, code);
	}

	@Override
	public List<DonorManMedialog> getAllSpermDonorPaginationBySearchParametr(int offset, int maxResult,
			String searchParametr) {
		return donorManMedialogDao.getAllSpermDonorsPaginationBySearchParametr(offset, maxResult, searchParametr);
	}

	@Override
	public int getAllSpermDonorCountParametr(String searchParametr) {
		return donorManMedialogDao.getAllSpermCountBySearchParametr(searchParametr);
	}

}
