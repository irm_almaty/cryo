package com.cryobank.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cryobank.medialog.dao.FamilyTreeDao;
import com.cryobank.medialog.domain.FamilyTree;
import com.cryobank.postgres.dao.BloodTypeDao;
import com.cryobank.postgres.dao.BodyTypeDao;
import com.cryobank.postgres.dao.DonorManPostgreDao;
import com.cryobank.postgres.dao.EyesDao;
import com.cryobank.postgres.dao.GenderDao;
import com.cryobank.postgres.dao.HairDao;
import com.cryobank.postgres.dao.NationDao;
import com.cryobank.postgres.dao.OrderStatusDao;
import com.cryobank.postgres.domain.BloodType;
import com.cryobank.postgres.domain.BodyType;
import com.cryobank.postgres.domain.DonorManPostgre;
import com.cryobank.postgres.domain.Eyes;
import com.cryobank.postgres.domain.Gender;
import com.cryobank.postgres.domain.Hair;
import com.cryobank.postgres.domain.Nation;
import com.cryobank.postgres.domain.Order;
import com.cryobank.postgres.domain.OrderStatus;
import com.cryobank.service.DonorService;

@Service
@Transactional
public class DonorServiceImpl implements DonorService {

	@Autowired
	private DonorManPostgreDao donorDao;

	@Autowired
	private GenderDao genderDao;

	@Autowired
	private NationDao nationDao;

	@Autowired
	private BloodTypeDao bloodTypeDao;

	@Autowired
	private BodyTypeDao bodyTypeDao;
	
	@Autowired
	private EyesDao eyesDao;

	@Autowired
	private HairDao hairDao;
	
	@Autowired 
	private OrderStatusDao orderStatusDao;
	
	
	@Autowired
    private FamilyTreeDao familyTreeDao;
	
	
	
	
	@Override
	public FamilyTree getMotherGrandma(int id) {
		return familyTreeDao.getMotherGrandma(id);
	}


	@Override
	public FamilyTree getMotherGrandpa(int id) {
		
		return familyTreeDao.getMotherGrandpa(id);
	}


	@Override
	public FamilyTree getFather(int id) {
		// TODO Auto-generated method stub
		return familyTreeDao.getFather(id);
	}


	@Override
	public FamilyTree getFatherGrandpa(int id) {
		
		return familyTreeDao.getFatherGrandpa(id);
	}


	@Override
	public FamilyTree getBrother(int id) {
		// TODO Auto-generated method stub
		return familyTreeDao.getBrother(id);
	}


	@Override
	public FamilyTree getSister(int id) {
		
		return familyTreeDao.getSister(id);
	}
	

	@Override
	public FamilyTree getMother(int id) {
		
		return familyTreeDao.getMother(id);
	}
	

	@Override
	public FamilyTree getFatherGrandma(int id) {
		
		return familyTreeDao.getFatherGrandma(id);
	}

	
	
	

	@Override
	public int createDonor(DonorManPostgre donor) {
		
		return donorDao.createDonor(donor);
	}

	@Override
	public DonorManPostgre updateDonor(DonorManPostgre donor) {
		
		return  donorDao.updateDonor(donor);
	}

	@Override
	public void saveDonor(DonorManPostgre donor) {
		donorDao.saveDonor(donor);

	}

	@Override
	public void deleteDonorById(int id) {
		donorDao.deleteDonorById(id);
	}

	@Override
	public DonorManPostgre getDonor(int id) {

		return donorDao.getDonor(id);

	}

	@Override
	public List<DonorManPostgre> getAllDonors() {

		return donorDao.getAllDonors();
	}

	@Override
	public List<Gender> getAllGender() {
		return genderDao.getAllGender();
	}

	@Override
	public List<BloodType> getAllBloodTypes() {

		return bloodTypeDao.getAllBloodTypes();
	}

	@Override
	public List<Nation> getAllNations() {
		//
		return nationDao.getAllNations();
	}

	@Override
	public List<Eyes> getAllEyes() {

		return eyesDao.getAllEyes();
	}

	@Override
	public List<Hair> getAllHairs() {

		return hairDao.getAllHairs();
	}

	@Override
	public List<DonorManPostgre> getAllFemaleDonors() {
		
		return donorDao.getAllFemaleDonors();
	}

	@Override
	public List<DonorManPostgre> getAllMaleDonors() {
		return donorDao.getAllMaleDonors();
	}
	
	@Override
	public List<OrderStatus> getAllOrderStatus() {
		return orderStatusDao.getAllOrderStatus();
	}

	@Override
	public List<BodyType> getAllBodyTypes() {
		
		return bodyTypeDao.getAllBodyTypes();
	}

	@Override
	public List<Order> searchFromAllDonors(String queryDonor) {
		
		return donorDao.searchFromAllDonors(queryDonor);
	}

}
