package com.cryobank.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cryobank.postgres.dao.ManDao;
import com.cryobank.postgres.domain.Man;
import com.cryobank.service.ManService;


@Service
@Transactional
public class ManServiceImpl implements ManService{

	
	@Autowired
    private ManDao manDAO;
	
	
	@Override
	public long createMan(Man man) {
		return manDAO.createMan(man);
	}

	
	
	@Override
	public Man updateMan(Man man) {
		return manDAO.updateMan(man);
	}

	@Override
	public void deleteMan(long id) {
		manDAO.deleteMan(id);
		
	}

	@Override
	public List<Man> getAllMan() {
		 return manDAO.getAllMan();
	}

	@Override
	public Man getMan(long id) {
		
		return manDAO.getMan(id);
	}

	@Override
	public List<Man> getAllMans(String mansName) {
		
		return manDAO.getAllMan(mansName);
	}
	

}
