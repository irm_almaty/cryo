package com.cryobank.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cryobank.postgres.dao.NewsDao;
import com.cryobank.postgres.domain.News;
import com.cryobank.service.NewsService;

@Service
@Transactional
public class NewsServiceImpl implements NewsService {

	@Autowired
	private NewsDao newsDao;

	@Override
	public int createNews(News news) {

		return newsDao.createNews(news);
	}

	@Override
	public News updateNews(News news) {

		return newsDao.updateNews(news);
	}

	@Override
	public void deleteNews(int id) {
		 newsDao.deleteNews(id);

	}

	@Override
	public List<News> getAllNews() {

		return newsDao.getAllNews();
	}

	@Override
	public News getNews(int id) {

		return newsDao.getNews(id);

	}

	@Override
	public List<News> getLastNews() {
		
		return newsDao.getLastNews();
	}

}
