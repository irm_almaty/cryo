package com.cryobank.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cryobank.postgres.dao.ArticleDao;
import com.cryobank.postgres.dao.CategoryDao;
import com.cryobank.postgres.domain.Category;
import com.cryobank.service.CategoryService;

@Service
@Transactional
public class CategoryServiceImpl implements CategoryService {

	@Autowired
	public CategoryDao categoryDao;
	
	
	@Override
	public List<Category> getAllCategory() {
		return categoryDao.getAllCategory();
		
	}
	
	

}
