package com.cryobank.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cryobank.postgres.dao.ArticleDao;
import com.cryobank.postgres.domain.Article;
import com.cryobank.service.ArticleService;

@Service
@Transactional
public class ArticleServiceImpl implements ArticleService {
	
	@Autowired
	public ArticleDao articleDao;

	@Override
	public int createArticle(Article article) {
		return articleDao.createArticle(article);
	}

	@Override
	public Article updateArticle(Article article) {
		
		return articleDao.updateArticle(article);
				
	}

	@Override
	public void deleteArticle(int id) {
		articleDao.deleteArticle(id);
		
	}

	@Override
	public List<Article> getAllArticle() {
		
		return articleDao.getAllArticle();
	}

	@Override
	public Article getArticle(int id) {
		return articleDao.getArticle(id);
	}

	@Override
	public List<Article> getLastArticle() {
		
		return articleDao.getLastArticle();
	}

}
