package com.cryobank.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cryobank.postgres.dao.OrderDao;
import com.cryobank.postgres.dao.OrderStatusDao;
import com.cryobank.postgres.domain.Order;
import com.cryobank.postgres.domain.OrderStatus;
import com.cryobank.postgres.domain.QueryValue;
import com.cryobank.service.OrderService;

@Service
@Transactional
public class OrderServiceImpl implements OrderService {

	@Autowired
	private OrderDao orderDao;

	@Autowired
	private OrderStatusDao orderStatusDao;

	@Override
	public List<Order> getAllOrders() {

		return orderDao.getAllOrders();
	}

	@Override
	public int createOrder(Order order) {

		return orderDao.createOrder(order);
	}

	@Override
	public List<Order> getAllOrdersByUser(int iuserId) {

		return orderDao.getAllOrdersByUser(iuserId);
	}

	@Override
	public List<Order> getAllOrdersByStatusNewOrder(int statusId) {

		return orderDao.getAllOrdersByStatusNewOrder(statusId);
	}

	@Override
	public List<Order> getAllOrdersByUserByStatus(int iuserId, int statusId) {

		return orderDao.getAllOrdersByUserByStatus(iuserId, statusId);
	}

	@Override
	public List<Order> searchFromAllOrders(String querySh) {

		return orderDao.searchFromAllOrders(querySh);
	}

	@Override
	public List<OrderStatus> allOrderStatusList() {

		return orderStatusDao.getAllOrderStatus();
	}

	@Override
	public Order getOrderById(int id) {
		return orderDao.getOrderById(id);
	}

	@Override
	public Order updateOrder(Order order) {
		return orderDao.updateOrder(order);
	}

	@Override
	public List<Order> getAllOrdersByUserByStatus(int iuserId, String status) {
		return orderDao.getAllOrdersByIuserByStatus(iuserId, status);
	}

	@Override
	public List<Order> getAllOrdersByUserByStatusByDonor(int iuserId, String status, int donorId) {
		return orderDao.getAllOrdersByIuserByStatusByDonorId(iuserId, status, donorId);
	}

	@Override
	public List<Order> getAllOrdersByStatus(String status) {
		return orderDao.getAllOrdersByStatus(status);
	}

	@Override
	public QueryValue getOrderCounts() {
		return orderDao.getOrderCounts();
	}

}
