package com.cryobank.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cryobank.medialog.dao.PatientMedialogDao;
import com.cryobank.medialog.domain.PatientMedialog;
import com.cryobank.postgres.dao.PatientPostgreDao;
import com.cryobank.postgres.domain.PatientPostgre;
import com.cryobank.postgres.domain.PatientStatus;
import com.cryobank.service.PatientService;

@Service
@Transactional
public class PatientServiceImpl implements PatientService {

	@Autowired
	PatientPostgreDao patientPostgreDao;

	@Autowired
	PatientMedialogDao patientMedialogDao;

	@Override
	public int createPatientPostgre(PatientPostgre patientPostgre) {

		return patientPostgreDao.createPatientPostgre(patientPostgre);

	}

	@Override
	public PatientPostgre updatePatientPostgre(PatientPostgre patientPostgre) {

		return patientPostgreDao.updatePatientPostgre(patientPostgre);
	}

	@Override
	public PatientPostgre getPatientPostgre(int id) {

		return patientPostgreDao.getPatientPostgre(id);
	}

	@Override
	public List<PatientPostgre> getAllPatientPostgre() {

		return patientPostgreDao.getAllPatientPostgre();
	}

	@Override
	public List<PatientMedialog> getPatientMedialogByEmail(String email) {

		return patientMedialogDao.getPatientMedialogByEmail(email);
	}

	@Override
	public List<PatientPostgre> getPatientPostgreByEmail(String email) {

		return patientPostgreDao.getPatientPostgreByEmail(email);
	}

	@Override
	public List<PatientPostgre> getAllPatientPostgre(PatientStatus status) {
		return patientPostgreDao.getAllPatientPostgre(status);
	}

	@Override
	public PatientPostgre getPatientPostgreByIuserId(Integer iuserId) {
		return patientPostgreDao.getPatientPostgreByIuserId(iuserId);
	}

	@Override
	public PatientMedialog getPatientMedialogByIuserId(Integer iuserId) {
		return patientMedialogDao.getPatientMedialogByIuserId(iuserId);
	}

	@Override
	public List<PatientMedialog> getPatientMedialogByPhone(String phone) {
		// TODO Auto-generated method stub
		return patientMedialogDao.getPatientMedialogByPhone(phone);
	}

	@Override
	public List<PatientMedialog> getPatientMedialogByName(String firstname, String lastname) {

		return patientMedialogDao.getPatientMedialogByName(firstname, lastname);
	}

	@Override
	public PatientMedialog getPatientMedialogById(int patientId) {
		return patientMedialogDao.getPatientMedialog(patientId);
	}

}
