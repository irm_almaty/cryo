package com.cryobank.service.impl;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cryobank.medialog.domain.Iuser;
import com.cryobank.service.UserContextService;

@Service
@Transactional
public class UserContextServiceImpl implements UserContextService {

	@Override
	public void setContext(Iuser user) {
		Authentication auth = new PreAuthenticatedAuthenticationToken(user, "ok");
		SecurityContextHolder.getContext().setAuthentication(auth);
	}

	@Override
	public Iuser getUserContext() {
		Iuser iuser = (Iuser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return iuser;
	}

}
