package com.cryobank.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cryobank.medialog.dao.DonorChildMedialogDao;
import com.cryobank.medialog.dao.DonorWomanMedialogDao;
import com.cryobank.medialog.domain.DonorChildMedialog;
import com.cryobank.medialog.domain.DonorWomanMedialog;
import com.cryobank.medialog.domain.Parametr;
import com.cryobank.service.DonorWomanService;

@Service
@Transactional
public class DonorWomanServiceImpl implements DonorWomanService {

	@Autowired
	private DonorWomanMedialogDao donorWomanMedialogDao;

	@Autowired
	private DonorChildMedialogDao donorChildMedialogDao;

	@Override
	public DonorWomanMedialog getDonor(int id) {
		return donorWomanMedialogDao.getDonor(id);
	}

	@Override
	public List<DonorWomanMedialog> getAllOoocyteDonors() {
		return donorWomanMedialogDao.getAllOocyteDonors();
	}

	@Override
	public List<DonorWomanMedialog> getAllSurmomDonors() {
		return donorWomanMedialogDao.getAllSurMomDonors();
	}

	@Override
	public List<DonorChildMedialog> listChilds(int donorId) {
		return donorChildMedialogDao.listChilds(donorId);
	}

	@Override
	public List<DonorWomanMedialog> getAllSurMomDonorsPagination(int offset, int maxResult) {
		return donorWomanMedialogDao.getAllSurMomDonorsPagination(offset, maxResult);
	}

	@Override
	public List<DonorWomanMedialog> getAllSurMomDonorsPaginationByCode(int offset, int maxResult, String code) {
		return donorWomanMedialogDao.getAllSurMomDonorsPaginationByDonorCode(offset, maxResult, code);
	}

	@Override
	public List<DonorWomanMedialog> getAllOocyteDonorsPagination(int offset, int maxResult) {
		return donorWomanMedialogDao.getAllOocyteDonorsPagination(offset, maxResult);
	}

	@Override
	public List<DonorWomanMedialog> getAllOocyteDonorsPaginationByCode(int offset, int maxResult, String code) {
		return donorWomanMedialogDao.getAllOocyteDonorsPaginationByDonorCode(offset, maxResult, code);
	}

	@Override
	public List<DonorWomanMedialog> getAllOocyteDonorsPaginationBySearchParametr(int offset, int maxResult,
			String searchParametr) {
		return donorWomanMedialogDao.getAllOocyteDonorsPaginationBySearchParametr(offset, maxResult, searchParametr);
	}

	@Override
	public int getAllOocyteDonorsCountBySearchParametr(String searchParametr) {
		return donorWomanMedialogDao.getAllOocyteCountBySearchParametr(searchParametr);
	}

	@Override
	public List<DonorWomanMedialog> getAllSurMomDonorsPaginationBySearchParametr(int offset, int maxResult,
			String searchParametr) {
		return donorWomanMedialogDao.getAllSurMomDonorsPaginationBySearchParametr(offset, maxResult, searchParametr);
	}

	@Override
	public Integer getAllSurMomDonorsCountBySearchParametr(String searchParametr) {
		return donorWomanMedialogDao.getAllSurMomCountBySearchParametr(searchParametr);
	}

	@Override
	public List<Parametr> getAllOocyteDonorNation() {
		return donorWomanMedialogDao.getAllOocyteDonorNation();
	}

}
