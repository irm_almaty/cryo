package com.cryobank.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cryobank.postgres.dao.SubscriptDao;
import com.cryobank.postgres.domain.Subscript;
import com.cryobank.service.SubscriptService;

@Service
@Transactional
public class SubscriptServiceImpl implements SubscriptService {

	@Autowired

    private SubscriptDao subscriptDao;
	
	@Override
	public Subscript getSubscriptById(int id) {
		
		return subscriptDao.getSubscriptById(id);
	}

	@Override
	public int createSubscript(Subscript subscript) {
		return subscriptDao.createSubscript(subscript);
	}

	@Override
	public List<Subscript> getAllSubscript() {
		
		return subscriptDao.getAllSubscript();
	}

	@Override
	public List<Subscript> getSubscriptEmail(String userEmail) {
		
		return subscriptDao.getSubscriptEmail(userEmail);
	}

}
