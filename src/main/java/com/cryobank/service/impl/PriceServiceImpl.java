package com.cryobank.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cryobank.postgres.dao.ArticleDao;
import com.cryobank.postgres.dao.PriceDao;
import com.cryobank.postgres.domain.Price;
import com.cryobank.service.PriceService;

@Service
@Transactional
public class PriceServiceImpl implements PriceService{

	@Autowired
	public PriceDao priceDao;
	
	@Override
	public List<Price> getAllPrice() {
		
		return priceDao.getAllPrice();
		
	}

}
