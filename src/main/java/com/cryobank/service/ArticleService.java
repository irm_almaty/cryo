package com.cryobank.service;

import java.util.List;

import com.cryobank.postgres.domain.Article;

public interface ArticleService {

	public int createArticle(Article article);
    public Article updateArticle(Article article);
    public void deleteArticle(int id);
    public List<Article> getAllArticle();
    public Article getArticle(int id);
    public List<Article> getLastArticle();
	
	
}
