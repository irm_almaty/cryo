package com.cryobank.service;

import java.util.List;

import com.cryobank.medialog.domain.PatientMedialog;
import com.cryobank.postgres.domain.PatientPostgre;
import com.cryobank.postgres.domain.PatientStatus;

public interface PatientService {

	public int createPatientPostgre(PatientPostgre patientPostgre);

	public PatientPostgre updatePatientPostgre(PatientPostgre patientPostgre);

	public PatientPostgre getPatientPostgre(int id);

	public List<PatientPostgre> getAllPatientPostgre();

	public List<PatientPostgre> getAllPatientPostgre(PatientStatus status);

	public List<PatientMedialog> getPatientMedialogByEmail(String email);

	public List<PatientPostgre> getPatientPostgreByEmail(String email);

	public List<PatientMedialog> getPatientMedialogByPhone(String phone);

	public List<PatientMedialog> getPatientMedialogByName(String firstname, String lastname);


	PatientMedialog getPatientMedialogById(int patientId);

	PatientPostgre getPatientPostgreByIuserId(Integer iuserId);

	PatientMedialog getPatientMedialogByIuserId(Integer iuserId);

}
