package com.cryobank.dto;

import java.util.Date;

import com.cryobank.medialog.domain.Iuser;
import com.cryobank.postgres.domain.PatientPostgre;

public class PatientDTO {

	private Integer id;

	private String fio;

	private String email;

	private String access;

	private String city;

	private String phone;

	private Date date;

	private Integer emr;

	public PatientDTO() {
	}

	public PatientDTO(Iuser iuser) {
		super();
		this.fio = iuser.getUserName();
		this.email = iuser.getUserLogin();
		this.access = iuser.getUserAccess();
		this.city = iuser.getUserCity();
		this.date = iuser.getCreateDate();
		this.emr = iuser.getUserId();
	}

	public PatientDTO(PatientPostgre patientPostgre) {
		super();
		this.id = patientPostgre.getId();
		this.fio = patientPostgre.getFio();
		this.email = patientPostgre.getEmail();
		this.access = patientPostgre.getAccess();
		this.city = patientPostgre.getCity();
		this.date = patientPostgre.getCreateDate();
		this.phone = patientPostgre.getPhone();
	}

	public String getFio() {
		return fio;
	}

	public void setFio(String fio) {
		this.fio = fio;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAccess() {
		return access;
	}

	public void setAccess(String access) {
		this.access = access;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Integer getEmr() {
		return emr;
	}

	public void setEmr(Integer emr) {
		this.emr = emr;
	}

}
