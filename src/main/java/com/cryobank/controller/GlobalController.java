package com.cryobank.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.support.SessionStatus;

import com.cryobank.medialog.domain.Iuser;
import com.cryobank.service.IuserService;
import com.cryobank.util.Constants;
import com.cryobank.util.ParametrList;

@ControllerAdvice
public class GlobalController {

	@Autowired
	IuserService iuserService;

	@ModelAttribute("iuser")
	public void addAttributes(HttpServletRequest request, ModelMap model) {
		addAtributeUser(model);
		removeAttribute(model, request);
	}

	public void addAtributeUser(ModelMap model) {
		String userName = SecurityContextHolder.getContext().getAuthentication().getName();
		Iuser user = new Iuser();
		if (!Constants.USER_ANONYMOUS.equals(userName) && !model.containsAttribute("iuser")) {
			user = iuserService.getEnableIuserByName(userName);
			Iuser iuser = new Iuser(user);
			model.addAttribute("iuser", iuser);
		} else if (!model.containsAttribute("iuser")) {
			model.addAttribute("iuser", null);
		}
	}

	public void removeAttribute(ModelMap model, HttpServletRequest request) {
	}

}
