package com.cryobank.controller;

import java.util.Map;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import com.cryobank.annotations.Link;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.cryobank.medialog.domain.PatientMedialog;
import com.cryobank.postgres.domain.PatientPostgre;
import com.cryobank.service.PatientService;
import com.cryobank.util.Constants;
import com.cryobank.util.email.Email;
import com.cryobank.util.email.EmailService;

@Controller
@RequestMapping(value = "/registration")
public class RegistrationController extends AbstractCryobankController {

	@Autowired
	PatientService patientService;

	@Autowired
	EmailService emailService;

	@Link(index = false)
	@RequestMapping("/signup")
	public ModelAndView articleArticle(@ModelAttribute("patient") PatientPostgre patientPostgre) {
		ModelAndView model = new ModelAndView("registration");
		Map<String, String> list = new LinkedHashMap<String, String>();
		list.put("������ ������", "������ ������");
		list.put("������ �������", "������ ������");
		list.put("������ ������� � ������", "������ ������ � ������");
		list.put("����������� ������", "����������� ������");
		model.addObject("list", list);
		return model;
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String registrateUser(@ModelAttribute("patient") PatientPostgre patientPostgre, Map<String, Object> map,
			BindingResult result) throws BindException {
		String message = "���� ����������� ��� ����������";
		if (patientPostgre.getFirstname().isEmpty()) {
			result.rejectValue("firstname", "error", message);
		}
		if (patientPostgre.getLastname().isEmpty()) {
			result.rejectValue("lastname", "error", message);
		}
		if (patientPostgre.getCountry().isEmpty()) {
			result.rejectValue("country", "error", message);
		}
		if (patientPostgre.getCity().isEmpty()) {
			result.rejectValue("city", "error", message);
		}
		if (patientPostgre.getPhone().isEmpty()) {
			result.rejectValue("phone", "error", message);
		}
		if (patientPostgre.getEmail().isEmpty()) {
			result.rejectValue("email", "error", message);
		}
		if (patientPostgre.getAccess().isEmpty()) {
			result.rejectValue("access", "error", message);
		}
		List<PatientMedialog> listPat = patientService.getPatientMedialogByEmail(patientPostgre.getEmail());

		/*
		 * if (!listPat.isEmpty() && !patientPostgre.getEmail().isEmpty()) {
		 * result.rejectValue("email", "error",
		 * "��� email ���� � ����������� ����"); }
		 */

		List<PatientPostgre> listPatients = patientService.getPatientPostgreByEmail(patientPostgre.getEmail());
		if (!listPatients.isEmpty() && !patientPostgre.getEmail().isEmpty()) {
			result.rejectValue("email", "error", "email �����");
		}
		if (!result.hasErrors()) {
			patientPostgre.setRegistryFrom(Constants.USER_WEB);
			patientService.createPatientPostgre(patientPostgre);
			String text = "<h3>" + patientPostgre.getFirstname() + " " + patientPostgre.getLastname()
					+ ", ����� ���������� �� ������ cryobank.kz!</br></h3>" + "���� ������, �� ������ � ������� "
					+ patientPostgre.getAccess().toUpperCase() + ", ��������� �� ������������ � ������c�������.</br>"
					+ "�������� ������, �� ��� ����������� �������� �����.";

			Email email = new Email(patientPostgre.getEmail(), "CRYOBANK.KZ �����������", text);
			emailService.sendSingleEmail(email);

			String textAdmin = "������������ � �������: </br>" + "�.�.�.: " + patientPostgre.getLastname() + " "
					+ patientPostgre.getFirstname() + " " + patientPostgre.getPatroname() + "</br>" + "�����: "
					+ patientPostgre.getCity() + "</br>" + "����� ��������: " + patientPostgre.getPhone() + "</br>"
					+ "������ � �������: " + patientPostgre.getAccess().toUpperCase() + "</br>"
					+ "������� �������������� �����������.";

			Email adminEmail = new Email("cryobank@irm.kz", "������ �� �����������", textAdmin);
			emailService.sendSingleEmail(adminEmail);
			return "redirect:../registration/succes";
		}
		map.put("result", result);
		return "registration";
	}

	@RequestMapping("/succes")
	public ModelAndView succesRegistration() {
		ModelAndView model = new ModelAndView("registrationSucces");
		return model;
	}

}
