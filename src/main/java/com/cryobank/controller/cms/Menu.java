package com.cryobank.controller.cms;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Shah on 21.11.2017.
 */
public class Menu {

    private Map<String, Boolean> menuMap = new HashMap<>();

    {
        this.menuMap.put("1", false);
        this.menuMap.put("2", false);
        this.menuMap.put("3", false);
        this.menuMap.put("4", false);
    }

    public Menu(Integer item) {
        this.menuMap.put(item.toString(), true);
    }

    public Map<String, Boolean> getMenuMap() {
        return menuMap;
    }
}
