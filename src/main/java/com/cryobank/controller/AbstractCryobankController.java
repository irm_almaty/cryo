package com.cryobank.controller;

import com.cryobank.controller.cms.CMSConstants;
import com.cryobank.controller.cms.Menu;
import com.cryobank.util.breadcrumb.Breadcrumb;
import com.cryobank.util.meta.MetaConstants;
import com.cryobank.util.meta.MetaData;
import com.cryobank.util.meta.MetaDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by Suleiman on 22.10.2017.
 */
public class AbstractCryobankController {

    @Autowired
    private MetaDataService metaDataService;

    @ModelAttribute(MetaConstants.metaAttribute)
    public void populateMetaData(HttpSession session, ModelMap model) {
        if (session.getAttribute(MetaConstants.metaAttribute) == null) {
            model.addAttribute(MetaConstants.metaAttribute, metaDataService.getDefaultMetaDatas());
        } else {
            List<MetaData> metaDatas = (List<MetaData>) session.getAttribute(MetaConstants.metaAttribute);
            model.addAttribute(metaDatas);
        }
    }

    @ModelAttribute(MetaConstants.metaTitle)
    public void populateTitle(HttpSession session, ModelMap model) {
        if (session.getAttribute(MetaConstants.metaTitle) == null) {
            model.addAttribute(MetaConstants.metaTitle, metaDataService.getDefaultTitle());
        } else {
            String title = (String) session.getAttribute(MetaConstants.metaTitle);
            model.addAttribute(MetaConstants.metaTitle, title);
        }
    }

    public void populateBreadcrumb(ModelMap model, Breadcrumb breadcrumb) {
        model.addAttribute(Breadcrumb.ATTRIBUTE, breadcrumb);
    }

    public void populateCanonical(ModelMap model, String link) {
        model.addAttribute(MetaConstants.canonical, link);
    }

    public void populateMenu(ModelMap model, Menu menu) {
        model.addAttribute(CMSConstants.Menu.menu, menu);
    }

}
