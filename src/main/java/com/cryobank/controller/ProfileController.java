package com.cryobank.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.cryobank.medialog.domain.Iuser;

@Controller
@RequestMapping("/profile")
public class ProfileController {

	@RequestMapping("/user/view")
	public ModelAndView viewUserProfile(@ModelAttribute("iuser") Iuser iuser) {
		ModelAndView model = new ModelAndView("userProfile");
		model.addObject("iuser", iuser);
		return model;
	}
	
}
