package com.cryobank.controller;

import java.util.List;

import com.cryobank.annotations.Link;
import com.cryobank.controller.cms.Menu;
import com.cryobank.util.breadcrumb.Breadcrumb;
import com.cryobank.util.meta.MetaConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


import com.cryobank.medialog.dao.CryoDao;
import com.cryobank.medialog.domain.DonorCount;
import com.cryobank.postgres.domain.Subscript;
import com.cryobank.service.DonorManService;
import com.cryobank.service.DonorWomanService;
import com.cryobank.util.HibernateMedialogUtil;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

@Controller
public class MainPageController extends AbstractCryobankController {

    @Autowired
    CryoDao cryoDao;

    @Autowired
    private DonorManService donorManService;

    @Autowired
    private DonorWomanService donorWomanService;

    @Autowired
    HibernateMedialogUtil hibernateMedialogUtil;

    @RequestMapping(value = {"/"}, method = RequestMethod.GET)
    public String getMainPage(ModelMap model) {
        model.addAttribute("msg", "�� ����� �� �������!!");
        Subscript subscript = new Subscript();
        model.addAttribute("subscript", subscript);
        DonorCount donorCount;
        try {
            donorCount = cryoDao.getDonorCount();
            model.addAttribute("counts", donorCount);
        } catch (Exception ex) {
            donorCount = new DonorCount(23L, 103L, 23L);
            model.addAttribute("counts", donorCount);
        }
        populateMenu(model, new Menu(1));
        return "mainPage";
    }

    @RequestMapping(value = {"/home", "index.php", "index"}, method = RequestMethod.GET)
    public RedirectView getHomePage(RedirectAttributes attributes) {
        attributes.addFlashAttribute("mainPage");
        return new RedirectView("/");
    }

    @RequestMapping(value = "/robots.txt", produces = {"text/plain"}, method = RequestMethod.GET)
    @ResponseBody
    public String getRobotsTxt() {
        return "User-agent: *" +
                "\n" + "Allow: /resources/css/*.css" +
                "\n" + "Allow: /resources/js/*.js" +
                "\n" + "Allow: /donor/*" +
                "\n" + "Disallow: /admin/*" +
                "\n" + "Disallow: /user/*" +
                "\n" + "Disallow: /laboratory/*" +
                "\n" + "Sitemap: http://cryobank.kz/sitemap.xml"+
                "\n" + "Host: cryobank.kz";
    }

    @Link(title = MetaConstants.Contact.title,
            description = MetaConstants.Contact.description,
            keywords = MetaConstants.Default.keywords)
    @RequestMapping(value = {"/contact"}, method = RequestMethod.GET)
    public String getContactPage(ModelMap model) {
        Subscript subscript = new Subscript();
        model.addAttribute("subscript", subscript);
        populateBreadcrumb(model, new Breadcrumb("��������"));
        populateMenu(model, new Menu(4));
        return "contactPage";
    }

    @RequestMapping(value = {"/index", "/main"}, method = RequestMethod.GET)
    public String getIndexPage(ModelMap model) {

        int donorOocyteCount = 71;

        int donorManCount = 18;

		/*
         * if (hibernateMedialogUtil.checkSession()) { if
		 * (donorWomanService.getAllDonors() != null &&
		 * !donorWomanService.getAllDonors().isEmpty()) { donorOocyteCount =
		 * donorWomanService.getAllDonors().size(); }
		 * 
		 * if (donorManService.getAllDonors() != null &&
		 * !donorManService.getAllDonors().isEmpty()) { donorManCount =
		 * donorManService.getAllDonors().size(); } }
		 */

        model.addAttribute("msg", "�� ����� �� �������!!");

        model.addAttribute("donorManCount", donorManCount);

        model.addAttribute("donorOocyteCount", donorOocyteCount);

        return "mainPage";
    }


}
