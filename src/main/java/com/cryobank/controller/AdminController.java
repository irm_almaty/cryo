package com.cryobank.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import com.cryobank.dto.PatientDTO;
import com.cryobank.medialog.domain.CountValue;
import com.cryobank.medialog.domain.Iuser;
import com.cryobank.medialog.domain.PatientMedialog;
import com.cryobank.postgres.domain.PatientPostgre;
import com.cryobank.postgres.domain.PatientStatus;
import com.cryobank.service.IuserService;
import com.cryobank.service.PatientService;
import com.cryobank.util.Constants;
import com.cryobank.util.RegistrationValidator;
import com.cryobank.util.email.Email;
import com.cryobank.util.email.EmailService;
import com.cryobank.util.sessionDTO.RegDTO;

@Controller
@RequestMapping("/admin/user")
@SessionAttributes(types = RegDTO.class)
public class AdminController {

	@Autowired
	PatientService patientService;

	@Autowired
	IuserService iuserService;

	@Autowired
	EmailService emailService;

	@RequestMapping("/list")
	public ModelAndView patientsList(
			@RequestParam(name = "status", required = true, defaultValue = "EXPECT") String status) {
		ModelAndView model;
		List<PatientPostgre> listExpect = patientService.getAllPatientPostgre(PatientStatus.EXPECT);
		CountValue counts = iuserService.getIuserCountByStatus();
		counts.setExpectValue(new Long(listExpect.size()));
		if (PatientStatus.EXPECT.equals(PatientStatus.valueOf(status))) {
			model = new ModelAndView("usersPostgreList");
			model.addObject("list", listExpect);
			model.addObject("counts", counts);
			return model;
		} else if (PatientStatus.ACTIVE.equals(PatientStatus.valueOf(status))
				|| PatientStatus.ARCHIVED.equals(PatientStatus.valueOf(status))) {
			model = new ModelAndView("iusersList");
			List<Iuser> list = iuserService.getIuserListByStatus(PatientStatus.valueOf(status));
			model.addObject("list", list);
			model.addObject("counts", counts);
			return model;
		}
		return new ModelAndView();
	}

	@RequestMapping("/view")
	public ModelAndView patientView(Model modelSession, @ModelAttribute("regval") RegistrationValidator regval,
			RegDTO regDTO, @RequestParam(value = "id", required = false) int id,
			@RequestParam(value = "code", required = false) String code) {
		PatientPostgre patientPostgre = new PatientPostgre();
		if (Constants.USER_MEDIALOG.equals(code)) {
			patientPostgre = patientService.getPatientPostgreByIuserId(id);
		} else {
			patientPostgre = patientService.getPatientPostgre(id);
		}
		if (patientPostgre == null && Constants.USER_MEDIALOG.equals(code)) {
			PatientMedialog patientMedialog = patientService.getPatientMedialogByIuserId(id);
			patientPostgre = new PatientPostgre(patientMedialog);
			List<PatientPostgre> paPostgre = patientService.getPatientPostgreByEmail(patientMedialog.getEmail());
			if (!paPostgre.isEmpty()) {
				patientPostgre = paPostgre.get(0);
			}
			// �������� ����� �� ����� cryo_patient
			Iuser iuser = iuserService.getIuserById(id);
			if (iuser != null) {
				patientPostgre.setAccess(iuser.getUserAccess());
			}
			patientService.createPatientPostgre(patientPostgre);
		}
		ModelAndView model = new ModelAndView("patientView");
		String message = regDTO.getError();
		model.addObject("patient", patientPostgre);
		List<PatientMedialog> listMedialogEmail = patientService.getPatientMedialogByEmail(patientPostgre.getEmail());
		List<PatientMedialog> listMedialogPhone = patientService.getPatientMedialogByPhone(patientPostgre.getPhone());
		List<PatientMedialog> listMedialogName = patientService.getPatientMedialogByName(patientPostgre.getFirstname(),
				patientPostgre.getLastname());
		// Iuser iuserMedialog =
		// iuserService.getIuserByName(patientPostgre.getEmail());
		Iuser iuserMedialog = iuserService.getEnableIuserByName(patientPostgre.getEmail());
		regDTO.setIuser(iuserMedialog);
		model.addObject("email", listMedialogEmail);
		model.addObject("phones", listMedialogPhone);
		model.addObject("names", listMedialogName);
		model.addObject("iuser", iuserMedialog);
		model.addObject("message", message);
		regDTO.setPatientPostgre(patientPostgre);
		regDTO.setIuser(iuserMedialog);
		modelSession.addAttribute(regDTO);
		return model;
	}

	@RequestMapping(value = "/confim", method = RequestMethod.POST)
	public String patientConfimPassword(RegDTO regDTO, @ModelAttribute("regval") RegistrationValidator regval,
			BindingResult result) {
		if (regval.getFirstValue() == null) {
			regval.setFirstValue("");
		}
		if (regval.getFirstValue().equals(regval.getSecondValue())
				&& regval.getFirstValue().equals(regval.getThirdValue())) {
			Email email = new Email("cryobank@irm.kz", "CRYOBANK.KZ �������� ��������������� ������",
					"������������ " + regDTO.getPatientPostgre().getFio() + "<br> " + "������� ������ � � ������� "
							+ regDTO.getPatientPostgre().getAccess() + "<br>" + "���������� � ������������: <br>"
							+ "�������� �����: " + regDTO.getPatientPostgre().getEmail());

			PatientPostgre patientPostgre = regDTO.getPatientPostgre();
			patientPostgre.setPatientStatus(PatientStatus.ACTIVE);
			patientPostgre.setIuserId(regDTO.getIuser().getUserId());
			patientService.updatePatientPostgre(patientPostgre);
			emailService.sendSingleEmail(email);

			Iuser iuser = iuserService.getEnableIuserByName(patientPostgre.getEmail());
			Email emailToUser = new Email(patientPostgre.getEmail(), "CRYOBANK.KZ ��������������� ������",
					regDTO.getPatientPostgre().getFio() + ", �� �������� ������ � ������� "
							+ regDTO.getPatientPostgre().getAccess() + "<br>" + "��� �����: " + iuser.getUserLogin()
							+ "<br>" + "��� ������: " + iuser.getPassword() + "<br>" + "������ ����������� ��: "
							+ iuser.getCreateDate());
			emailService.sendSingleEmail(emailToUser);

			return "redirect:../user/succes";
		} else {
			regDTO.setError("��� �������� �� ���������!!! ��������� ������������.");
			return "redirect:../user/view?id=" + regDTO.getPatientPostgre().getId();
		}
	}

	@RequestMapping("/succes")
	public ModelAndView confimSucces(RegDTO regDTO, SessionStatus status) {
		ModelAndView model = new ModelAndView("confimSucces");
		String message = regDTO.getError();
		Iuser iuser = regDTO.getIuser();
		PatientPostgre patientPostgre = regDTO.getPatientPostgre();
		model.addObject("iuser", iuser);
		model.addObject("patient", patientPostgre);
		model.addObject("message", message);
		status.setComplete();
		return model;
	}

	@RequestMapping("/changeStatus")
	public String changeUserStatus(@RequestParam(name = "status", required = true) String status,
			@RequestParam(name = "id", required = true) Integer id) {
		PatientPostgre patient = patientService.getPatientPostgre(id);
		patient.setPatientStatus(PatientStatus.valueOf(status));
		patientService.updatePatientPostgre(patient);
		return "redirect:../user/list";
	}

}
