package com.cryobank.controller;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.cryobank.annotations.Link;
import com.cryobank.controller.cms.Menu;
import com.cryobank.util.breadcrumb.Breadcrumb;
import com.cryobank.util.meta.MetaConstants;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.StandardPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.cryobank.medialog.domain.DonorManMedialog;
import com.cryobank.medialog.domain.DonorWomanMedialog;
import com.cryobank.medialog.domain.FamilyTree;
import com.cryobank.medialog.domain.Iuser;
import com.cryobank.medialog.domain.DTO.DonorSearch;
import com.cryobank.postgres.domain.BloodType;
import com.cryobank.postgres.domain.BodyType;
import com.cryobank.postgres.domain.DonorManPostgre;
import com.cryobank.postgres.domain.DonorType;
import com.cryobank.postgres.domain.Eyes;
import com.cryobank.postgres.domain.Gender;
import com.cryobank.postgres.domain.Hair;
import com.cryobank.postgres.domain.Nation;
import com.cryobank.postgres.domain.Order;
import com.cryobank.postgres.domain.OrderStatus;
import com.cryobank.postgres.domain.Photo;
import com.cryobank.postgres.domain.Voice;
import com.cryobank.service.DonorManService;
import com.cryobank.service.DonorSearchService;
import com.cryobank.service.DonorService;
import com.cryobank.service.DonorWomanService;
import com.cryobank.service.OrderService;
import com.cryobank.util.Pagination;
import com.cryobank.util.PaginationItem;
import com.cryobank.util.PaginationService;
import com.cryobank.util.ParametrList;
import com.cryobank.util.email.Email;
import com.cryobank.util.email.EmailService;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@SessionAttributes({"search_man", "parameters_man"})
public class DonorManController extends AbstractCryobankController {

    @Autowired
    private DonorManService donorManService;

    DonorManMedialog man = new DonorManMedialog();

    @Autowired
    private DonorWomanService donorWomanService;

    @Autowired
    private DonorService donorService;

    @Autowired
    private PaginationService paginationService;

    @Autowired
    private DonorSearchService donorSearchService;

    @Autowired
    OrderService orderService;

    @Autowired
    EmailService emailService;

    @ModelAttribute("search_man")
    public DonorSearch populatePerson() {
        return new DonorSearch();
    }

    @ModelAttribute("parameters_man")
    public ParametrList setParametrs() {
        ParametrList parameters = new ParametrList();
        parameters.setNations(donorSearchService.getAllManNation());
        parameters.setHairs(donorSearchService.getAllManHairColor());
        parameters.setBlood(donorSearchService.getAllManBloodType());
        parameters.setEyes(donorSearchService.getAllManEyesColor());
        return parameters;
    }

    @Link(title = MetaConstants.DonorSperm.title,
            description = MetaConstants.DonorSperm.description,
            keywords = MetaConstants.DonorSperm.keywords)
    @RequestMapping(value = {"allMaleDonors", "/donor/sperm/list",})
    public String listAllMaleDonors(ModelMap map,
                                    @RequestParam(value = "maxResult", required = false, defaultValue = "12") int maxResult,
                                    @RequestParam(value = "page", required = false, defaultValue = "1") int page,
                                    @RequestParam(value = "code", required = false) String code,
                                    @ModelAttribute("search_man") DonorSearch search,
                                    @ModelAttribute("parameters_man") ParametrList parameters) {

        map.put("nations", parameters.getNations());
        map.put("hairs", parameters.getHairs());
        map.put("blood", parameters.getBlood());
        map.put("eyes", parameters.getEyes());

        String searchParametr = donorSearchService.getSearchQuery(search);

        int count = donorManService.getAllSpermDonorCountParametr(searchParametr);
        Pagination pagination = paginationService.getPagination(count, maxResult, page);
        List<PaginationItem> pagItems = pagination.getPagItems();
        PaginationItem pagItem = pagination.getPagItem();
        int offset = pagination.getOffsetCount();

        String msg = "������ ������";
        map.put("count", count);
        map.put("msg", msg);
        map.put("pags", pagItems);
        map.put("pag", pagItem);
        map.put("donorList",
                donorManService.getAllSpermDonorPaginationBySearchParametr(offset, maxResult, searchParametr));

        // ������� ����������� � ������ ��� ������ ��������� � ���������
        if (code != null && !code.isEmpty()) {
            String searchInfo = "<strong>�� �������: </strong> " + code + " <br><strong>�������: </strong> "
                    + donorManService.getAllDonorsPaginationByCode(offset, maxResult, code).size() + " �������";
            if (donorManService.getAllDonorsPaginationByCode(offset, maxResult, code).size() == 0) {
                searchInfo = "<strong>�� �������: </strong> " + code + " <br><strong>�� ������� �������</strong> ";
            }
            pagination.setShowPagination(false);
            map.put("searchInfo", searchInfo);
        }
        populateBreadcrumb(map, new Breadcrumb(msg));
        populateCanonical(map, MetaConstants.Canonical.spermList);
        map.put("showPag", pagination.isShowPagination());
        populateCmsPage(map);
        return "listDonor";

    }

    @RequestMapping(value = "/donor/sperm/search", method = RequestMethod.POST)
    public String searchDonor(@ModelAttribute("search_man") DonorSearch search, RedirectAttributes attributes) {
        attributes.addFlashAttribute("search_man");
        return "redirect:../sperm/list";
    }

    @RequestMapping(value = "/donor/sperm/search/refresh", method = RequestMethod.GET)
    public String searchDonorReset(ModelMap model, HttpSession session,
                                   @ModelAttribute("search_man") DonorSearch search, RedirectAttributes attributes) {
        if (model.containsAttribute("search_man")) {
            session.removeAttribute("search_man");
            model.remove("search_man");
        }
        attributes.addFlashAttribute("search_man");
        return "redirect:../list";
    }

    @RequestMapping("/donor/sperm/order")
    public String orderDonor(@RequestParam int id, @ModelAttribute("iuser") Iuser iuser) {
        DonorManMedialog donorManMedialog = donorManService.getDonor(id);
        Order order = new Order(donorManMedialog, iuser, OrderStatus.WAITS);
        List<Order> orders = orderService.getAllOrdersByUserByStatus(iuser.getUserId(), OrderStatus.WAITS.toString());
        if (orders.isEmpty() || orders == null) {
            orderService.createOrder(order);
        }
        List<Order> orderList = orderService.getAllOrdersByUser(iuser.getUserId());
        if (orderList.isEmpty() || orderList == null) {
            orderList = new ArrayList<>();
        }
        StringBuilder sb = new StringBuilder();
        sb.append("<h3>������ �� ������ ������ ������</h3>");
        sb.append("<br>");
        sb.append("<strong>������������:</strong> ");
        sb.append(iuser.getUserName() + " :: " + iuser.getUserId());
        sb.append("<br>");
        sb.append("�������������� ������ ������ :: " + donorManMedialog.getDonorManId() + " "
                + donorManMedialog.getDonorName());
        sb.append("<br>");
        sb.append(
                "<strong>��� ����������� ������������ ������ ��������� � ������� ������ �� ����� cryobank.kz</strong> ");
        Email adminEmail = new Email("cryobank@irm.kz", "������ ������ ������", sb.toString());
        emailService.sendSingleEmail(adminEmail);
        return "redirect:/orders/list";
    }

    @Link(title = MetaConstants.DonorSperm.title,
            description = MetaConstants.DonorSperm.description,
            keywords = MetaConstants.DonorSperm.keywords)
    @RequestMapping("/donor/sperm/view")
    public ModelAndView viewDonor(@RequestParam int id) {

        ModelAndView model = new ModelAndView("donorView");
        DonorManMedialog donorMedialog = donorManService.getDonor(id);
        DonorManPostgre donorPosgre = donorService.getDonor(id);

        FamilyTree familyTreeMother = donorManService.getMother(id);
        FamilyTree motherGrandma = donorManService.getMotherGrandma(id);
        FamilyTree motherGrandpa = donorManService.getMotherGrandpa(id);

        FamilyTree father = donorManService.getFather(id);
        FamilyTree fatherGrandma = donorManService.getFatherGrandma(id);
        FamilyTree fatherGrandpa = donorManService.getFatherGrandpa(id);

        FamilyTree sister = donorManService.getSister(id);
        FamilyTree brother = donorManService.getBrother(id);

        if (donorMedialog == null) {
            ModelAndView modelError = new ModelAndView("Error");
            String message = "������ ��� � ����";
            modelError.addObject("message", message);
            return modelError;
        }
        if (donorPosgre == null) {
            donorPosgre = new DonorManPostgre();
            donorPosgre.setDonorId(id);
            donorService.createDonor(donorPosgre);

        }
        model.addObject("donor2", donorPosgre);
        List<Photo> listPhoto = donorPosgre.getDonorPhoto();
        List<Voice> listVoice = donorPosgre.getDonorVoice();
        model.addObject("listVoice", listVoice);
        model.addObject("list3", listPhoto);
        model.addObject("donor", donorMedialog);

        model.addObject("familyTreeMother", familyTreeMother);
        model.addObject("motherGrandma", motherGrandma);
        model.addObject("motherGrandpa", motherGrandpa);

        model.addObject("father", father);
        model.addObject("fatherGrandpa", fatherGrandpa);
        model.addObject("fatherGrandma", fatherGrandma);

        model.addObject("sister", sister);
        model.addObject("brother", brother);

        populateCmsPage(model.getModelMap());
        return model;
    }

    DonorManMedialog donorMedialog = new DonorManMedialog();

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping("/admin/editDonor")
    public ModelAndView editDonor(@RequestParam int id, HttpServletResponse request,
                                  @ModelAttribute DonorManPostgre donorPostgre) {

        if (donorService.getDonor(id) == null) {
            donorMedialog = donorManService.getDonor(id);
            int donorId = donorMedialog.getDonorManId();
            donorPostgre.setDonorId(donorId);
            donorService.createDonor(donorPostgre);
        }
        donorPostgre = donorService.getDonor(id);
        donorMedialog = donorManService.getDonor(id);
        ModelAndView model = new ModelAndView("donorForm");
        model.addObject("donor", donorPostgre);
        model.addObject("donorMedialog", donorMedialog);
        List<Photo> listPhoto = donorPostgre.getDonorPhoto();
        List<Voice> listVoice = donorPostgre.getDonorVoice();
        model.addObject("listPhoto", listPhoto);
        model.addObject("listVoice", listVoice);
        return model;
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping("admin/deletePhoto")
    public ModelAndView deletePhoto(@RequestParam int id, int donorId) {

        ModelAndView model = new ModelAndView("redirect:../admin/editDonor?id=" + donorId);
        donorManService.deletePhotoById(id);
        model.addObject("photo", photo);
        return model;
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping("admin/deleteVoice")
    public ModelAndView deleteVoice(@RequestParam int id, int donorId) {
        ModelAndView model = new ModelAndView("redirect:../admin/editDonor?id=" + donorId);
        donorManService.deleteVoiceById(id);
        return model;

    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping("/admin/saveDonor")
    public ModelAndView saveDonor(@ModelAttribute DonorManPostgre donor, HttpServletRequest request,
                                  @RequestParam CommonsMultipartFile[] fileUpload, @RequestParam CommonsMultipartFile[] fileUploadVoice)
            throws Exception {

        if (donor.getDonorId() == 0) {
            donorService.createDonor(donor);
        } else {
            donorService.updateDonor(donor);
        }
        /* in DEV */
        ModelAndView model = new ModelAndView("mainPage");
        System.out.println("description: " + request.getParameter("description"));

        if (fileUpload != null && fileUpload.length > 0) {
            for (CommonsMultipartFile aFile : fileUpload) {
                String name = aFile.getOriginalFilename() + request.getParameter("description");
                Photo photoDonorMan = new Photo();
                LocalDateTime photoUploadDate = new org.joda.time.LocalDateTime();
                Date date = new Date();
                StandardPasswordEncoder encoder = new StandardPasswordEncoder(aFile.getOriginalFilename());
                String result = encoder.encode(date.toString());
                String namePhoto = result + aFile.getOriginalFilename();
                String photoPath = "/resource/donor/image/" + namePhoto;
                String uploadFolder = "/opt/tomcat/webapps/resource/donor/image/";
                // String uploadFolder =
                // "C:/STS.springMVC.project/com.cryobank/src/main/webapp/resources/image/";
                if (aFile.getOriginalFilename() != "") {
                    photoDonorMan.setDonor(donor);
                    photoDonorMan.setPhotoPath(photoPath);
                    photoDonorMan.setPhotoCreateDate(photoUploadDate);
                    donorManService.createPhoto(photoDonorMan);
                }
                System.out.println("Saving file: " + name);
                if (!aFile.getOriginalFilename().equals("")) {
                    aFile.transferTo(new File(uploadFolder + namePhoto));
                }
            }
        }

        if (fileUploadVoice != null && fileUploadVoice.length > 0) {
            for (CommonsMultipartFile aFile : fileUploadVoice) {
                String name = aFile.getOriginalFilename() + request.getParameter("description");
                Voice voiceDonorMan = new Voice();
                LocalDateTime voiceUploadDate = new org.joda.time.LocalDateTime();
                Date date = new Date();
                StandardPasswordEncoder encoder = new StandardPasswordEncoder(aFile.getOriginalFilename());
                String result = encoder.encode(date.toString());
                String nameVoice = result + aFile.getOriginalFilename();

                // String
                // voicePath="C:/STS.springMVC.project/com.cryobank/src/main/webapp/resources/image";

                String voicePath = "/resource/donor/voice/" + nameVoice;
                String uploadFolder = "/opt/tomcat/webapps/resource/donor/voice/";
                if (aFile.getOriginalFilename() != "") {
                    voiceDonorMan.setDonor(donor);
                    voiceDonorMan.setVoicePath(voicePath);
                    voiceDonorMan.setVoiceCreateDate(voiceUploadDate);
                    donorManService.createVoice(voiceDonorMan);
                }

                System.out.println("Saving file: " + name);
                if (!aFile.getOriginalFilename().equals("")) {
                    aFile.transferTo(new File(uploadFolder + nameVoice));
                }
            }
        }
        int donorId = donor.getDonorId();
        return new ModelAndView("redirect:../admin/editDonor?id=" + donorId);

    }

    private void populateCmsPage(ModelMap model){
        populateMenu(model,new Menu(3));
    }

    Photo photo = new Photo();
}
