package com.cryobank.controller;

import java.util.List;
import java.util.Map;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.cryobank.postgres.domain.Article;
import com.cryobank.postgres.domain.BloodType;
import com.cryobank.postgres.domain.BodyType;
import com.cryobank.postgres.domain.Category;
import com.cryobank.postgres.domain.DonorManPostgre;
import com.cryobank.postgres.domain.Eyes;
import com.cryobank.postgres.domain.Gender;
import com.cryobank.postgres.domain.Hair;
import com.cryobank.medialog.domain.Iuser;
import com.cryobank.postgres.domain.Nation;
import com.cryobank.service.DonorService;
import com.cryobank.service.IuserService;


//@Controller
public class DonorController {

	DonorManPostgre donor = new DonorManPostgre();

	public void donor() {
/*
		donor.getGender().getGenderType();

		donor.getBloodType().getBloodType();

		donor.getHair().getHairColor();

		donor.getGender().getGenderType();
		
		donor.getDonorHeight();
		
		donor.getDonorWeight();
*/
	}
	
	
	/*
	 * <h3>���: ${donor.donorName}</h3>

			<p>���: ${donor.getGender().getGenderType()}</p>

			<p>���� ��������: ${donor.birthDate}</p>

			<p>�����: ${donor.getNation().getNationName()}</p>

			<p>������ �����: ${donor.getBloodType().getBloodType()}</p>

			<p>���� �����: ${donor.getHair().getHairColor()}</p>
	 * 
	 * 
	 * */

	@Autowired
	public DonorService donorService;



	@Autowired
	private IuserService iuserService;

	@RequestMapping("/donor/allDonors")
	public String listAllDonors(Map<String, Object> map) {

		String msg = "��� ������";

		List<DonorManPostgre> donorListCount = donorService.getAllDonors();

		int donorCount = donorListCount.size();

		String count = "����� �������: " + donorCount;

		map.put("count", count);

		map.put("msg", msg);

		map.put("donor", new DonorManPostgre());

		map.put("donorList", donorService.getAllDonors());

		return "listDonor";

	}

	@RequestMapping(value = { "/donor/allFemaleDonors", "allFemaleDonors" })
	public String listAllFemaleDonors(Map<String, Object> map) {

		String msg = "������ �������";

		List<DonorManPostgre> donorListCount = donorService.getAllFemaleDonors();

		int donorFemaleCount = donorListCount.size();

		String count = "����� ������� �������: " + donorFemaleCount;

		map.put("count", count);

		map.put("msg", msg);

		map.put("donor", new DonorManPostgre());

		map.put("donorList", donorService.getAllFemaleDonors());

		return "listDonor";

	}

	//@RequestMapping(value = { "allMaleDonors", "/donor/allMaleDonors", })
	public String listAllMaleDonors(Map<String, Object> map) {

		String msg = "������ ������";

		List<DonorManPostgre> donorListCount = donorService.getAllMaleDonors();

		int donorMaleCount = donorListCount.size();

		String count = "����� ������� ������: " + donorMaleCount;

		map.put("count", count);

		map.put("msg", msg);

		map.put("donor", new DonorManPostgre());

		map.put("donorList", donorService.getAllMaleDonors());

		return "listDonor";

	}

	@RequestMapping("/donor/createDonor")
	public ModelAndView articleArticle(@ModelAttribute DonorManPostgre donor) {

		ModelAndView model = new ModelAndView("donorForm");

		String msg = "�������� ������";

		List<Gender> genderList = donorService.getAllGender();

		List<BloodType> bloodTypeList = donorService.getAllBloodTypes();

		List<Nation> nationList = donorService.getAllNations();

		List<Eyes> eyesList = donorService.getAllEyes();

		List<Hair> hairList = donorService.getAllHairs();
		
		List<BodyType> bodyTypeList=donorService.getAllBodyTypes();

		model.addObject("hairList", hairList);

		model.addObject("eyesList", eyesList);

		model.addObject("nationList", nationList);

		model.addObject("genderList", genderList);

		model.addObject("bloodTypeList", bloodTypeList);
		
		model.addObject("bodyTypeList",bodyTypeList);

		model.addObject("msg", msg);

		return model;

	}

	@RequestMapping("/donor/editDonor")
	public ModelAndView editDonor(@RequestParam int id, @ModelAttribute DonorManPostgre donor) {

		donor = donorService.getDonor(id);

		ModelAndView model = new ModelAndView("donorForm");

		List<Gender> genderList = donorService.getAllGender();

		List<BloodType> bloodTypeList = donorService.getAllBloodTypes();

		List<Nation> nationList = donorService.getAllNations();

		List<Eyes> eyesList = donorService.getAllEyes();

		List<Hair> hairList = donorService.getAllHairs();
		
		List<BodyType> bodyTypeList=donorService.getAllBodyTypes();

		model.addObject("donor", donor);

		model.addObject("hairList", hairList);

		model.addObject("eyesList", eyesList);

		model.addObject("nationList", nationList);

		model.addObject("genderList", genderList);

		model.addObject("bloodTypeList", bloodTypeList);
		
		model.addObject("bodyTypeList",bodyTypeList);

		String msg = "�������� ������ ������";

		return model;
	}

	@RequestMapping("/donor/saveDonor")
	public ModelAndView saveDonor(@ModelAttribute DonorManPostgre donor) {

		if (donor.getDonorId() == 0) {

			donorService.createDonor(donor);
		} else {
			donorService.updateDonor(donor);
		}

		if (1 == 1) {

			return new ModelAndView("redirect:/donor/allMaleDonors");

		}
		return new ModelAndView("redirect:/donor/allFemaleDonors");
	}

	
	//@RequestMapping("/donor/viewDonor")
	public ModelAndView viewArticle(@RequestParam int id) {

		donor = donorService.getDonor(id);

		ModelAndView model = new ModelAndView("donorView");

		model.addObject("donor", donor);
		

		//model.addObject("order", order);

		return model;

	}

	/*
	@RequestMapping("/donor/orderDonor")
	public ModelAndView orderDonor(@ModelAttribute  Iuser userOrder, Authentication auth) {

		auth = SecurityContextHolder.getContext().getAuthentication();

		userOrder = iuserService.getIuserByName(auth.getName());

		order.setIuser(userOrder);

		LocalDateTime orderRegDate = new org.joda.time.LocalDateTime();

		order.setOrderRegDate(orderRegDate);

		OrderStatus orderStatus = new OrderStatus();

		orderStatus.setStatusId(1);

		order.setOrderStatus(orderStatus);

		orderService.createOrder(order);

		ModelAndView model = new ModelAndView("redirect:/donor/allDonors");

		return model;

	}

*/
	
	@RequestMapping("/donor/deleteDonor")
	public ModelAndView deleteArticle(@RequestParam int id) {

		ModelAndView model = new ModelAndView("redirect:/donor/allDonors");

		List<DonorManPostgre> listArticle = donorService.getAllDonors();

		donorService.deleteDonorById(id);

		model.addObject("donor", donor);

		return model;

	}
	
	
	
	

}
