package com.cryobank.controller;



import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.List;

import org.joda.time.LocalDateTime;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.cryobank.postgres.domain.Article;
import com.cryobank.postgres.domain.Subscript;
import com.cryobank.service.ArticleService;
import com.cryobank.service.SubscriptService;

@Controller
public class SubscriptController {

	
	@Autowired
	private SubscriptService subscriptService;
	
	@RequestMapping(value = "/saveSubscript", method = { RequestMethod.POST })
	public ModelAndView saveArticle(@ModelAttribute Subscript subscript) {

		System.out.println("*****************///////////////////////////*************++++++++++++++////////"+"Start");
		
		LocalDateTime subRegDate=new org.joda.time.LocalDateTime();
		
		subscript.setSubRegDate(subRegDate);
		
		String userEmail=subscript.getEmail();
		
		System.out.println("*****************///////////////////////////*************++++++++++++++////////"+userEmail);
		
		try{
			
        List<Subscript> subs= subscriptService.getSubscriptEmail(userEmail);
		
        System.out.println(subs.size());
        
        System.out.println("*****************///////////////////////////*************++++++++++++++////////"+"RAzmer");
		}
		catch(Exception e){
			
			System.out.println("*****************///////////////////////////*************++++++++++++++////////"+"Hapnulll");
			subscriptService.createSubscript(subscript);
			System.out.println("*****************///////////////////////////*************++++++++++++++////////"+"Save");
		}
		return new ModelAndView("redirect:/main");
	}
}