package com.cryobank.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.cryobank.medialog.domain.Iuser;
import com.cryobank.postgres.domain.Order;
import com.cryobank.postgres.domain.OrderStatus;
import com.cryobank.service.OrderService;

@Controller
@RequestMapping("/order/test")
public class OrderControllerSec {

	@Autowired
	OrderService orderService;

	@RequestMapping("/list")
	public ModelAndView orderDonor(@ModelAttribute("iuser") Iuser iuser,
			@RequestParam(name = "status", defaultValue = "WAITS") String status) {

		ModelAndView model = new ModelAndView("userOrderList");
		List<Order> list = orderService.getAllOrdersByUserByStatus(iuser.getUserId(), status);
		model.addObject("list", list);
		return model;
	}

	
	
	
	@RequestMapping("{orderId}/action")
	public String cancelOrder(@ModelAttribute("iuser") Iuser iuser, @PathVariable int orderId,
			@RequestParam(name = "status", required = true) String status) {

		ModelAndView model = new ModelAndView("userOrderList");
		Order order = orderService.getOrderById(orderId);
		order.setOrderStatus(OrderStatus.valueOf(status));
		orderService.updateOrder(order);
		return "redirect:/order/list";
	}

}