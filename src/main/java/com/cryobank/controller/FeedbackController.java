package com.cryobank.controller;

import java.util.Map;

import com.cryobank.annotations.Link;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.cryobank.postgres.domain.Feedback;
import com.cryobank.postgres.domain.Subject;
import com.cryobank.util.email.Email;
import com.cryobank.util.email.EmailService;


@Controller
@RequestMapping("/feedback")
public class FeedbackController extends AbstractCryobankController {

	@Autowired
	EmailService emailService;

	@Link(index = false)
	@RequestMapping("/form")
	public ModelAndView getFeedbackForm(@ModelAttribute("feedback") Feedback feedback) {
		ModelAndView model = new ModelAndView("feedbackForm");
		return model;
	}

	@RequestMapping(value = "/send", method = RequestMethod.POST)
	public String registrateUser(@ModelAttribute("feedback") Feedback feedback, Map<String, Object> map,
			BindingResult result) throws BindException {
		String message = "���� ����������� ��� ����������";
		feedback.setSubject(Subject.SIMPLE_FEEDBACK);
		if (StringUtils.isEmpty(feedback.getAuthorName())) {
			result.rejectValue("authorName", "error", message);
		}
		if (StringUtils.isEmpty(feedback.getAuthorEmail())) {
			result.rejectValue("authorEmail", "error", message);
		}
		if (StringUtils.isEmpty(feedback.getMessageBody())) {
			result.rejectValue("messageBody", "error", message);
		}
		feedback.setSubject(Subject.SIMPLE_FEEDBACK);
		if (!result.hasErrors()) {
			String successMessage = "���� ��������� ������� ����������!";
			StringBuilder sb = new StringBuilder();
			sb.append("email ����������� : ");
			sb.append(feedback.getAuthorEmail() + "<br>");
			sb.append(feedback.getMessageBody());
			Email email = new Email("cryobank@irm.kz", "cryobank.kz ::: �������� �����", sb.toString());
			emailService.sendSingleEmail(email);
			map.put("successMessage", successMessage);
		}
		map.put("result", result);
		return "feedbackForm";
	}

}
