package com.cryobank.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.cryobank.medialog.domain.Iuser;
import com.cryobank.service.IuserService;

@Controller
public class MyController {

	@Autowired
	private IuserService iuserService;

	@RequestMapping("/main/{userName}")
	public ModelAndView getMainPage2(@PathVariable("userName") String name) {
		ModelAndView model = new ModelAndView("mainPage");
		model.addObject("msg", "�� ����� �� ������ ������� ��������!! ����� ���������� " + name);
		return model;
	}

	@RequestMapping("/user/userPage")
	public String getUserPage(ModelMap model) {
		model.addAttribute("msg", "����� ���������� �� ������ cryobank.kz , �� ����� ��� ������������");
		return "userPage";
	}

	@RequestMapping("/admin/adminPage")
	public String getAdminPage(ModelMap model,Iuser userLog,Authentication auth) {

		
		auth = SecurityContextHolder.getContext().getAuthentication();
		
		String name= auth.getName();
		
		userLog = iuserService.getIuserByName(auth.getName());

		String name2=userLog.getUserName();

		model.addAttribute("msg", "����� ���������� �� ������ cryobank.kz , �� ����� ��� �������������  "+"���:"+name2+ "   "+name);
				
		return "adminPage";
		
	}
}



