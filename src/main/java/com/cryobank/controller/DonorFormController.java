package com.cryobank.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;
import javax.xml.ws.spi.http.HttpHandler;

import com.cryobank.annotations.Link;
import com.cryobank.controller.cms.Menu;
import com.cryobank.util.breadcrumb.Breadcrumb;
import com.cryobank.util.meta.MetaConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.cryobank.postgres.domain.DonorType;
import com.cryobank.postgres.domain.Feedback;
import com.cryobank.postgres.domain.Subject;
import com.cryobank.util.email.Email;
import com.cryobank.util.email.EmailService;

@Controller
@RequestMapping("/form")
public class DonorFormController extends AbstractCryobankController {

	@Autowired
	EmailService emailService;

	@Link(title = MetaConstants.DonorSperm.Form.title,
			description = MetaConstants.DonorSperm.Form.description,
			keywords = MetaConstants.Default.keywords)
	@RequestMapping("/sperm")
	public ModelAndView spermDonorForm(@ModelAttribute("feedback") Feedback feedback) {
		ModelAndView model = new ModelAndView("spermForm");
		populateBreadcrumb(model.getModelMap(),new Breadcrumb("��� ����� ������� ������"));
		populateCmsPage(model.getModelMap());
		return model;
	}

	@Link(title = MetaConstants.DonorOocyte.Form.title,
			description = MetaConstants.DonorOocyte.Form.description,
			keywords = MetaConstants.Default.keywords)
	@RequestMapping("/oocyt")
	public ModelAndView oocytDonorForm(@ModelAttribute("feedback") Feedback feedback) {
		ModelAndView model = new ModelAndView("oocytForm");
		String msg = "��� ������";
		populateBreadcrumb(model.getModelMap(),new Breadcrumb("��� ����� ������� �������"));
		populateCmsPage(model.getModelMap());
		return model;
	}

	@Link(title = MetaConstants.Surmom.Form.title,
			description = MetaConstants.Surmom.Form.description,
			keywords = MetaConstants.Surmom.keywords)
	@RequestMapping("/surmom")
	public ModelAndView surmomDonorForm(@ModelAttribute("feedback") Feedback feedback) {
		ModelAndView model = new ModelAndView("surmomForm");
		populateBreadcrumb(model.getModelMap(),new Breadcrumb("��� ����� ����������� �������"));
		populateCmsPage(model.getModelMap());
		return model;
	}

	@RequestMapping(value = "/submit", method = RequestMethod.POST)
	public String registrateUser(@ModelAttribute("feedback") Feedback feedback, Map<String, Object> map,
			BindingResult result, @RequestParam(name = "type", required = true) String donorType) throws BindException {
		String message = "���� ����������� ��� ����������";
		feedback.setSubject(Subject.SIMPLE_FEEDBACK);
		if (StringUtils.isEmpty(feedback.getAuthorName())) {
			result.rejectValue("authorName", "error", message);
		}
		if (StringUtils.isEmpty(feedback.getAuthorEmail())) {
			result.rejectValue("authorEmail", "error", message);
		}
		if (StringUtils.isEmpty(feedback.getMessageBody())) {
			result.rejectValue("messageBody", "error", message);
		}
		feedback.setSubject(Subject.SIMPLE_FEEDBACK);
		if (!result.hasErrors()) {
			String successMessage = "���� ��������� ������� ����������!";
			StringBuilder sb = new StringBuilder();
			sb.append("email ����������� : ");
			sb.append(feedback.getAuthorEmail() + "<br>");
			sb.append(feedback.getMessageBody());
			Email email = new Email("cryobank@irm.kz", "cryobank.kz ::: �������� �����", sb.toString());
			emailService.sendSingleEmail(email);
			map.put("successMessage", successMessage);
		}
		map.put("result", result);
		if (DonorType.SPERM.equals(DonorType.valueOf(donorType))) {
			
			
			
			
			
			return "spermForm";
		}
		if (DonorType.OOCYTE.equals(DonorType.valueOf(donorType))) {
			return "oocytForm";
		}
		if (DonorType.SURMOM.equals(DonorType.valueOf(donorType))) {
			return "surmomForm";
		}
		return "mainPage";
	}

	private void populateCmsPage(ModelMap model){
		populateMenu(model,new Menu(3));
	}

}
