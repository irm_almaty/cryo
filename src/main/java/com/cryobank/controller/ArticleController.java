package com.cryobank.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.cryobank.medialog.domain.DonorManMedialog;
import com.cryobank.postgres.domain.Article;
import com.cryobank.postgres.domain.Category;
import com.cryobank.postgres.domain.News;
import com.cryobank.service.ArticleService;
import com.cryobank.service.CategoryService;
import com.cryobank.service.DonorManService;
import com.cryobank.service.NewsService;

@Controller
public class ArticleController {

	Article article = new Article();

	Category category = new Category();

	public void viewArt() {
	}

	@Autowired
	private ArticleService articleService;

	@Autowired
	private NewsService newsService;

	@Autowired
	private CategoryService categoryService;

	@RequestMapping("/article")
	public ModelAndView articleList() {

		ModelAndView model = new ModelAndView("articleList");

		List<Article> listArticle = articleService.getAllArticle();

		String msg = "��� ������";

		model.addObject("msg", msg);

		model.addObject("listArticle", listArticle);

		return model;

	}

	@RequestMapping("/article/createArticle")
	public ModelAndView articleArticle(@ModelAttribute Article article) {

		ModelAndView model = new ModelAndView("articleForm");

		String msg = "�������� ������";

		List<Category> listCategory = categoryService.getAllCategory();

		model.addObject("listCategory", listCategory);

		model.addObject("msg", msg);

		return model;

	}

	@RequestMapping("/article/editArticle")
	public ModelAndView editArticle(@RequestParam int id, @ModelAttribute Article article) {

		article = articleService.getArticle(id);

		ModelAndView model = new ModelAndView("articleForm");

		model.addObject("article", article);
		List<Category> listCategory = categoryService.getAllCategory();
		model.addObject("listCategory", listCategory);

		return model;
	}

	@RequestMapping("/article/saveArticle")
	public ModelAndView saveArticle(@ModelAttribute Article article) {

		// articleService.createArticle(article);

		if (article.getId() == 0) {
			// if employee id is 0 then creating the employee other updating the
			// employee
			articleService.createArticle(article);
		} else {
			articleService.updateArticle(article);
		}
		return new ModelAndView("redirect:/article");
	}

	@RequestMapping("/article/viewArticle")
	public ModelAndView viewArticle(@RequestParam int id, @ModelAttribute Article article) {
		
		article = articleService.getArticle(id);

		if (article == null) {
			ModelAndView modelError = new ModelAndView("Error");
			String message = "������ �� ����������";
			modelError.addObject("message", message);
			return modelError;

		}

		List<Article> listArticle = articleService.getLastArticle();

		List<News> listNews = newsService.getLastNews();

		ModelAndView model = new ModelAndView("articleView");

		model.addObject("listArticle", listArticle);

		model.addObject("listNews", listNews);

		model.addObject("article", article);

		return model;
	}

	@RequestMapping("/article/view")
	public ModelAndView view(@ModelAttribute Article article) {

		ModelAndView model = new ModelAndView("articleView");

		model.addObject("article", article);

		return model;
	}

	@RequestMapping("/article/deleteArticle")
	public ModelAndView deleteArticle(@RequestParam int id) {

		ModelAndView model = new ModelAndView("redirect:/article");

		List<Article> listArticle = articleService.getAllArticle();

		articleService.deleteArticle(id);

		model.addObject("article", article);

		return model;

	}

}
