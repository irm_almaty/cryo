package com.cryobank.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;
import javax.validation.groups.Default;

import com.cryobank.annotations.Link;
import com.cryobank.controller.cms.Menu;
import com.cryobank.util.breadcrumb.Breadcrumb;
import com.cryobank.util.meta.MetaConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionAttributeStore;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.context.request.SessionScope;
import org.springframework.web.method.annotation.SessionAttributesHandler;
import org.springframework.web.servlet.ModelAndView;

import com.cryobank.medialog.domain.DonorChildMedialog;
import com.cryobank.medialog.domain.DonorManMedialog;
import com.cryobank.medialog.domain.DonorWomanMedialog;
import com.cryobank.medialog.domain.FamilyTree;
import com.cryobank.medialog.domain.Iuser;
import com.cryobank.medialog.domain.Parametr;
import com.cryobank.medialog.domain.DTO.DonorSearch;
import com.cryobank.postgres.domain.DonorManPostgre;
import com.cryobank.postgres.domain.DonorType;
import com.cryobank.postgres.domain.Order;
import com.cryobank.postgres.domain.OrderStatus;
import com.cryobank.postgres.domain.PatientPostgre;
import com.cryobank.postgres.domain.Photo;
import com.cryobank.postgres.domain.Voice;
import com.cryobank.service.DonorSearchService;
import com.cryobank.service.DonorService;
import com.cryobank.service.DonorWomanService;
import com.cryobank.service.OrderService;
import com.cryobank.service.UserContextService;
import com.cryobank.util.Constants;
import com.cryobank.util.Pagination;
import com.cryobank.util.PaginationItem;
import com.cryobank.util.PaginationService;
import com.cryobank.util.ParametrList;
import com.cryobank.util.email.Email;
import com.cryobank.util.email.EmailService;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/donor/oocyte")
@SessionAttributes({ "search_oocyte", "parameters_woman" })
public class DonorOocitController extends AbstractCryobankController {

	@Autowired
	private DonorWomanService donorWomanService;

	@Autowired
	private PaginationService paginationService;

	@Autowired
	private DonorService donorService;

	@Autowired
	private DonorSearchService donorSearchService;

	@Autowired
	private OrderService orderService;

	@Autowired
	private UserContextService userContextService;

	@Autowired
	private EmailService emailService;

	@ModelAttribute("search_oocyte")
	public DonorSearch populatePerson() {
		return new DonorSearch();
	}

	@ModelAttribute("parameters_woman")
	public ParametrList setParametrs(ModelMap model) {
		ParametrList parameters = new ParametrList();
		if (!model.containsAttribute("parameters_woman")) {
			parameters.setNations(donorSearchService.getAllOocyteDonorNation());
			parameters.setHairs(donorSearchService.getAllOocyteHairColor());
			parameters.setBlood(donorSearchService.getAllOocyteBloodType());
			parameters.setEyes(donorSearchService.getAllOocyteEyesColor());
		}
		model.addAttribute("parameters_woman", parameters);
		return parameters;
	}

	@Link(title = MetaConstants.DonorOocyte.title,
			description = MetaConstants.DonorOocyte.description,
			keywords = MetaConstants.DonorOocyte.keywords)
	@RequestMapping("/list")
	public String listAllMaleDonors(ModelMap map,
			@RequestParam(value = "maxResult", required = false, defaultValue = "12") int maxResult,
			@RequestParam(value = "page", required = false, defaultValue = "1") int page,
			@RequestParam(value = "code", required = false) String code,
			@ModelAttribute("search_oocyte") DonorSearch search,
			@ModelAttribute("parameters_woman") ParametrList parameters, @ModelAttribute("iuser") Iuser iuser) {

		String searchParametr = donorSearchService.getSearchQuery(search);
		map.put("nations", parameters.getNations());
		map.put("hairs", parameters.getHairs());
		map.put("blood", parameters.getBlood());
		map.put("eyes", parameters.getEyes());
		int donorFemaleCount;
		if (!StringUtils.isEmpty(search.getDonorCode())) {
		}
		donorFemaleCount = donorWomanService.getAllOocyteDonorsCountBySearchParametr(searchParametr);
		Pagination pagination = paginationService.getPagination(donorFemaleCount, maxResult, page);
		List<PaginationItem> pagItems = pagination.getPagItems();
		PaginationItem paginationItem = pagination.getPagItem();
		int offset = pagination.getOffsetCount();
		map.put("pagi", paginationItem);
		map.put("pagItems", pagItems);
		map.put("donorList",
				donorWomanService.getAllOocyteDonorsPaginationBySearchParametr(offset, maxResult, searchParametr));
		String msg = "������ �������";
		map.put("msg", msg);
		map.put("count", donorFemaleCount);
		if (code != null && !code.isEmpty()) {
			String searchInfo = "<strong>�� �������: </strong> " + code + " <br><strong>�������: </strong> "
					+ donorWomanService.getAllOocyteDonorsPaginationByCode(offset, maxResult, code).size() + " �������";
			if (donorWomanService.getAllOocyteDonorsPaginationByCode(offset, maxResult, code).size() == 0) {
				searchInfo = "<strong>�� �������: </strong> " + code + " <br><strong>�� ������� �������</strong> ";
			}
			pagination.setShowPagination(false);
			map.put("searchInfo", searchInfo);
		}
		map.put("showPag", pagination.isShowPagination());
		populateBreadcrumb(map,new Breadcrumb(msg));
		populateCanonical(map,MetaConstants.Canonical.oocyteList);
		populateCmsPage(map);
		return "listOocyteDonor";
	}

	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public String searchDonor(@ModelAttribute("search_oocyte") DonorSearch search,RedirectAttributes attributes) {
		attributes.addFlashAttribute("search_oocyte");
		return "redirect:../oocyte/list";
	}

	@RequestMapping(value = "/refresh", method = RequestMethod.GET)
	public String refreshSearchParameters(ModelMap model, HttpSession session,
			@ModelAttribute("search_oocyte") DonorSearch search, RedirectAttributes attributes) {

		if (model.containsAttribute("search_oocyte")) {
			session.removeAttribute("search_oocyte");
			model.remove("search_oocyte");
		}
		attributes.addFlashAttribute("search_oocyte");
		return "redirect:../oocyte/list";
	}

	@RequestMapping("/view")
	public ModelAndView viewDonor(@RequestParam int id) {
		ModelAndView model = new ModelAndView("donorOocyteView");
		DonorWomanMedialog donorWomanMedialog = donorWomanService.getDonor(id);
		DonorManPostgre donorPosgre = donorService.getDonor(id);
		List<DonorChildMedialog> listChilds = donorWomanService.listChilds(id);
		FamilyTree mother = donorService.getMother(id);
		FamilyTree father = donorService.getFather(id);
		if (donorWomanMedialog == null) {
			ModelAndView modelError = new ModelAndView("Error");
			String message = "������ ��� � ����";
			modelError.addObject("message", message);
			return modelError;
		}
		if (donorPosgre == null) {
			donorPosgre = new DonorManPostgre();
			donorPosgre.setDonorId(id);
			donorService.createDonor(donorPosgre);
		}
		model.addObject("donor", donorPosgre);
		List<Photo> listPhoto = donorPosgre.getDonorPhoto();
		List<Voice> listVoice = donorPosgre.getDonorVoice();
		model.addObject("listVoice", listVoice);
		model.addObject("listPhoto", listPhoto);
		model.addObject("donorWoman", donorWomanMedialog);
		model.addObject("listChilds", listChilds);
		model.addObject("mother", mother);
		model.addObject("father", father);
		populateCmsPage(model.getModelMap());
		return model;
	}

	@RequestMapping("/order")
	public String orderDonor(@RequestParam int id, @ModelAttribute("iuser") Iuser iuser) {

		DonorWomanMedialog donorWomanMedialog = donorWomanService.getDonor(id);
		Order order = new Order(donorWomanMedialog, iuser, OrderStatus.WAITS);
		List<Order> orders = orderService.getAllOrdersByUserByStatus(iuser.getUserId(), OrderStatus.WAITS.toString());
		if (orders.isEmpty() || orders == null) {
			orderService.createOrder(order);
		}
		List<Order> orderList = orderService.getAllOrdersByUser(iuser.getUserId());
		if (orderList.isEmpty() || orderList == null) {
			orderList = new ArrayList<>();
		}
		StringBuilder sb = new StringBuilder();
		sb.append("<h3>������ �� ������ ������ �������</h3>");
		sb.append("<br>");
		sb.append("<strong>������������:</strong> ");
		sb.append(iuser.getUserName() + " :: " + iuser.getUserId());
		sb.append("<br>");
		sb.append("�������������� ������ ������� :: " + donorWomanMedialog.getDonorWomanId() + " "
				+ donorWomanMedialog.getDonorName());
		sb.append("<br>");
		sb.append(
				"<strong>��� ����������� ������������ ������ ��������� � ������� ������ �� ����� cryobank.kz</strong> ");
		Email adminEmail = new Email("cryobank@irm.kz", "������ ������ �������", sb.toString());
		emailService.sendSingleEmail(adminEmail);
		return "redirect:/orders/list";
	}

	private void populateCmsPage(ModelMap model){
		populateMenu(model,new Menu(3));
	}

}
