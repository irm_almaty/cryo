package com.cryobank.controller;

import java.util.List;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.cryobank.postgres.domain.Article;
import com.cryobank.postgres.domain.Category;
import com.cryobank.postgres.domain.News;
import com.cryobank.service.NewsService;

@Controller
public class NewsController {

	@Autowired
	private NewsService newsService;

	News news = new News();

	@RequestMapping("/news")
	public ModelAndView newsList() {

		ModelAndView model = new ModelAndView("newsList");

		List<News> listNews = newsService.getAllNews();

		String msg = "��� �������";

		model.addObject("msg", msg);

		model.addObject("listNews", listNews);

		return model;

	}

	@RequestMapping("/news/viewNews")
	public ModelAndView viewArticle(@RequestParam int id, @ModelAttribute News news) {

		news = newsService.getNews(id);

		if (news == null) {
			ModelAndView modelError = new ModelAndView("Error");
			String message = "������� �� ����������";
			modelError.addObject("message", message);
			return modelError;

		}
		
	
		List<News> listNews = newsService.getLastNews();

		ModelAndView model = new ModelAndView("newsView");

		model.addObject("news", news);

		model.addObject("listNews", listNews);

		return model;
	}

	@RequestMapping("/news/editNews")
	public ModelAndView editNews(@RequestParam int id, @ModelAttribute News news) {

		news = newsService.getNews(id);

		LocalDate createEditDate = news.getCreateDate();

		news.setCreateDate(createEditDate);

		ModelAndView model = new ModelAndView("newsForm");

		model.addObject("news", news);

		return model;
	}

	@RequestMapping("/news/createNews")
	public ModelAndView articleArticle(@ModelAttribute News news) {

		ModelAndView model = new ModelAndView("newsForm");

		String msg = "�������� �������";

		model.addObject("msg", msg);

		return model;

	}

	@RequestMapping("/news/saveNews")
	public ModelAndView saveArticle(@ModelAttribute News news) {

		// articleService.createArticle(article);

		if (news.getNewsId() == 0) {

			LocalDate newsCreateDate = new org.joda.time.LocalDate();

			news.setCreateDate(newsCreateDate);

			newsService.createNews(news);

		} else {

			int id = news.getNewsId();

			News newsEdit = newsService.getNews(id);

			LocalDate createEditDate = newsEdit.getCreateDate();

			news.setCreateDate(createEditDate);

			newsService.updateNews(news);
		}
		return new ModelAndView("redirect:/news");
	}

	@RequestMapping("/news/deleteNews")
	public ModelAndView deleteNews(@RequestParam int id) {

		ModelAndView model = new ModelAndView("redirect:/news");

		System.out.println(
				"������� ������� �������************************/////////////****************////////////*******///");

		newsService.deleteNews(id);

		System.out.println("������ �������************************/////////////****************////////////*******///");

		return model;

	}

}
