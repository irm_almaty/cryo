package com.cryobank.controller;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.cryobank.annotations.Link;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.cryobank.medialog.domain.Iuser;
import com.cryobank.postgres.domain.Subscript;
import com.cryobank.service.IuserService;
import com.cryobank.service.UserContextService;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class LoginController extends AbstractCryobankController {

	// @Autowired
	// private IuserService iuserService;

	@Autowired
	UserContextService userContextService;

	@Autowired
	IuserService iuserService;

	private final Log logger = LogFactory.getLog(LoginController.class);

	@Link(index = false)
	@RequestMapping(value = "/login/loginpage", method = RequestMethod.GET)
	public String login(ModelMap model) {

		Subscript subscript = new Subscript();

		model.addAttribute("subscript", subscript);

		return "loginPage";

	}

	@Link(index = false)
	@RequestMapping(value = "/login/loginPage", method = RequestMethod.GET)
	public String loginRedirect(RedirectAttributes attributes) {
		attributes.addFlashAttribute("loginPage");
		return "redirect:../login/loginpage";
	}

	/**
	 * This method called when spring security return true
	 * 
	 * @param model
	 * @param session
	 * @return
	 */
	@Link(index = false)
	@RequestMapping(value = "/login/loginPass", method = RequestMethod.GET)
	public String loginPass(Model map, HttpSession session, HttpServletRequest req,RedirectAttributes attributes) {
		logger.info("********************** loginPass() ****************************Start");

		String userName = SecurityContextHolder.getContext().getAuthentication().getName();

		Collection<? extends GrantedAuthority> roleNames = SecurityContextHolder.getContext().getAuthentication()
				.getAuthorities();
		String roleName = "";
		for (GrantedAuthority role : roleNames) {
			roleName = role.getAuthority();
			logger.info("********************** user.roleName() " + roleName);
			break;
		}
		String retstr = "";

		attributes.addFlashAttribute("login");
		if ("ROLE_ADMIN".equalsIgnoreCase(roleName)) {

			retstr = "redirect:../admin/adminPage";

		} else if

		("ROLE_USER".equalsIgnoreCase(roleName)) {
			retstr = "redirect:../user/userPage";
		}

		else if

		("ROLE_MANAGER".equalsIgnoreCase(roleName)) {
			retstr = "redirect:../user/userPage";
		}

		else if

		("ROLE_SPERM".equalsIgnoreCase(roleName)) {
			retstr = "redirect:../donor/sperm/list";
		}

		else if

		("ROLE_OOCYT".equalsIgnoreCase(roleName)) {
			retstr = "redirect:../donor/oocyte/list";
		}

		else if

		("ROLE_SUR_MOM".equalsIgnoreCase(roleName)) {
			retstr = "redirect:../donor/surmom/list";
		}

		else if

		("ROLE_SUR_MOM_OOCYT".equalsIgnoreCase(roleName)) {
			retstr = "redirect:../donor/surmom/list";
		}

		else if

		("ROLE_SUR_MOM_SPERM".equalsIgnoreCase(roleName)) {
			retstr = "redirect:../donor/surmom/list";
		}
		
		else if

		("ROLE_LAB".equalsIgnoreCase(roleName)) {
			retstr = "redirect:../laboratory/list";
		}

		logger.info("********************** Logged In successfully by user  --  " + userName);

		return retstr;

	}

	@Link(index = false)
	@RequestMapping(value = "/login/loginFailed")
	public ModelAndView loginFailed() {

		logger.info("********************** loginFailed() ****************************END");

		ModelAndView model = new ModelAndView("redirect:loginPage");

		String er = "������ �����������";

		model.addObject("er", er);

		return model;

	}
	/*
	 * 
	 * @RequestMapping(value = "/login/loginFailed", method = RequestMethod.GET)
	 * public String loginFailed(Model map, HttpSession session) {
	 * 
	 * logger.info(
	 * "********************** loginFailed() ****************************END");
	 * 
	 * 
	 * 
	 * return "redirect:loginPage";
	 * 
	 * }
	 * 
	 */

	@Link(index = false)
	@RequestMapping(value = "/login/logout")
	public String logout(Model map, HttpServletRequest req) {

		logger.info("*******************************session invalidated");
		req.getSession().invalidate();
		SecurityContextHolder.clearContext();
		return "redirect:loginPage";
	}

}