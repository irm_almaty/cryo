package com.cryobank.controller.rest;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cryobank.postgres.domain.Article;
import com.cryobank.service.ArticleService;

@Controller
@RestController
@RequestMapping("/rest")
public class RestContoller {

	@Autowired
	ArticleService articleService;

	@RequestMapping(value = "/test", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody List<Article> getArcticle() {

		List<Article> listArticles = new ArrayList<>();

		for (int i = 0; i < 20; i++) {
			Article article = new Article();
			article.setId(i);
			article.setTitle("title" + i);
			listArticles.add(article);

		}
		return listArticles;

	}

	@RequestMapping(value = "/post", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody Article createArcticle(@RequestBody Article article) {

		articleService.createArticle(article);
		return article;
	}

}
