package com.cryobank.controller;

import java.io.IOException;
import java.io.InputStream;

import javax.mail.Message;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.sun.mail.handlers.message_rfc822;

@Controller
public class SendMailController {
	final String emailTO = "suleimen_1@mail.ru";

	@Autowired
	private JavaMailSender mailSender;

	//@RequestMapping("/mail")
	public ModelAndView getMainPage2() {
		ModelAndView model = new ModelAndView("feedbackForm");
		return model;
	}

	//@RequestMapping(value = "/mailSend", method = { RequestMethod.POST, RequestMethod.GET })
	public ModelAndView doSendEmail(HttpServletRequest request, final @RequestParam CommonsMultipartFile attachFile) {

		final String emailTo="suleimen_1@mail.ru";
		ModelAndView model = new ModelAndView("feedbackFormResult");
		// takes input from e-mail form
		String recipientAddress = request.getParameter("recipient");
		final String subject = request.getParameter("subject");
		final String message = request.getParameter("message");
		// prints debug info
		System.out.println("emailTo: " + emailTo);
		System.out.println("subject: " + subject);
		System.out.println("message: " + message);
		System.out.println("attachFile: " + attachFile.getOriginalFilename());
		
		
		mailSender.send(new MimeMessagePreparator() {
			@Override
			public void prepare(MimeMessage mimeMessage) throws Exception {
				MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage, true, "UTF-8");
				messageHelper.setTo(emailTo);
				messageHelper.setSubject(subject);
				messageHelper.setText(message);

				// determines if there is an upload file, attach it to the
				// e-mail
				String attachName = attachFile.getOriginalFilename();
				if (!attachFile.equals("")) {

					messageHelper.addAttachment(attachName, new InputStreamSource() {

						@Override
						public InputStream getInputStream() throws IOException {
							return attachFile.getInputStream();
						}
					});
				}
			}
		});
		// forwards to mainPage
		return model;
	}
	
}
