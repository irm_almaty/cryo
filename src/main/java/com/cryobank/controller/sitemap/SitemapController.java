package com.cryobank.controller.sitemap;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by Suleiman on 20.11.2017.
 */
@Controller
public class SitemapController {

    @RequestMapping(value = "/sitemap.xml", method = RequestMethod.GET)
    @ResponseBody
    public XmlUrlSet main() {
        XmlUrlSet xmlUrlSet = new XmlUrlSet();

        create(xmlUrlSet, "", XmlUrl.Priority.HIGH);
        create(xmlUrlSet, "/donor/sperm/list", XmlUrl.Priority.HIGH);
        create(xmlUrlSet, "/donor/oocyte/list", XmlUrl.Priority.HIGH);
        create(xmlUrlSet, "/donor/surmom/list", XmlUrl.Priority.HIGH);

        create(xmlUrlSet, "/form/oocyt", XmlUrl.Priority.HIGH);
        create(xmlUrlSet, "/form/sperm", XmlUrl.Priority.HIGH);
        create(xmlUrlSet, "/form/surmom", XmlUrl.Priority.HIGH);

        create(xmlUrlSet, "/contact", XmlUrl.Priority.MEDIUM);

        return xmlUrlSet;
    }

    private void create(XmlUrlSet xmlUrlSet, String link, XmlUrl.Priority priority) {
        xmlUrlSet.addUrl(new XmlUrl("http://cryobank.kz" + link, priority));
    }

}
