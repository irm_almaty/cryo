package com.cryobank.controller;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;


public class AppAdapter extends WebMvcConfigurerAdapter {

	@Bean
	public DonorInterceptor donorInterceptor() {
		return new DonorInterceptor();
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		// registry.addInterceptor(donorInterceptor()).addPathPatterns("/donor/**");

		registry.addInterceptor(new DonorInterceptor()).addPathPatterns("/**");

		
		
	}

}
