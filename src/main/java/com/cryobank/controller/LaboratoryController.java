package com.cryobank.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.cryobank.medialog.domain.DonorManMedialog;
import com.cryobank.medialog.domain.Iuser;
import com.cryobank.medialog.domain.PatientMedialog;
import com.cryobank.postgres.domain.Order;
import com.cryobank.postgres.domain.OrderStatus;
import com.cryobank.postgres.domain.QueryValue;
import com.cryobank.postgres.domain.dto.Action;
import com.cryobank.postgres.domain.dto.OrderDTO;
import com.cryobank.service.DonorManService;
import com.cryobank.service.IuserService;
import com.cryobank.service.OrderService;
import com.cryobank.service.PatientService;
import com.cryobank.service.UserContextService;
import com.cryobank.util.email.EmailService;

@Controller
@RequestMapping("/orders")
public class LaboratoryController {

	@Autowired
	private UserContextService userContextService;

	@Autowired
	private EmailService emailService;

	@Autowired
	private OrderService orderService;

	@Autowired
	IuserService iuserService;

	@Autowired
	DonorManService donorManService;

	@Autowired
	PatientService patientService;

	@RequestMapping("/list")
	public ModelAndView listOrders(@RequestParam(value = "status", required = false) String status,
			@ModelAttribute("iuser") Iuser iuser) {

		ModelAndView model = null;
		if ("ROLE_MANAGER".equals(iuser.getRole().getRoleName()) || "MANAGER".equals(iuser.getUserAccess())) {
			if (status == null) {
				status = OrderStatus.WAITS.toString();
			}
			model = new ModelAndView("managerOrderList");
		}
		if ("ROLE_LAB".equals(iuser.getRole().getRoleName())) {
			if (status == null) {
				status = OrderStatus.DOSSIER.toString();
			}
			model = new ModelAndView("labOrderList");
		}
		if ("ROLE_USER".equals(iuser.getRole().getRoleName())) {
			if (status == null) {
				status = OrderStatus.WAITS.toString();
			}
			model = new ModelAndView("userOrderList");
		}
		if ("ROLE_ADMIN".equals(iuser.getRole().getRoleName()) && "ALL".equals(iuser.getUserAccess())) {
			model = new ModelAndView("adminOrderList");
		}
		List<Order> listOrder = orderService.getAllOrdersByStatus(status);
		QueryValue counts = orderService.getOrderCounts();
		model.addObject("counts", counts);
		model.addObject("list", listOrder);
		return model;
	}

	@RequestMapping("/order/{orderId}")
	public ModelAndView getOrderDetail(@PathVariable int orderId, @ModelAttribute("iuser") Iuser iuser) {
		ModelAndView model = null;
		if ("ROLE_MANAGER".equals(iuser.getRole().getRoleName())) {
			model = new ModelAndView("managerOrder");
		} else if ("ROLE_LAB".equals(iuser.getRole().getRoleName())) {
			model = new ModelAndView("labOrder");
		} else if ("ROLE_ADMIN".equals(iuser.getRole().getRoleName())) {
			model = new ModelAndView("adminOrder");
		}
		Order order = orderService.getOrderById(orderId);
		PatientMedialog patientMedialog = patientService.getPatientMedialogById(order.getIuserId());
		model.addObject("order", order);
		model.addObject("patient", patientMedialog);
		DonorManMedialog donor = donorManService.getDonor(order.getDonorId());
		List<Action> actionList = new ArrayList<>();
		Action actionConsider = new Action(OrderStatus.CONSIDER.toString(), "�� ������������");
		Action actionDossier = new Action(OrderStatus.DOSSIER.toString(), "�������� ���������");
		Action actionDisagree = new Action(OrderStatus.DISAGREE.toString(), "C������� �� ���������");
		Action actionAccepted = new Action(OrderStatus.ACCEPTED.toString(), "������� � ������");
		Action actionApproved = new Action(OrderStatus.APPROVED.toString(), "����� ���������");
		Action actionRejected = new Action(OrderStatus.REJECTED.toString(), "�������� �����");
		Action actionArchive = new Action(OrderStatus.ARCHIVED.toString(), "� �����");
		if (OrderStatus.WAITS.equals(order.getOrderStatus())) {
			actionList.add(actionConsider);
		}
		if (OrderStatus.CONSIDER.equals(order.getOrderStatus())) {
			actionList.add(actionDossier);
			actionList.add(actionDisagree);
		}
		if (OrderStatus.DOSSIER.equals(order.getOrderStatus()) && "ROLE_LAB".equals(iuser.getRole().getRoleName())) {
			actionList.add(actionAccepted);
		}
		if (OrderStatus.ACCEPTED.equals(order.getOrderStatus())) {
			actionList.add(actionApproved);
			actionList.add(actionRejected);
		}
		if (OrderStatus.REJECTED.equals(order.getOrderStatus())) {
			actionList.add(actionArchive);
		}
		model.addObject("actions", actionList);
		return model;
	}

	@RequestMapping("/order/action")
	public String actionOrder(@RequestParam(name = "order", required = true) int orderId,
			@RequestParam(name = "status", required = true) String status) {
		Order order = orderService.getOrderById(orderId);
		order.setOrderStatus(OrderStatus.valueOf(status));
		orderService.updateOrder(order);
		return "redirect:/orders/list";
	}
}
