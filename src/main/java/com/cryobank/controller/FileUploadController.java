package com.cryobank.controller;

import java.io.File;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.crypto.password.StandardPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.cryobank.postgres.domain.Man;
import com.cryobank.postgres.domain.Photo;

@Controller
public class FileUploadController {

	Photo photoDonorMan = new Photo();

	private String saveDirectory = "C:/test/";

	@RequestMapping("/file")
	public ModelAndView uploadFileForm() {
		String text = "ddddd";
		ModelAndView model = new ModelAndView("fileUpload");
		return model;
	}

	@RequestMapping("/resources/img/")
	public ModelAndView uploadFileFormTest() {
		ModelAndView model = new ModelAndView("fileUpload");
		return model;
	}

	@RequestMapping("/upload")
	public ModelAndView uploadFile(HttpServletRequest request, @RequestParam CommonsMultipartFile[] fileUpload)
			throws Exception {

		/* in PRODUCTION */
		String rootDirectory = request.getSession().getServletContext().getRealPath("/");
		String uploadDir = rootDirectory + "\\resources\\img\\";

		
		Date date = new Date();

		
		//String namePhoto = result + aFile.getOriginalFilename();
		
		
		/* in DEV */
		

		ModelAndView model = new ModelAndView("mainPage");
		System.out.println("description: " + request.getParameter("description"));

		if (fileUpload != null && fileUpload.length > 0) {
			for (CommonsMultipartFile aFile : fileUpload) {

				StandardPasswordEncoder encoder = new StandardPasswordEncoder(aFile.getOriginalFilename());
				String result = encoder.encode(date.toString());
				
				String uploadFolder = "C:/STS.springMVC.project/com.cryobank/src/main/webapp/resources/image"+result
						+ request.getParameter("description");
				
				String name = aFile.getOriginalFilename() + request.getParameter("description");

				System.out.println("Saving file: " + name);

				if (!aFile.getOriginalFilename().equals("")) {
					aFile.transferTo(new File(uploadFolder + aFile.getOriginalFilename()));
				}
			}
		}
		return model;
	}
}
