<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>


<!-- *** TESTIMONIALS CAROUSEL ***
 _________________________________________________________ -->

		<ul class="owl-carousel testimonials same-height-row">
			<li class="item">
				<div class="testimonial same-height-always">
					<div class="text">
						<p>One morning, when Gregor Samsa woke from troubled dreams,
							he found himself transformed in his bed into a horrible vermin.
							He lay on his armour-like back, and if he lifted his</p>
					</div>
					<div class="bottom">
						<div class="icon">
							<i class="fa fa-quote-left"></i>
						</div>
						<div class="name-picture">
							<img class="" alt=""
								src="<c:url value="/resources/img/1234.jpg"/>">
							<h5>John McIntyre</h5>
							<p>CEO, TransTech</p>
						</div>
					</div>
				</div>
			</li>
			<li class="item">
				<div class="testimonial same-height-always">
					<div class="text">
						<p>The bedding was hardly able to cover it and seemed ready to
							slide off any moment. His many legs, pitifully thin compared with
							the size of the rest of him, waved about helplessly as he looked.
							"What's happened to me? " he thought. It wasn't a dream.</p>
					</div>
					<div class="bottom">
						<div class="icon">
							<i class="fa fa-quote-left"></i>
						</div>
						<div class="name-picture">
							<img class="" alt=""
								src="<c:url value="/resources/img/1234.jpg"/>">



							<h5>John McIntyre</h5>
							<p>CEO, TransTech</p>
						</div>
					</div>
				</div>
			</li>
			<li class="item">
				<div class="testimonial same-height-always">
					<div class="text">
						<p>His room, a proper human room although a little too small,
							lay peacefully between its four familiar walls.</p>

						<p>A collection of textile samples lay spread out on the table
							- Samsa was a travelling salesman - and above it there hung a</p>
					</div>
					<div class="bottom">
						<div class="icon">
							<i class="fa fa-quote-left"></i>
						</div>
						<div class="name-picture">
							<img class="" alt=""
								src="<c:url value="/resources/img/1234.jpg"/>">
							<h5>John McIntyre</h5>
							<p>CEO, TransTech</p>
						</div>
					</div>
				</div>
			</li>
			<li class="item">
				<div class="testimonial same-height-always">
					<div class="text">
						<p>It showed a lady fitted out with a fur hat and fur boa who
							sat upright, raising a heavy fur muff that covered the whole of
							her lower arm towards the viewer.</p>
					</div>

					<div class="bottom">
						<div class="icon">
							<i class="fa fa-quote-left"></i>
						</div>
						<div class="name-picture">
							<img class="" alt=""
								src="<c:url value="/resources/img/1234.jpg"/>">
							<h5>John McIntyre</h5>
							<p>CEO, TransTech</p>
						</div>
					</div>
				</div>
			</li>
			<li class="item">
				<div class="testimonial same-height-always">
					<div class="text">
						<p>It showed a lady fitted out with a fur hat and fur boa who
							sat upright, raising a heavy fur muff that covered the whole of
							her lower arm towards the viewer.</p>
					</div>

					<div class="bottom">
						<div class="icon">
							<i class="fa fa-quote-left"></i>
						</div>
						<div class="name-picture">
							<img class="" alt=""
								src="<c:url value="/resources/img/1234.jpg"/>">
							<h5>John McIntyre</h5>
							<p>CEO, TransTech</p>
						</div>
					</div>
				</div>
			</li>
		</ul>
		<!-- /.owl-carousel -->
		<!-- *** TESTIMONIALS CAROUSEL END *** -->


</body>
</html>