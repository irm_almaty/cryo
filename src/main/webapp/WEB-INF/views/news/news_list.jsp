<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>12${news.title}</title>

<meta name="description" content="12${news.metaDescription}">
<meta name="keywords" content="12${news.metaKeywords}">

</head>



	<h3>${msg}</h3>

	<sec:authorize access="hasRole('ROLE_ADMIN')">
		<h4>
			<a href="<c:url value="/news/createNews" />"> Добавить новость <span
				class="glyphicon glyphicon-duplicate"> </span>
			</a>
		</h4>
	</sec:authorize>

	<c:forEach items="${listNews}" var="news">
		<div class="col-sm-4 col-md-3" style="padding-bottom: 5px">
			<div class="thumbnail">
				<div class="caption">

					<h3>код: ${news.newsId}</h3>
					<h5>название:${news.title}</h5>
					<p ng-controller="donorController">
						<a href="<c:out value='news/viewNews?id=${news.newsId}'/>"> <span
							class="glyphicon glyphicon-eye-open"> </span> Просмотр
						</a>
					</p>

					<p>превью: ${news.preview}</p>


					<sec:authorize access="hasRole('ROLE_ADMIN')">

						<p ng-controller="newsController">
							<a href="<c:out value='news/editNews?id=${news.newsId}'/>"> <span
								class="glyphicon glyphicon-edit"> </span> Изменить
							</a> <a href="<c:out value='news/deleteNews?id=${news.newsId}'/>">
								<span class="glyphicon glyphicon-remove"> </span> Удалить
							</a>
						</p>

					</sec:authorize>

				</div>
			</div>
		</div>
	</c:forEach>



</html>