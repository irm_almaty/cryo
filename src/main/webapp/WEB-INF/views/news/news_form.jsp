<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://ckeditor.com" prefix="ckeditor"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>news form</title>

<script>
	CKEDITOR.editorConfig = function(config) {
		// misc options
		config.height = '850px';
	};
</script>

</head>


<body>


	<div class="panel-body">
		<form:form id="newsRegisterForm" cssClass="form-horizontal"
			modelAttribute="news" method="post" action="saveNews">

			<div class="form-group">
				<div class="control-label col-xs-2">
					<form:label path="newsId">newsId</form:label>
				</div>
				<div class="col-md-10">
					<form:input cssClass="form-control" path="newsId"
						value="${news.newsId}" />
				</div>
			</div>

			<div class="form-group">
				<div class="control-label col-xs-2">
					<form:label path="title">title</form:label>
				</div>
				<div class="col-md-10">
					<form:input cssClass="form-control" path="title"
						value="${news.title}" />
				</div>
			</div>

			<div class="form-group">
				<div class="control-label col-xs-2">
					<form:label path="preview">preview</form:label>
				</div>
				<div class="col-md-10">
					<form:input cssClass="form-control" path="preview"
						value="${news.preview}" />
				</div>
			</div>

			<div class="form-group">
				<div class="control-label col-xs-2">
					<form:label path="metaDescription">metaDescription</form:label>
				</div>
				<div class="col-md-10">
					<form:input cssClass="form-control" path="metaDescription"
						value="${news.metaDescription}" />
				</div>
			</div>

			<div class="form-group">
				<div class="control-label col-xs-2">
					<form:label path="metaKeywords">metaKeywords</form:label>
				</div>
				<div class="col-md-10">
					<form:input cssClass="form-control" path="metaKeywords"
						value="${news.metaKeywords}" />
				</div>
			</div>


			<div class="form-group">

				<div class="col-md-12">
					<p>контент</p>
					<form:textarea id="editor1" path="content" />
				</div>
			</div>



			<div class="form-group">
				<div class="row">
					<div class="col-xs-2"></div>
					<div class="col-xs-2">
						<input type="submit" class="btn btn-primary" value="Save" />
					</div>

				</div>
			</div>


		</form:form>
		<ckeditor:replace replace="editor1"
			basePath="/resources/ckeditor/" />
	</div>



</body>
</html>