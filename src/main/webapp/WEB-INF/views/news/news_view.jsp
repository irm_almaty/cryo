<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${news.title}</title>
<meta name="description" content="${news.metaDescription}">
<meta name="keywords" content="${news.metaKeywords}">

</head>
<body>

	<div class="col-xs-9">

		<h3>код: ${news.newsId}</h3>

		<h3>название:${news.title}</h3>

		<p>превью: ${news.preview}</p>

		<p>${news.content}</p>
		
		<p>${pageContext.request.requestURL}</p>
		${pageContext.request.contextPath}

	</div>

	<div class="col-xs-3">
		<h4>Новостная колонка</h4>



		<c:forEach items="${listNews}" var="news">
			<div class="thumbnail">
				<p>название:${news.title}</p>
				<p>дата: ${news.createDate}</p>

			</div>
		</c:forEach>
	</div>


</body>
</html>