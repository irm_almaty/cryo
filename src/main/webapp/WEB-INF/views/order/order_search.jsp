<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="joda" uri="http://www.joda.org/joda/time/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<head>

<link rel="stylesheet"
	href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<title>Order Search</title>




<script>
	$(function() {
		$("#userId").selectmenu();

		$("#orderStatus").selectmenu();

		$("#number").selectmenu().selectmenu("menuWidget").addClass("overflow");
	});
</script>

<style>
.ui-accordion .ui-accordion-header {
	display: block;
	cursor: pointer;
	position: relative;
	margin: 2px 0 0 0;
	padding: .em .5em .5em .7em;
	min-height: 0; /* support: IE7 */
	font-size: 130%;
}
</style>


<style>
fieldset {
	border: 0;
}

label {
	display: block;
	margin: 30px 0 0 0;
}

select {
	width: 200px;
}

.overflow {
	height: 200px;
}
</style>



</head>
<body>





	<form:form id="newsRegisterForm" cssClass="form-horizontal"
		method="post" action="searchOr">

		<div class="row">
			<div class="col-md-12">



				<div class="col-md-3">
					 <select name="orderStatus" id="orderStatus">
						<option value="">статус заказа</option>
						<option value="1">новый заказ</option>
						<option value="2">в обработке</option>
						<option value="3">обработанный</option>
					</select>
				</div>




				<div class="col-md-3">
					 <select name="userId" id="userId">
						<option value="">Выберите User</option>
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
					</select>
				</div>


				<div class="col-md-6">
					<input class="btn btn-success" type='submit' value='Поиск' />
				</div>
				
				
			</div>
			
			
		</div>
	</form:form>



	<table class="table table-hover">
		<thead>
			<tr>
				<th>Номер заказа</th>
				<th>Имя донора</th>
				<th>Заказчик</th>
				<th>Статус заказа</th>
				<th>Дата/Время заказа</th>
			</tr>
		</thead>
		<c:forEach items="${searchList}" var="order">
			<tr>
				<td>${order.id}</td>
				<td>${order.getDonor().getDonorName()}</td>
				<td>${order.getIuser().getUserName()}</td>
				<td>${order.getOrderStatus().getStatusName()}</td>
				<td><joda:format value="${order.getOrderRegDate()}" style="SM" /></td>

			</tr>
		</c:forEach>
	</table>

</body>
</html>