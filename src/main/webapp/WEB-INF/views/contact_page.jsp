<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<body>
	<div class="container" >
		<div class="heading" >
			<h1 class="info">Контакты</h1>
		</div>
		<div class="col-md-12" style="margin-top: 0pt">
			<div class="col-md-3">
				<div class="box-simple">
					<h3>
						Режим работы </br>Call центра
					</h3>
					<p>Пн-Пт 08:00 - 19:00</p>
					<p>Сб 08:00 - 16:00</p>
					<p>Вс Выходной</p>
				</div>
			</div>
			<div class="col-md-3">
				<div class="box-simple">
					<h3>Наш адрес</h3>
					<p>Республика Казахстан</p>
					<p>050012, г.Алматы</p>
					<p>ул.Толе би 99 угол ул.</p>
					<p>А.Байтурсынова</p>
				</div>
			</div>
			<div class="col-md-3">
				<div class="box-simple">
					<h3>Наши телефоны</h3>
					<p>+7 (727) 234-34-34 (городской)</p>
					<p>+7 7719-34-34-34 (моб.Beeline)</p>
					<p>многоканальные</p>
				</div>
			</div>
			<div class="col-md-3">
				<div class="box-simple">
					<h3>Факсы</h3>
					<p>+7 (727) 260-12-80</p>
					<p>+7 (727) 382-83-13</p>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-12" style="margin-top: 0pt">
		<div id="map"></div>
	</div>
</body>
<script
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCahuejjlsD0Eiv3HpUkYrzUmV2fpNHVeU"
	type="text/javascript"></script>
<script>
	$(function() {

		map();

	});

	function map() {
		var styles = [ {
			"featureType" : "landscape",
			"stylers" : [ {
				"saturation" : -100
			}, {
				"lightness" : 65
			}, {
				"visibility" : "on"
			} ]
		}, {
			"featureType" : "poi",
			"stylers" : [ {
				"saturation" : -100
			}, {
				"lightness" : 51
			}, {
				"visibility" : "simplified"
			} ]
		}, {
			"featureType" : "road.highway",
			"stylers" : [ {
				"saturation" : -100
			}, {
				"visibility" : "simplified"
			} ]
		}, {
			"featureType" : "road.arterial",
			"stylers" : [ {
				"saturation" : -100
			}, {
				"lightness" : 30
			}, {
				"visibility" : "on"
			} ]
		}, {
			"featureType" : "road.local",
			"stylers" : [ {
				"saturation" : -100
			}, {
				"lightness" : 40
			}, {
				"visibility" : "on"
			} ]
		}, {
			"featureType" : "transit",
			"stylers" : [ {
				"saturation" : -100
			}, {
				"visibility" : "simplified"
			} ]
		}, {
			"featureType" : "administrative.province",
			"stylers" : [ {
				"visibility" : "off"
			} ]
		}, {
			"featureType" : "water",
			"elementType" : "labels",
			"stylers" : [ {
				"visibility" : "on"
			}, {
				"lightness" : -25
			}, {
				"saturation" : -100
			} ]
		}, {
			"featureType" : "water",
			"elementType" : "geometry",
			"stylers" : [ {
				"hue" : "#ffff00"
			}, {
				"lightness" : -25
			}, {
				"saturation" : -97
			} ]
		} ];
		map = new GMaps({
			el : '#map',
			lat : 43.253903,
			lng : 76.926481,
			zoom : 17,
			zoomControl : true,
			zoomControlOpt : {
				style : 'BIG',
				position : 'TOP_LEFT'
			},
			panControl : true,
			streetViewControl : true,
			mapTypeControl : false,
			overviewMapControl : false,
			scrollwheel : true,
			draggable : true,
			styles : styles
		});
		var image = 'http://cryobank.kz/resources/img/marker_cryo.png';
		map.addMarker({
			lat : 43.253903,
			lng : 76.926481,
			icon : image
		/* ,
			 title: '',
			 infoWindow: {
			 content: '<p>HTML Content</p>'
			 }*/
		});
	}
</script>