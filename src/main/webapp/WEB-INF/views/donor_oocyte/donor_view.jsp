<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://ckeditor.com" prefix="ckeditor"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Donor view</title>
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<link href="<c:url value="/resources/css/lightbox.css"/>"
	rel="stylesheet">



<style>
button.accordion {
	background-color: #eee;
	color: #444;
	cursor: pointer;
	padding: 18px;
	width: 100%;
	border: none;
	text-align: left;
	outline: none;
	font-size: 15px;
	transition: 0.0s;
	outline: none;
}

button.accordion.active, button.accordion:hover {
	background-color: #70BDFF;
}

div.panel {
	padding: 0 10px;
	background-color: white;
	max-height: 0;
	overflow: hidden;
	transition: 0.1s ease-in-out;
	opacity: 0;
}

div.panel.show {
	opacity: 1;
	max-height: 5000px;
}

.btn-lg {
	padding: 18px 16px;
	font-size: 16px;
	line-height: 1.33;
	border-radius: 0;
}
</style>



</head>

<div class="heading">
	<h3>Просмотр донора:</h3>
</div>
<body>
	<div class="col-md-3" style="margin-bottom: 10pt;">
		<h4>Код донора: ${donorWoman.donorCode}</h4>
		<p>
			Национальность: <strong>${donorWoman.nation}</strong>
		</p>
		<c:if test="${donorWoman.rod!=null}">
			<p>
				Род: <strong>${donorWoman.rod}</strong>
			</p>
		</c:if>
		<p>
			Знак зодиака:<strong> ${donorWoman.zodiak}</strong>
		</p>
		<p>
			Группа крови:<strong> ${donorWoman.grKrovi}</strong>
		</p>
		<p>
			Резус фактор:<strong> ${donorWoman.rezus}</strong>
		</p>

		<a href="order/?id=${donorWoman.donorWomanId}">
			<div class="btn btn-small btn-template-main"
				style="margin-top: 10pt;">Зарезервировать</div>
		</a>


	</div>

	<div class="col-md-9 " style="margin-bottom: 10pt; margin-top: 10pt";>
		<button type="button" class="btn btn-lg btn-info" style="width: 100%">
			Информация о доноре</button>
		<div class="panel">
			<div class="col-md-10" style="margin-bottom: 10pt; margin-top: 10pt";>
				<p>
					Национальность: <strong>${donorWoman.nation}</strong>
				</p>

				<c:if test="${donorWoman.rod!=null}">
					<p>
						Род: <strong>${donorWoman.rod}</strong>
					</p>
				</c:if>
				<p>
					Возраст:<strong> ${donorWoman.vozrast}</strong>
				</p>

				<p>
					Семейное положение:<strong> ${donorWoman.famPoloj}</strong>
				</p>

				<p>
					Знак зодиака:<strong> ${donorWoman.zodiak}</strong>
				</p>

				<p>
					Группа крови:<strong> ${donorWoman.grKrovi}</strong>
				</p>

				<p>
					Резус фактор:<strong> ${donorWoman.rezus}</strong>
				</p>

				<p>
					Образование: <strong>${donorWoman.obrazovanie}</strong>
				</p>
				<p>
					Профессия: <strong>${donorWoman.profession}</strong>
				</p>
				
				<p>
					Телефон:<strong> ${donorWoman.phoneNumber}</strong>
				</p>

				<p>
					Дата создания анкеты:<strong> ${donorWoman.createDate}</strong>
				</p>

				<p>
					Дата последних изменений анкеты:<strong>
						${donorWoman.zapolnDate}</strong>
				</p>

			</div>
		</div>

		<button type="button" class="btn btn-lg btn-info" style="width: 100%">
			Фенотипические данные:</button>
		<div class="panel">
			<div class="col-md-10" style="margin-bottom: 10pt; margin-top: 10pt";>
				<p>
					Рост: <strong>${donorWoman.rost}</strong>
				</p>
				<p>
					Вес: <strong>${donorWoman.ves}</strong>
				</p>

				<p>
					Телосложение: <strong>${donorWoman.bodyType}</strong>
				</p>

				<p>
					Размер обуви: <strong>${donorWoman.footSize}</strong>
				</p>

				<p>
					Размер одежды:: <strong>${donorWoman.clothSize}</strong>
				</p>

				<p>
					Цвет кожи:: <strong>${donorWoman.skinColor}</strong>
				</p>

				<p>
					Волосы: <strong>${donorWoman.tipVolos}</strong>
				</p>

				<p>
					Цвет волос: <strong>${donorWoman.cvetVolos}</strong>
				</p>

				<p>
					Форма лица: <strong>${donorWoman.lico}</strong>
				</p>
				<p>
					Лоб: <strong>${donorWoman.lob}</strong>
				</p>

				<p>
					Разрез глаз: <strong>${donorWoman.razrezGlaz}</strong>
				</p>

				<p>
					Цвет глаз: <strong>${donorWoman.cvetGlaz}</strong>
				</p>

				<p>
					Скулы: <strong>${donorWoman.skuli}</strong>
				</p>
				<p>
					Форма носа: <strong>${donorWoman.nos}</strong>
				</p>
				<p>
					Форма губ: <strong>${donorWoman.gubi}</strong>
				</p>
			</div>
		</div>

		<button type="button" class="btn btn-lg btn-info" style="width: 100%">
			Опрос донора:</button>
		<div class="panel">
			<div class="col-md-10" style="margin-bottom: 10pt; margin-top: 10pt";>

				<p>
					Состояние зрения: <strong>${donorWoman.zrenie}</strong>
				</p>

				<p>
					Ношение очков: <strong>${donorWoman.ochki}</strong>
				</p>

				<p>
					Правша/Левша: <strong>${donorWoman.glavHand}</strong>
				</p>

				<p>
					Охарактеризовать себя: <strong>${donorWoman.harakSebya}</strong>
				</p>

				<p>
					Сильные стороны характера: <strong>${donorWoman.silHarak}</strong>
				</p>

				<p>
					Слабые стороны характера: <strong>${donorWoman.slabHarak}</strong>
				</p>

				<p>
					Какой спорт предпочитаете?: <strong>${donorWoman.sport}</strong>
				</p>

				<c:if test="${donorWoman.hobbi!=null}">
					<p>
						Хобби: <strong>${donorWoman.hobbi}</strong>
					</p>
				</c:if>
				<p>
					Любимый цвет: <strong>${donorWoman.lubCvet}</strong>
				</p>

				<p>
					Играете ли вы на музыкальных инструментах?: <strong>${donorWoman.igraNaInstr}</strong>
				</p>

				<p>
					Любимое блюдо: <strong>${donorWoman.lubBlyudo}</strong>
				</p>

				<c:if test="${donorWoman.cennostiLive!=null}">
					<p>
						Ценности в жизни: <strong>${donorWoman.cennostiLive}</strong>
					</p>
				</c:if>

				<p>
					Цель жизни: <strong>${donorWoman.celLive}</strong>
				</p>
				<p>
					Аллергоанамнез: <strong>${donorWoman.allergiya}</strong>
				</p>
				<p>
					Аборт: <strong>${donorWoman.abort}</strong>
				</p>

				<p>
					Перенесенные болезни/операции, наследственные болезни: <strong>${donorWoman.perenesZabol}</strong>
				</p>

				<p>
					Перенесенные ЗППП: <strong>${donorWoman.perenesZPPP}</strong>
				</p>

				<p>
					Преценденты участия в подобной программе: <strong>${donorWoman.precendent}</strong>
				</p>

			</div>

			<c:if test="${!listChilds.isEmpty()}">
				<div class="col-md-10" style="margin-bottom: 5pt; margin-top: 0pt";>
					<h4>Родители донора:</h4>
					<div class="table-responsive">

						<table class="table">
							<thead>
								<tr>
									<th>Родитель</th>
									<th>Национальность</th>
									</th>
								</tr>
							</thead>
							<tbody>

								<tr>
									<td>Мать</td>
									<td>${mother.nation}</td>
									<td></td>
									<td></td>
									<td></td>
								</tr>

								<tr>
									<td>Отец</td>
									<td>${father.nation}</td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
							</tbody>
						</table>

					</div>
				</div>
			</c:if>


			<c:if test="${!listChilds.isEmpty()}">
				<div class="col-md-10" style="margin-bottom: 5pt; margin-top: 0pt";>
					<h4>Дети донора:</h4>
					<div class="table-responsive">

						<table class="table">
							<thead>
								<tr>
									<th>Пол</th>
									<th>Дата рождения</th>
									<th>Цвет глаз</th>
									<th>Цвет волос</th>
									<th>Вес при рождении</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${listChilds}" var="listChilds">
									<tr>
										<td>${listChilds.pol}</td>
										<td>${listChilds.birthday}</td>
										<td>${listChilds.cvetGlaz}</td>
										<td>${listChilds.cvetVolos}</td>
										<td>${listChilds.ves}</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>

					</div>
				</div>
			</c:if>
		</div>

		<button type="button" class="btn btn-lg btn-info" style="width: 100%">
			Фотографии:</button>
		<div class="panel">
			<div class="col-md-10" style="margin-bottom: 10pt; margin-top: 10pt";>
				<c:forEach items="${listPhoto}" var="photo">
					<div>
						<div class="col-md-3" style="margin-bottom: 5pt; margin-top: 5pt";>
							<a class="example-image-link"
								href="<c:url value="${photo.photoPath}"/>"
								data-lightbox="example-set" data-title="${photo.photoId}"><img
								class="example-image" src="<c:url value="${photo.photoPath}"/> "
								height="100" alt="" /></a>
						</div>
					</div>
				</c:forEach>
			</div>
		</div>

		<script>
			var acc = document.getElementsByClassName("btn btn-lg btn-info");
			var i;

			acc[0].onclick = function() {
				this.classList.toggle("active");
				this.nextElementSibling.classList.toggle("show");
			}

			for (i = 0; i < acc.length; i++) {

				acc[i].onclick = function() {
					this.classList.toggle("active");
					this.nextElementSibling.classList.toggle("show");
				}
			}
		</script>

	</div>
</body>

<script>
	lightbox.option({
		'resizeDuration' : 2000,
		'wrapAround' : true
	})
</script>

<script src="<c:url value="/resources/js/lightbox-plus-jquery.js"/>"></script>

</html>