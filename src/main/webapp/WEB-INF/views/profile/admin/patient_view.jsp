<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>


<style>
.btn span.glyphicon {
	opacity: 0;
}

.btn.active span.glyphicon {
	opacity: 1;
}
</style>
<style>
.col-md-12 {
	width: 100%;
}
</style>
</head>
<body>
	<form:form method="post" modelAttribute="regval" action="confim">

		<div class="col-md-12" style="margin-top: 40pt">
			<div class="col-md-2" style="margin-top: 0pt">
				<div class="heading">
					<h2>Пользователь</h2>
				</div>
				<h4>
					Данные при регистрации:</strong>
				</h4>
				<p>
					<strong>Ф.И.О. :</strong> ${patient.fio}
				</p>
				<p>
					<strong>дата рождения :</strong>


					<fmt:formatDate pattern="dd-MM-yyyy" value="${patient.birthday}" />
				</p>
				<p>
					<strong>email :</strong> ${patient.email}
				</p>
				<p>
					<strong>страна :</strong> ${patient.country}
				</p>
				<p>
					<strong>город:</strong> ${patient.city}
				</p>
				<p>
					<strong>телефон:</strong> ${patient.phone}
				</p>
				<p>
					<strong>доступ к разделу:</strong> ${patient.access}
				</p>
				<p>
					<strong>регистрация из:</strong> ${patient.registryFrom}
				</p>

				</br> </br>

			</div>

			<div class="col-md-10" style="margin-top: 30pt">

				<h3>${message}</h3>

				<h4>Совпадения по имени и фамилии:</h4>
				<table class="table table-stripped table-bordered">
					<thead>
						<tr>
							<th>ЭМК Медиалог</th>
							<th>email</th>
							<th>Ф.И.О.</th>
							<th>телефон</th>
							<th>Дата рождения</th>
						</tr>
					</thead>
					<c:forEach items="${names}" var="names">
						<tr>

							<td><form:radiobutton path="firstValue" value="${names.id}" />${names.id}<a
								href="<c:out value='patients/view?id=${names.id}'/>"><i
									class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></a></td>
							<td>${names.email}</td>
							<td>${names.name}</td>
							<td>${names.phone}</td>
							<td><fmt:formatDate pattern="dd-MM-yyyy"
									value="${names.birthday}" /></td>
						</tr>
					</c:forEach>
				</table>
				<script>
					$('input[type="checkbox"]').on(
							'change',
							function() {
								$(this).siblings('input[type="checkbox"]')
										.prop('checked', false);
							});
				</script>
				<br>

				<h4>Совпадения по email:</h4>
				<table class="table table-stripped table-bordered">
					<thead>
						<tr>

							<th>ЭМК Медиалог</th>
							<th>email</th>
							<th>Ф.И.О.</th>
							<th>телефон</th>
							<th>Дата рождения</th>
						</tr>
					</thead>
					<c:forEach items="${email}" var="email">
						<tr>

							<td><form:radiobutton path="secondValue" value="${email.id}" />${email.id}<a
								href="<c:out value='patients/view?id=${list.id}'/>"><i
									class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></a></td>
							<td>${email.email}</td>
							<td>${email.name}</td>
							<td>${email.phone}</td>
							<td><fmt:formatDate pattern="dd-MM-yyyy"
									value="${email.birthday}" /></td>
						</tr>
					</c:forEach>
				</table>
				<br>


				<h4>Данные доступа:</h4>
				<table class="table table-stripped table-bordered">
					<thead>
						<tr>
							<th>ЭМК Медиалог</th>
							<th>email</th>
							<th>пароль</th>
							<th>дата создания</th>
							<th>дата действия</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><form:radiobutton path="thirdValue"
									value="${iuser.userId}" />${iuser.userId}<a
								href="<c:out value='patients/view?id=${list.id}'/>"><i
									class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></a></td>
							<td>${iuser.userName}</td>
							<td>${iuser.password}</td>
							<td>${iuser.createDate}</td>
							<td><fmt:formatDate pattern="dd-MM-yyyy"
									value="${iuser.createDate}" /></td>
						</tr>
					</tbody>
				</table>
				<input class="btn btn-template-main" type="submit"
					value="Отправить пароль">
			</div>
		</div>
	</form:form>
	<div class="col-md-12" style="margin-top: 20pt"></div>
	<div class="container"></div>
</body>
</html>