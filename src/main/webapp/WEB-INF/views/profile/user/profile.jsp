<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Профиль пользователя</title>
</head>
<body>

	<div class="col-xs-9">

		<div class="heading">
			<h2>Профиль пользователя:</h2>
		</div>
		<p>
			<span style="font-size: 18px">Имя: <strong>
					${iuser.userName}</strong></span>
		</p>
		<p>
			<span style="font-size: 18px">Логин: <strong>
					${iuser.userLogin}</strong></span>
		</p>
		<p>
			<span style="font-size: 18px">Город: <strong>
					${iuser.userCity}</strong></span>
		</p>
		<p>
			<span style="font-size: 18px">Доступ к разделу: <strong>
					${iuser.userAccess}</strong></span>
		</p>
		<p>
			<span style="font-size: 18px">Пароль действителен до: <strong>
					${iuser.createDate}</strong></span>
		</p>

	</div>

</body>
</html>