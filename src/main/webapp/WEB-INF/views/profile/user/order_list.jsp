<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Order List</title>

<script
	src="<c:url value="https://raw.githubusercontent.com/mistic100/Bootstrap-Confirmation/master/bootstrap-confirmation.min.js"/>"></script>
</head>
<body>

	<div class="col-md-12" style="margin-top: 40pt">

		<div class="col-md-2" style="margin-top: 10pt">

			<div class="list-group"></div>

			<div class="list-group">
				<h3>Мои заявки:</h3>
				<a class="list-group-item" href="list?status=WAITS"><i
					class="glyphicon glyphicon-plus btn-lg"></i>в очереди</a> <a
					class="list-group-item" href="list?status=ACCEPTED"><i
					class="glyphicon glyphicon-ok btn-lg"></i> на рассмотрении</a> <a
					class="list-group-item" href="list?status=APPROVED"><i
					class="glyphicon glyphicon-ok-circle btn-lg"></i> взяты в резерв</a> <a
					class="list-group-item" href="list?status=REJECTED"><i
					class="glyphicon glyphicon-ban-circle btn-lg"></i> отмененые заявки</a>
				<a class="list-group-item" href="#"><i
					class="glyphicon glyphicon-inbox btn-lg"></i>в архиве</a>
			</div>
		</div>

		<div class="col-md-8" style="margin-top: 40pt; margin-left: 40pt">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>№ заявки</th>
						<th>код донора</th>
						<th>дата заявки</th>
						<th>статус заявки</th>
					</tr>
				</thead>
				<c:forEach items="${list}" var="list">
					<tr>
						<td><a href="<c:out value='view?id=${list.id}'/>"><i
								class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></a>
							${list.id}</td>

						<td><a href="<c:out value='/donor/${list.donorType.toString().toLowerCase()}/view?id=${list.donorId}'/>"><i
								class="glyphicon glyphicon-eye-open" aria-hidden="true"></i>
								${list.donorCode}</a> </</td>
						<td>${list.orderDate}</td>
						<td>${list.orderStatus}</td>
						<td><a
							href="<c:out value='action?order=${list.id}&status=CANCEL'/>"><i
								class="glyphicon glyphicon-ban-circle" aria-hidden="true"></i>
								отмена</a></td>
					</tr>
				</c:forEach>
			</table>
		</div>


		<div class="col-md-9" style="margin-top: 40pt; margin-left: 50pt">
			<h4>В резерв можно взять небольше одного донора!</h4>

		</div>
	</div>
	<div class="col-md-12" style="margin-top: 40pt"></div>
	<div class="container"></div>

</body>
</html>