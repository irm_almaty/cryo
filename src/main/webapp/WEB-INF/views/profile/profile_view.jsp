<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="joda" uri="http://www.joda.org/joda/time/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<link rel="stylesheet"
	href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<script>
	$(function() {
		$("#accordion").accordion({
			heightStyle : "content",
			collapsible : true

		});

	});
</script>

<script>
	$(function() {
		$("#accordion2").accordion({
			heightStyle : "content",
			collapsible : true

		});

	});
</script>






<style>
.ui-accordion .ui-accordion-header {
	display: block;
	cursor: pointer;
	position: relative;
	margin: 2px 0 0 0;
	padding: .em .5em .5em .7em;
	min-height: 0; /* support: IE7 */
	font-size: 130%;
}
</style>



<script>
	$(function() {
		$("#userId").selectmenu();

		$("#orderStatus").selectmenu();

		$("#number").selectmenu().selectmenu("menuWidget").addClass("overflow");

	});
</script>


<script>
	jQuery.cookie = function(name, value, options) {

		if (typeof value != 'undefined') { // name and value given, set cookie

			options = options || {};

			if (value === null) {

				value = '';

				options.expires = -1;

			}

			var expires = '';

			if (options.expires
					&& (typeof options.expires == 'number' || options.expires.toUTCString)) {

				var date;

				if (typeof options.expires == 'number') {

					date = new Date();

					date.setTime(date.getTime()
							+ (options.expires * 24 * 60 * 60 * 1000));

				} else {

					date = options.expires;

				}

				expires = '; expires=' + date.toUTCString(); // use expires attribute, max-age is not supported by IE

			}

			// CAUTION: Needed to parenthesize options.path and options.domain

			// in the following expressions, otherwise they evaluate to undefined

			// in the packed version for some reason...

			var path = options.path ? '; path=' + (options.path) : '';

			var domain = options.domain ? '; domain=' + (options.domain) : '';

			var secure = options.secure ? '; secure' : '';

			document.cookie = [ name, '=', encodeURIComponent(value), expires,
					path, domain, secure ].join('');

		} else { // only name given, get cookie

			var cookieValue = null;

			if (document.cookie && document.cookie != '') {

				var cookies = document.cookie.split(';');

				for (var i = 0; i < cookies.length; i++) {

					var cookie = jQuery.trim(cookies[i]);

					// Does this cookie string begin with the name we want?

					if (cookie.substring(0, name.length + 1) == (name + '=')) {

						cookieValue = decodeURIComponent(cookie
								.substring(name.length + 1));

						break;

					}

				}

			}

			return cookieValue;

		}

	};

	/* Here's the actual code. */

	if ($.cookie('remember_select') != null) {

		$('.select_class option[value="' + $.cookie('remember_select') + '"]')
				.attr('selected', 'selected');

	}

	$('.select_class').change(function() {

		$.cookie('remember_select', $('.select_class option:selected').val(), {
			expires : 90,
			path : '/'
		});

	});
</script>



<style>
fieldset {
	border: 0;
}

label {
	display: block;
	margin: 30px 0 0 0;
}

select {
	width: 200px;
}

.overflow {
	height: 200px;
}
</style>




</head>
<body>



	<h3>${msg}</h3>

<a href="tel:555-555-5555">555-555-5555</a>

	<table class="table table-hover">
		<thead>
			<tr>
				<th>Номер </th>
				<th>Имя</th>
				
			</tr>
		</thead>
		<c:forEach items="${priceList}" var="price">
			<tr>
				<td>${price.id}</td>
				<td>${price.name}</td>
				

			</tr>
		</c:forEach>
	</table>






	<h4>Имя пользователя: ${userLog.getUserName()}</h4>
	<h4>Email: ${userLog.getEmail()}</h4>
	<h4>Роль: ${userLog.getRole().getRoleName()}</h4>
	<h4>Пароль: ${userLog.getPassword()}</h4>

	<h4>Ваш ip: ${ipaddres}</h4>

	<h4>url1: ${urlinfo}</h4>

	<sec:authorize access="hasRole('ROLE_ADMIN')">

		<h3>
			<li><a href="<c:url value="/order_s" />">Поиск заказа</a></li>
	</sec:authorize>


	</br>


	<sec:authorize access="hasRole('ROLE_USER')">

		<h4>Мои заказы:</h4>


		<h5>мои заказы ${countList}</h5>
		<h5>новые заказы ${countOrderListNewOrderByUser}</h5>
		<h5>заказы в обработке ${countOrderListWorkOrderByUser}</h5>
		<h5>обработанные заказы ${countOrderListResultOrderByUser}</h5>


		<div id="accordion">
			<h3>Все мои заказы ${countList}</h3>
			<div>
				<table class="table table-hover">
					<thead>
						<tr>
							<th>Номер заказа</th>
							<th>Имя донора</th>
							<th>Заказчик</th>
							<th>Статус заказа</th>
							<th>Дата/Время заказа</th>
						</tr>
					</thead>
					<c:forEach items="${orderList}" var="order">
						<tr>
							<td>${order.id}</td>
							<td>${order.getDonor().getDonorName()}</td>
							<td>${order.getIuser().getUserName()}</td>
							<td>${order.getOrderStatus().getStatusName()}</td>
							<td><joda:format value="${order.getOrderRegDate()}"
									style="SM" /></td>

						</tr>
					</c:forEach>
				</table>
			</div>

			<h3>Новые заказы ${countOrderListNewOrderByUser}</h3>
			<div>
				<table class="table table-hover">
					<thead>
						<tr>
							<th>Номер заказа</th>
							<th>Имя донора</th>
							<th>Заказчик</th>
							<th>Статус заказа</th>
							<th>Дата/Время</th>
						</tr>
					</thead>
					<c:forEach items="${orderListNewOrderByUser}" var="order">
						<tr>
							<td>${order.id}</td>
							<td>${order.getDonor().getDonorName()}</td>
							<td>${order.getIuser().getUserName()}</td>
							<td>${order.getOrderStatus().getStatusName()}</td>
							<td><joda:format value="${order.getOrderRegDate()}"
									style="SM" /></td>

						</tr>
					</c:forEach>
				</table>
			</div>

			<h3>Заказы в обработке ${countOrderListWorkOrderByUser}</h3>
			<div>
				<table class="table table-hover">
					<thead>
						<tr>
							<th>Номер заказа</th>
							<th>Имя донора</th>
							<th>Заказчик</th>
							<th>Статус заказа</th>
							<th>Дата/Время</th>
						</tr>
					</thead>
					<c:forEach items="${orderListWorkOrderByUser}" var="order">
						<tr>
							<td>${order.id}</td>
							<td>${order.getDonor().getDonorName()}</td>
							<td>${order.getIuser().getUserName()}</td>
							<td>${order.getOrderStatus().getStatusName()}</td>
							<td><joda:format value="${order.getOrderRegDate()}"
									style="SM" /></td>

						</tr>
					</c:forEach>
				</table>
			</div>

			<h3>обработанные заказы ${countOrderListResultOrderByUser}</h3>
			<div>
				<table class="table table-hover">
					<thead>
						<tr>
							<th>Номер заказа</th>
							<th>Имя донора</th>
							<th>Заказчик</th>
							<th>Статус заказа</th>
							<th>Дата/Время</th>
						</tr>
					</thead>
					<c:forEach items="${orderListResultOrderByUser}" var="order">
						<tr>
							<td>${order.id}</td>
							<td>${order.getDonor().getDonorName()}</td>
							<td>${order.getIuser().getUserName()}</td>
							<td>${order.getOrderStatus().getStatusName()}</td>
							<td><joda:format value="${order.getOrderRegDate()}"
									style="SM" /></td>

						</tr>
					</c:forEach>
				</table>
			</div>




		</div>
	</sec:authorize>

	<sec:authorize access="hasRole('ROLE_ADMIN')">

		<h4>Все заказы:</h4>

		<table class="table table-hover">


			<h5>новые заказы ${countListNewOrders}</h5>
			<h5>заказы в обработке ${countOrderListWorkOrders}</h5>
			<h5>обработанные заказы ${countOrderListResultOrders}</h5>

		</table>

		<div id="accordion2">

			<h3>Заказы в обработке ${countOrderListWorkOrders}</h3>
			<div>
				<table class="table table-hover">
					<thead>
						<tr>
							<th>Номер заказа</th>
							<th>Имя донора</th>
							<th>Заказчик</th>
							<th>Статус заказа</th>
							<th>Дата/Время</th>
						</tr>
					</thead>
					<c:forEach items="${orderListWorkOrders}" var="order">
						<tr>
							<td>${order.id}</td>
							<td>${order.getDonor().getDonorName()}</td>
							<td>${order.getIuser().getUserName()}</td>
							<td>${order.getOrderStatus().getStatusName()}</td>
							<td><joda:format value="${order.getOrderRegDate()}"
									style="SM" /></td>

						</tr>
					</c:forEach>
				</table>
			</div>
			<h3>Обработанные заказы ${countOrderListResultOrders}</h3>
			<div>
				<table class="table table-hover">
					<thead>
						<tr>
							<th>Номер заказа</th>
							<th>Имя донора</th>
							<th>Заказчик</th>
							<th>Статус заказа</th>
							<th>Дата/Время</th>
						</tr>
					</thead>
					<c:forEach items="${orderListResultOrders}" var="order">
						<tr>
							<td>${order.id}</td>
							<td>${order.getDonor().getDonorName()}</td>
							<td>${order.getIuser().getUserName()}</td>
							<td>${order.getOrderStatus().getStatusName()}</td>
							<td><joda:format value="${order.getOrderRegDate()}"
									style="SM" /></td>

						</tr>
					</c:forEach>
				</table>
			</div>

			<h2>Новые заказы ${countListNewOrders}</h2>
			<div>
				<table class="table table-hover">
					<thead>
						<tr>
							<th>Номер заказа</th>
							<th>Имя донора</th>
							<th>Заказчик</th>
							<th>Статус заказа</th>
							<th>Дата/Время</th>
						</tr>
					</thead>
					<c:forEach items="${orderListNewOrders}" var="order">
						<tr>
							<td>${order.id}</td>
							<td>${order.getDonor().getDonorName()}</td>
							<td>${order.getIuser().getUserName()}</td>
							<td>${order.getOrderStatus().getStatusName()}</td>
							<td><joda:format value="${order.getOrderRegDate()}"
									style="SM" /></td>


						</tr>
					</c:forEach>
				</table>
			</div>

		</div>






	</sec:authorize>


</body>
</html>