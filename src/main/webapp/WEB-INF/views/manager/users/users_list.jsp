<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

	<div class="col-md-12" style="margin-top: 40pt">

		<div class="col-md-2" style="margin-top: 50pt">
			<div class="list-group"></div>
			<div class="list-group">
				<a class="list-group-item" href="list?status=EXPECT"><i
					class="glyphicon glyphicon-plus btn-lg"></i>заявки на регистрацию <strong>(${counts.expectValue})</strong></a>
				<a class="list-group-item" href="list?status=ACTIVE"><i
					class="glyphicon glyphicon-ok btn-lg"></i> активные пользователи <strong>(${counts.activeValue})</strong></a>
				<a class="list-group-item" href="list?status=ARCHIVED"><i
					class="glyphicon glyphicon-ok-circle btn-lg"></i>архивные
					пользователи <strong>(${counts.archivedValue})</strong></a>
			</div>
		</div>
		<div class="col-md-9" style="margin-top: 10pt">
			<div class="heading" style="margin-top: 0pt">
				<h2>Пользователи</h2>
			</div>
			<table class="table table-hover">
				<thead>
					<tr>
						<th>Ф.И.О.</th>
						<th>email</th>
						<th>доступ к разделу</th>
						<th>город</th>
						<th>телефон</th>
						<th>Дата/Время заявки</th>
						<th>В архив</th>
					</tr>
				</thead>
				<c:forEach items="${list}" var="list">
					<tr>
						<td><a href="<c:out value='view?id=${list.id}'/>"><i
								class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></a>
							${list.fio}</td>
						<td>${list.email}</td>
						<td>${list.access}</td>
						<td>${list.city}</td>
						<td>${list.phone}</td>
						<td>${list.createDate}</td>
						<td><a
							href="<c:out value='changeStatus?id=${list.id}&status=ARCHIVED'/>">архив<i
								class="fa fa-archive" aria-hidden="true"></i></a></td>
					</tr>
				</c:forEach>
			</table>
		</div>
	</div>
	<div class="col-md-12" style="margin-top: 40pt"></div>
	<div class="container"></div>
</body>
</html>