<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<div class="col-md-12" style="margin-top: 40pt">
	<div class="col-md-2" style="margin-top: 50pt">
		<div class="list-group">
			<a class="list-group-item" href="list?status=WAITS"><i
				class="glyphicon glyphicon-plus btn-lg"></i>Новые заявки <strong>(${counts.waitsValue})</strong>
			</a> <a class="list-group-item" href="list?status=CONSIDER"><i
				class="glyphicon glyphicon-ok btn-lg"></i> В рассмотрении <strong>(${counts.considerValue})</strong>
			</a> <a class="list-group-item" href="list?status=DOSSIER"><i
				class="glyphicon glyphicon-ok-circle btn-lg"></i> Согласованные
				заявки <strong>(${counts.dossierValue})</strong> </a> <a
				class="list-group-item" href="list?status=DISAGREE"><i
				class="glyphicon glyphicon-ban-circle btn-lg"></i> Отмененные заявки<strong>(${counts.disagreeValue})</strong>
			</a>
		</div>
	</div>
	<div class="col-md-9" style="margin-top: 0pt; margin-left: 20pt">
		<div class="heading">
			<h3>Рассмотрение заявок</h3>
		</div>

		<table class="table table-hover">
			<thead>
				<tr>
					<th>№ заявки</th>
					<th>ЭМК донора</th>
					<th>код донора</th>
					<th>донор тип</th>
					<th>ЭМК пациента</th>
					<th>ф.и.о. пациента</th>
					<th>дата заявки</th>
					<th>статус заявки</th>
				</tr>
			</thead>
			<c:forEach items="${list}" var="list">
				<tr>
					<td><a href="<c:out value='order/${list.id}'/>"><i
							class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></a>
						${list.id}</td>
					<td>${list.donorId}</td>
					<td>${list.donorCode}</td>
					<td>${list.donorType}</td>
					<td>${list.iuserId}</td>
					<td>${list.iuserName}</td>
					<td>${list.orderDate}</td>
					<td>${list.orderStatus}</td>
				</tr>
			</c:forEach>
		</table>
	</div>
</div>
<div class="col-md-12" style="margin-top: 40pt"></div>
<div class="container"></div>