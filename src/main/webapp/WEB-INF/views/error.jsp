<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" pageEncoding="utf-8"
	contentType="text/html; charset=UTF-8"%>

<head>
<%-- <tiles:insertAttribute name="title" ignore="true" /> --%>
<title>cryo</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<link
	href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,500,700,800'
	rel='stylesheet' type='text/css'>

<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

<link href="<c:url value="/resources/css/animate.css"/>"
	rel="stylesheet">


<link href="<c:url value="/resources/css/custom.css"/>" rel="stylesheet">

<link href="<c:url value="/resources/css/style.default.css"/>"
	rel="stylesheet">

<link href="<c:url value="/resources/css/login.form.css"/>"
	rel="stylesheet">


<style> 
.center {text-align: center; margin-left: auto; margin-right: auto; margin-bottom: auto; margin-top: auto;}
</style>
</head>	
	
	
<div class="container">
  <div class="row">
    <div class="span12">
      <div class="hero-unit center">
          <h1>Ошибка !</h1>

<h2>Произошла внутренняя ошибка. Попробуйте повторить попытку позже. <h2>
           <h1> <small><font face="Tahoma" color="red">${message}</font></small></h1>
          
     
          <a href="http://cryobank.kz/" class="btn btn-large btn-info"><i class="icon-home icon-white"></i> на главную</a>
        </div>
  </div>
</div>
    