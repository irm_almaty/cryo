<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<div class="heading">
		<h3>Лаборатория</h3>
	</div>
	<div class="col-md-12" style="margin-top: 0pt">
		<div class="col-md-6" style="margin-top: 0pt">
			<h3>Пациент:</h3>
			<table class="table table-stripped table-bordered">
				<thead>
					<tr>
						<th>#</th>
						<th>Информация:</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<th>Номер эмк:</th>
						<td>${patient.id}</td>
					</tr>
					<tr>
						<th>Номер ИИН:</th>
						<td>${patient.iin}</td>
					</tr>

					<tr>
						<th>УДВ:</th>
						<td>${patient.udv}</td>
					</tr>

					<tr>
						<th>Email::</th>
						<td>${patient.email}</td>
					</tr>
					<tr>
						<th>Номер тел.:</th>
						<td>${patient.phone}</td>
					</tr>
					<tr>
						<th>Фамилия:</th>
						<td>${patient.lastname}</td>
					</tr>
					<tr>
						<th>Имя:</th>
						<td>${patient.firstname}</td>
					</tr>
					<tr>
						<th>Отчество:</th>
						<td>${patient.patroname}</td>
					</tr>
					<tr>
						<th>Дата рождения:</th>
						<td>${patient.birthday}</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="col-md-6" style="margin-top: 0pt">
			<h3>Донор:</h3>
			<table class="table table-stripped table-bordered">
				<thead>
					<tr>
						<th>#</th>
						<th>Информация:</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<th>Номер эмк:</th>
						<td>${order.donorId}</td>
					</tr>
					<tr>
						<th>Код донора:</th>
						<td><a
							href="<c:out value='/donor/${order.donorType.toString().toLowerCase()}/view?id=${order.donorId}'/>"><i
								class="glyphicon glyphicon-eye-open" aria-hidden="true"></i></a>
							${order.donorCode}</td>
					</tr>
				</tbody>
			</table>



		</div>
		<div class="col-md-12" style="margin-top: 20pt">
			<c:forEach items="${actions}" var="actions">

				<a
					href="<c:out value='/orders/order/action?order=${order.id}&status=${actions.actionCode}'/>">
					<div class="btn btn-small btn-template-main"
						style="margin-top: 10pt;">${actions.actionName}</div>
				</a>
			</c:forEach>
		</div>
	</div>

</body>
</html>