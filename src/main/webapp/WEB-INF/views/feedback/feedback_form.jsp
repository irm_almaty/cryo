<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" pageEncoding="utf-8"
	contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<style>
span.req {
	color: maroon;
	font-size: 112%;
}
</style>
<script src="//cdn.ckeditor.com/4.5.7/basic/ckeditor.js"></script>
<section>
	<div class="row">
		<div class="col-md-4">
			<div class="box-simple">
				<div class="icon">
					<i class="fa fa-map-marker"></i>
				</div>
				<h3>Наш адрес</h3>
				<p>
					99 Толе би <br>Алматы <br> <strong>Казахстан</strong>
				</p>
			</div>
		</div>
		<div class="col-md-4">
			<div class="box-simple">
				<div class="icon">
					<i class="fa fa-phone"></i>
				</div>
				<h3>Call center</h3>
				<p class="text-muted">Многопоточная линия</p>
				<p>
					<strong>+7 (727) 234-34-34</strong>
				</p>
			</div>
		</div>
		<div class="col-md-4">
			<div class="box-simple">
				<div class="icon">
					<i class="fa fa-envelope"></i>
				</div>
				<h3>Email поддержка</h3>
				<p class="text-muted">Пишите нам.</p>
				<ul class="list-style-none">
					<li><strong><a href="mailto:">info@irm.kz</a></strong></li>
				</ul>
			</div>
		</div>
	</div>
</section>
<form:form method="post" modelAttribute="feedback" action="send">
	<section>
		<div class="col-md-12 col-md-offset-1">
			<div class="heading">
				<h2>Обратная связь</h2>
			</div>
		</div>
		<div class="col-md-8 col-md-offset-1">
			<div class="row">
				<c:if test="${result.hasErrors()!=null&&!result.hasErrors()}">
					<div class="alert alert-success" role="alert">${successMessage}</div>
				</c:if>
				<c:if test="${result.hasErrors()}">
					<div class="alert alert-danger alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert">
							<span aria-hidden="true">×</span><span class="sr-only">Close</span>
						</button>
						Проверьте правильность заполнения данных
					</div>
				</c:if>

				<div class="col-md-10">
					<div class="form-group">
						<label for="subject">Ваш email:</label>
						<form:input path="authorEmail" class="form-control" type="text" />
						<div id="errFirst">
							<span class="req"> <form:errors path="authorEmail"
									cssClass="error" /></span>
						</div>
					</div>
				</div>
				<div class="col-md-10">
					<div class="form-group">
						<label for="subject">Ваше имя:</label>
						<form:input path="authorName" class="form-control" type="text" />
					</div>
				</div>
				<div class="col-md-10">
					<div class="form-group">
						<label for="subject">Контактный телефон:</label>
						<form:input path="authorPhone" class="form-control" type="text" />
					</div>
				</div>
				<div class="col-sm-12">
					<div class="form-group">
						<label for="messageBody">Сообщение</label>
						<form:textarea id="editor1" path="messageBody" />
						<script type="text/javascript">
							CKEDITOR.replace('editor1');
						</script>
					</div>
				</div>
				<div class="col-sm-12 ">
					<button type="submit" value="Send E-mail"
						class="btn btn-template-main">
						<i class="fa fa-envelope-o"></i> Отправить
					</button>
				</div>
			</div>
		</div>
	</section>
</form:form>