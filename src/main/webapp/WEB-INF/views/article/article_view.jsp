<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${article.title}</title>

<meta name="description" content="${article.metaDescription}">

<meta name="keywords" content="${article.metaKeywords}">

</head>
<body>

	<div class="col-xs-9">
		<h5>Просмотр</h5>

		<h3>${article.title}</h3>

		<p>
		<h5>Дата:${article.createDate}</h5>
		</p>

		<p>
		<h5>Категория: ${article.getCategory().getName()}</h5>
		</p>

		<p>
		<h5>Превью: ${article.preview}</h5>
		</p>

		<p>${article.content}</p>

	</div>
	<div class="col-xs-3">

		<h4>Последние статьи</h4>
		<c:forEach items="${listArticle}" var="article">

			<div class="thumbnail">
				<h6>Название: ${article.title}</h6>

				<p>Дата: ${article.createDate}</p>

				<p ng-controller="articleController">
					<a href="<c:out value='viewArticle?id=${article.id}'/>"> <span
						class="glyphicon glyphicon-eye-open"> </span> Просмотр
					</a>
				</p>
			</div>
		</c:forEach>

		<div class="news" style="margin-bottom: 30pt; margin-top: 55pt";>
			<h4>Новостная колонка</h4>

			<c:forEach items="${listNews}" var="news">
				<div class="thumbnail">
					<h6>название: ${news.title}</h6>
					<p>Дата: ${news.createDate}</p>
					<p ng-controller="newsController">
					<a href="<c:out value='${pageContext.request.contextPath}/news/viewNews?id=${news.newsId}'/>"> <span
						class="glyphicon glyphicon-eye-open"> </span> Просмотр
					</a>
				</p>
				</div>
			</c:forEach>

		</div>
</body>
</html>