<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>


	<h3>${msg}</h3>

	<sec:authorize access="hasRole('ROLE_ADMIN')">
		<h4><a href="<c:url value="article/createArticle" />">
			
				Добавить статью <span class="glyphicon glyphicon-duplicate"></span>
			
		</a></h4>
	</sec:authorize>
	
	
	
	
		<c:forEach items="${listDonorManMedialog}" var="suleiman">
		<div class="col-sm-6 col-md-10" style="padding-bottom: 5px">
			<div class="thumbnail">
				<div class="caption">

					<h4>Название: ${suleiman.donorManId}</h4>
                  
                
                     
					<p>Дата создания: ${suleiman.cvetVolos}</p>

					
					
				</div>
			</div>
		</div>
	</c:forEach>
	
	
	
	<c:forEach items="${listArticle}" var="article">
		<div class="col-sm-6 col-md-10" style="padding-bottom: 5px">
			<div class="thumbnail">
				<div class="caption">

					<h4>Название: ${article.title}</h4>
                  
                  <p ng-controller="articleController">
                  <a href="<c:out value='article/viewArticle?id=${article.id}'/>">
								<span class="glyphicon glyphicon-eye-open"> </span> Просмотр

							</a>
                     </p>
					<p>Дата создания: ${article.createDate}</p>

					<p>Категория: ${article.getCategory().getName()}</p>

					<p>Превью: ${article.preview}</p>

					<sec:authorize access="hasRole('ROLE_ADMIN')">

						<p ng-controller="articleController">
							<a href="<c:out value='article/editArticle?id=${article.id}'/>">
								<span class="glyphicon glyphicon-edit"> </span> Изменить
							</a> <a
								href="<c:out value='article/deleteArticle?id=${article.id}'/>">
								<span class="glyphicon glyphicon-remove"> </span> Удалить
							</a>

						</p>
					</sec:authorize>
				</div>
			</div>
		</div>
	</c:forEach>

</body>








	
	
		

	
	







</html>