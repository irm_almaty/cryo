<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://ckeditor.com" prefix="ckeditor"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>



<link rel="stylesheet"
	href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script>


	$(function() {
	
		
		$( "#datepicker" ).datepicker({changeMonth: true, changeYear: true, yearRange: "2015:2019"} );
		
	});
	(function(factory) {
		if (typeof define === "function" && define.amd) {

			// AMD. Register as an anonymous module.
			define([ "../widgets/datepicker" ], factory);
		} else {

			// Browser globals
			factory(jQuery.datepicker);
		}
	}
			(function(datepicker) {

				datepicker.regional.ru = {
					closeText : "Закрыть",
					prevText : "&#x3C;Пред",
					nextText : "След&#x3E;",
					currentText : "Сегодня",
					monthNames : [ "Январь", "Февраль", "Март", "Апрель",
							"Май", "Июнь", "Июль", "Август", "Сентябрь",
							"Октябрь", "Ноябрь", "Декабрь" ],
					monthNamesShort : [ "Янв", "Фев", "Мар", "Апр", "Май",
							"Июн", "Июл", "Авг", "Сен", "Окт", "Ноя", "Дек" ],
					dayNames : [ "воскресенье", "понедельник", "вторник",
							"среда", "четверг", "пятница", "суббота" ],
					dayNamesShort : [ "вск", "пнд", "втр", "срд", "чтв", "птн",
							"сбт" ],
					dayNamesMin : [ "Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб" ],
					weekHeader : "Нед",
					dateFormat : "dd/mm/yy",
					firstDay : 1,
					isRTL : false,
					showMonthAfterYear : false,
					yearSuffix : ""
				};
				datepicker.setDefaults(datepicker.regional.ru);

				return datepicker.regional.ru;

			}));
</script>
</head>
<body>

	
		<h3>${msg}</h3>

	<div class="panel-body">
		<form:form id="manRegisterForm" cssClass="form-horizontal"
			modelAttribute="article" method="post" action="saveArticle">

			<div class="form-group">
				<div class="control-label col-xs-1">
					<form:label path="title">id</form:label>
				</div>
				<div class="col-md-11">
					<form:input cssClass="form-control" path="id" value="${article.id}" />
				</div>
			</div>

			<div class="form-group">
				<div class="control-label col-xs-1">
					<form:label path="title">title</form:label>
				</div>
				<div class="col-md-11">
					<form:input cssClass="form-control" path="title"
						value="${article.title}" />
				</div>
			</div>

			<div class="form-group">
				<div class="control-label col-xs-1">
					<form:label path="title">preview</form:label>
				</div>
				<div class="col-md-11">
					<form:input cssClass="form-control" path="preview"
						value="${article.preview}" />
				</div>
			</div>
			
			<div class="form-group">
				<div class="control-label col-xs-1">
					<form:label path="metaDescription">metaDescription</form:label>
				</div>
				<div class="col-md-11">
					<form:input cssClass="form-control" path="metaDescription"
						value="${article.metaDescription}" />
				</div>
			</div>
			
			<div class="form-group">
				<div class="control-label col-xs-1">
					<form:label path="metaKeywords">metaKeywords</form:label>
				</div>
				<div class="col-md-11">
					<form:input cssClass="form-control" path="metaKeywords"
						value="${article.metaKeywords}" />
				</div>
			</div>
			
			

			<div class="form-group">
				<div class="control-label col-xs-1">
					<form:label path="category">категория:</form:label>
				</div>
				<div class="col-md-11">
					<form:select path="category.id" items="${listCategory}"
						itemValue="id" itemLabel="name" class="form-control">
					</form:select>
				</div>
			</div>

			<div class="form-group">
				<div class="control-label col-xs-1">
					<form:label path="content">content</form:label>
				</div>
				<div class="col-md-11">

					<form:textarea id="editor1" path="content" class="form-control"
						rows="30" cols="250" />
				</div>
			</div>

			<div class="form-group">
				<div class="control-label col-xs-1">
					<form:label path="createDate">date</form:label>
				</div>
				<div class="col-xs-11">
					<form:input path="createDate" id="datepicker" />

				</div>
			</div>

			<div class="form-group">
				<div class="row">
					<div class="col-xs-2"></div>
					<div class="col-xs-2">
						<input type="submit" class="btn btn-primary" value="Save" />
					</div>

				</div>
			</div>


		</form:form>
		<ckeditor:replace replace="editor1"
			basePath="/resources/ckeditor/" />
	</div>

</body>
</html>