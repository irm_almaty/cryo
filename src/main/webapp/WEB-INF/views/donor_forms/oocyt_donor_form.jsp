<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<script src="//cdn.ckeditor.com/4.5.7/basic/ckeditor.js"></script>	
<div class="heading">
	<h1 class="info">Как стать донором ооцитов</h1>
</div>

<p>
	<span style="font-size: 18px">Донорство &ndash; это одно из
		самых благородных и гуманных решений в жизни человека. Став участником
		программы донорства яйцеклеток в Казахстане, вы сможете подарить счастье
		материнства женщине, живущей в постоянной надежде, но не имеющей
		собственных возможностей для зачатия ребенка.</span>
</p>

<p>
	<span style="font-size: 18px"><strong><u>Что такое
				донорство яйцеклеток?</u></strong></span>
</p>

<p>
	<span style="font-size: 18px">Каждая женщина производит в месяц
		одну зрелую яйцеклетку, а с момента рождения в яичниках будущей матери
		вырабатывается около 400 000 ооцитов. Каждый месяц несколько ооцитов
		одновременно начинают процесс развития, который заканчивается в день
		овуляции. Только один из них становится в этот момент созревшей
		яйцеклеткой, в то время как остальные атрофируются. Так женский
		организм подготавливается к беременности, или, если зачатия не
		происходит, то яйцеклетка удаляется с менструальным кровотечением.</span>
</p>

<p>
	<span style="font-size: 18px">Донорство яйцеклеток &nbsp;- это
		передача нескольких яйцеклеток от одной здоровой женщины другой.</span>
</p>

<p>
	<span style="font-size: 18px">Важно понимать, что доноры
		яйцеклеток не вправе брать на себя родительские обязанности на
		будущего ребенка.</span>
</p>

<p>
	<span style="font-size: 18px"><strong><u>Кто
				нуждается в донорских яйцеклетках?</u></strong></span>
</p>

<p>
	<span style="font-size: 18px">Донорские яйцеклетки являются
		единственной возможностью для женщин, которые не могут самостоятельно
		забеременеть. &nbsp;Причины, по которым женщина вынуждена прибегать к
		донорским яйцеклеткам, могут быть разными, но их можно обобщить в две
		большие группы: или у женщины практически нет своих яйцеклеток, или
		эти яйцеклетки не пригодны для зачатия.</span>
</p>

<p>
	<span style="font-size: 18px"><strong><u>Донорами
				ооцитов могут быть:</u></strong></span>
</p>

<p>
	<span style="font-size: 18px">&middot;анонимные/неанонимные
		добровольные доноры;</span>
</p>

<p>
	<span style="font-size: 18px">&middot;знакомые, родственницы
		реципиентов;</span>
</p>

<p>
	<span style="font-size: 18px">&middot;пациентки программ ВРТ,
		которые по письменно оформленному добровольному согласию предоставляют
		реципиенту часть своих ооцитов.</span>
</p>

<p>
	<span style="font-size: 18px"><strong><u>Требования
				к донорам ооцитов:</u></strong></span>
</p>

<p>
	<span style="font-size: 18px">&middot;Стать донором яйцеклетки может женщина в возрасте от 18
		до 35 лет;</span>
</p>

<p>
	<span style="font-size: 18px">&middot;наличие здорового ребенка;</span>
</p>

<p>
	<span style="font-size: 18px">&middot;отсутствие выраженных
		фенотипических проявлений;</span>
</p>

<p>
	<span style="font-size: 18px">&middot;удовлетворительное
		соматическое здоровье;</span>
</p>

<p>
	<span style="font-size: 18px">&middot;отсутствие
		противопоказаний для участия в программе донации&nbsp;ооцитов;</span>
</p>

<p>
	<span style="font-size: 18px">&middot;отсутствие наследственных
		заболеваний;</span>
</p>

<p>
	<span style="font-size: 18px">&middot;отсутствие вредных
		привычек: наркомания, алкоголизм,&nbsp;токсикомания.</span>
</p>

<p>
	<span style="font-size: 18px"><strong><u>Алгоритм
				осуществления программы донации ооцитов:</u></strong></span>
</p>

<p>
	<span style="font-size: 18px">&middot;заполнение анкеты у
		менеджера по донорам или на сайте <a href="<c:url value="/"/>">Криобанка</a>  ИРМ;</span>
</p>

<p>
	<span style="font-size: 18px">&middot;консультация;</span>
</p>

<p>
	<span style="font-size: 18px">&middot;медицинский осмотр и
		обследование донора (оплачивается ИРМ);</span>
</p>

<p>
	<span style="font-size: 18px">&middot;синхронизация
		менструальных циклов, наблюдение у врача;</span>
</p>

<p>
	<span style="font-size: 18px">&middot;стимуляция яичников и
		пункция фолликулов (оплачивается ИРМ).</span>
</p>

<p>
	<span style="font-size: 18px">Восстановление организма
		происходит в течение 3 месяцев. После чего, при желании, женщина снова
		может участвовать в донорской программе.</span>
</p>

<p>
	<span style="font-size: 18px"><strong><u>Компенсация:</u></strong></span>
</p>

<p>
	<span style="font-size: 18px">Стоимость вознаграждения
		выплачивается ИРМ и составляет от 60&nbsp;000 тг до 180&nbsp;000 тг.</span>
</p>

<p>
	<span style="font-size: 18px">Медицинское обследование и все
		препараты, участвующие в стимуляции, проводятся за счет ИРМ.</span>
</p>

<p>
	<span style="font-size: 18px"><strong><u>Как можно
				стать донором яйцеклеток?</u></strong></span>
</p>

<p>
	<span style="font-size: 18px">1.Обратиться к менеджерам по
		донорству и заполнить анкету. При себе нужно иметь удостоверение
		личности, свидетельство о рождении ребенка, свое личное детское фото и
		фото своего ребенка.</span>
</p>

<p>
	<span style="font-size: 18px">2.&nbsp;Кроме того, вы можете
		заполнить анкету на сайте ИРМ. С вами свяжутся менеджеры по донорству
		и проконсультируют по всем необходимым вопросам.</span>
</p>

<p>
	<span style="font-size: 18px"><strong>Подарите счастье
			тем, кто его очень долго ждет!</strong></span>
</p>
<p>
	<br /> <span style="font-size: 19px"><strong><u>Заполнить
				анкету:</u></strong>
</p>
<form:form method="post" modelAttribute="feedback"
	action="submit?type=OOCYTE">
	<div class="row">
		<div class="col-md-9">
			<c:if test="${result.hasErrors()!=null&&!result.hasErrors()}">
				<div class="alert alert-success" role="alert">${successMessage}</div>
			</c:if>
			<c:if test="${result.hasErrors()}">
				<div class="alert alert-danger alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert">
						<span aria-hidden="true">×</span><span class="sr-only">Close</span>
					</button>
					Проверьте правильность заполнения данных
				</div>
			</c:if>
			<div class="col-md-8">
				<div class="form-group">
					<span style="font-size: 18px">Ваш email:</span>
					<form:input path="authorEmail" class="form-control" type="text" />
					<div id="errFirst">
						<span class="req"> <form:errors path="authorEmail"
								cssClass="error" /></span>
					</div>
				</div>
			</div>
			<div class="col-md-8">
				<div class="form-group">
					<span style="font-size: 18px">Ваше имя:</span>
					<form:input path="authorName" class="form-control" type="text" />
				</div>
			</div>
			<div class="col-md-8">
				<div class="form-group">
					<span style="font-size: 18px">Контактный телефон:</span>
					<form:input path="authorPhone" class="form-control" type="text" />
				</div>
			</div>
		</div>
		<div class="col-md-12">
			<div class="col-md-10">
				<div class="form-group">
					<span style="font-size: 18px">Сообщение:</span>
					<form:textarea id="editor1" path="messageBody" />
					<script type="text/javascript">
						CKEDITOR.replace('editor1');
					</script>
				</div>
			</div>
			<div class="col-md-12">
				<button type="submit" value="Send E-mail"
					class="btn btn-template-main">
					<i class="fa fa-envelope-o"></i> Отправить
				</button>
			</div>
		</div>
	</div>
</form:form>