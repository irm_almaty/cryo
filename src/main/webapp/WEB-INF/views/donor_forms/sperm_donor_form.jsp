<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<script src="//cdn.ckeditor.com/4.5.7/basic/ckeditor.js"></script>
<div class="heading">
	<h1 class="info">Как стать донором спермы</h1>
</div>
<p>
	<span style="font-size: 18px">В Криобанке донором может стать любой мужчина
		психически и физически здоровый, любой национальности и
		вероисповедания.</span>
</p>

<p>
	<span style="font-size: 18px"><u><strong>Требования,
				предъявляемые к донорам спермы:</strong></u><br /> - возраст &ndash; 18-35
		лет,&nbsp;<br /> - отсутствие отклонений в нормальных
		органометрических и фенотипических признаках.</span>
</p>
<p>
	<span style="font-size: 18px"><u><strong>Сперма
				должна соответствовать следующим требованиям:</strong></u><br /> 1) объем
		эякулята более 2 мл;<br /> 2) концентрация сперматозоидов в 1 мл
		эякулята более 60 миллионов;<br /> 3) доля прогрессивно-подвижных
		форм (А+В) более 50 %;<br /> 4) доля морфологически-нормальных форм
		более 30 % (по строгим критериям Крюгера 14 % и более);<br /> 5)
		криотолерантность;<br /> 6) тест, определяющий иммунокомпетентные
		тела поверхности сперматозоида (МАР-тест) &ndash; по&nbsp;показаниям.</span>
</p>
<p>
	<span style="font-size: 18px"><u><strong>Как
				сдается спермограмма:</strong></u><br /> Для получения достоверного анализа
		спермы следует выполнить следующие требования:&nbsp;<br /> - 3 дня
		половое воздержание перед сдачей спермы на исследование<br /> - не
		рекомендуется посещение бань и саун, т.к. повышенная температура может
		неблагоприятно&nbsp;влиять на качество спермы<br /> - перед сдачей
		анализа исключить алкоголь, острую и жирную пищу.&nbsp;<br /> -
		анализы принимают по записи в каб.№203.</span>
</p>
<p>
	<span style="font-size: 18px"><u><strong>Этапы
				прохождения отбора:</strong></u><br /> I этап: Исследование спермы бесплатно
		(Спермограмма). При хороших показателях проводится дополнительное
		исследование на морфологию эякулята+МАР-тест.<br /> II этап: Осмотр и
		заключение андролога проводится бесплатно для доноров, прошедших
		первый этап отбора с хорошими показателями спермы;<br /> III этап:
		Медицинское обследование.</span>
</p>
<p>
	<span style="font-size: 18px"><strong><u>Материальное
				вознаграждение:</u></strong><br /> Расходы комплексного медицинского
		обследования компенсируются только при нормальных показателях при
		заключении договора на донорство спермы!<br /> Стоимость компенсации
		за одну порции спермы&nbsp;<strong>15.000 тенге</strong>.</span>
</p>
<p>
	<span style="font-size: 18px"><strong><u>Как можно
				стать донором спермы?</u></strong><br /> Подойти к менеджерам по донорству и
		заполнить анкету. При себе иметь удостоверение личности или заполнить
		заявку на нашем сайте и менеджеры свяжутся с вами.</span>
</p>
<p>
	<br /> <span style="font-size: 19px"><strong><u>Заполнить
				анкету:</u></strong>
</p>
<form:form method="post" modelAttribute="feedback" action="submit?type=SPERM">
	<div class="row">
		<div class="col-md-9">
			<c:if test="${result.hasErrors()!=null&&!result.hasErrors()}">
				<div class="alert alert-success" role="alert">${successMessage}</div>
			</c:if>
			<c:if test="${result.hasErrors()}">
				<div class="alert alert-danger alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert">
						<span aria-hidden="true">×</span><span class="sr-only">Close</span>
					</button>
					Проверьте правильность заполнения данных
				</div>
			</c:if>
			<div class="col-md-8">
				<div class="form-group">
					<span style="font-size: 18px">Ваш email:</span>
					<form:input path="authorEmail" class="form-control" type="text" />
					<div id="errFirst">
						<span class="req"> <form:errors path="authorEmail"
								cssClass="error" /></span>
					</div>
				</div>
			</div>
			<div class="col-md-8">
				<div class="form-group">
					<span style="font-size: 18px">Ваше имя:</span>
					<form:input path="authorName" class="form-control" type="text" />
				</div>
			</div>
			<div class="col-md-8">
				<div class="form-group">
					<span style="font-size: 18px">Контактный телефон:</span>
					<form:input path="authorPhone" class="form-control" type="text" />
				</div>
			</div>
		</div>
		<div class="col-md-12">
			<div class="col-md-10">
				<div class="form-group">
					<span style="font-size: 18px">Сообщение:</span>
					<form:textarea id="editor1" path="messageBody" />
					<script type="text/javascript">
						CKEDITOR.replace('editor1');
					</script>
				</div>
			</div>
			<div class="col-md-12">
				<button type="submit" value="Send E-mail"
					class="btn btn-template-main">
					<i class="fa fa-envelope-o"></i> Отправить
				</button>
			</div>
		</div>
	</div>
</form:form>