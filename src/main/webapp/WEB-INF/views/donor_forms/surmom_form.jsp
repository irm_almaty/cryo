<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<script src="//cdn.ckeditor.com/4.5.7/basic/ckeditor.js"></script>
<div class="heading">
	<h1 class="info">Как стать суррогатной матерью</h1>
</div>
<p>
	<span style="font-size: 18px"><strong><u>Что такое
				суррогатное материнство?</u></strong></span>
</p>

<p>
	<span style="font-size: 18px"><strong>Суррогатное
			материнство в Казахстане</strong>&nbsp;&mdash;&nbsp;вспомогательная репродуктивная
		технология, при применении которой в&nbsp;зачатии&nbsp;и рождении
		ребёнка участвуют три человека: генетический отец&nbsp;&mdash; лицо,
		предоставившее свою сперму для&nbsp;оплодотворения&nbsp;и согласное
		после рождения ребёнка взять на себя обязанности отца; генетическая
		мать&nbsp;&mdash; лицо, предоставившее свою&nbsp;яйцеклетку&nbsp;для
		оплодотворения и согласное после рождения ребёнка взять на себя
		обязанности матери; суррогатная мать&nbsp;&mdash; женщина детородного
		возраста, согласившаяся на возмездной или безвозмездной основе
		выносить и родить ребёнка от генетических родителей и не претендующая
		на роль матери данного ребёнка. После рождения ребёнка генетические
		родители оформляются в качестве юридических родителей.</span>
</p>

<p>
	<span style="font-size: 18px"><strong><u>Кто
				нуждается в суррогатных мамах?</u></strong></span>
</p>

<p>
	<span style="font-size: 18px">В большинстве случаев суррогатное
		материнство применяется для преодоления бесплодия в супружеских парах,
		в которых женщина не способна выносить ребёнка по медицинским
		показаниям.</span>
</p>

<p>
	<span style="font-size: 18px">Суррогатное материнство в Алматы возможно
		только при применении&nbsp;экстракорпорального&nbsp; оплодотворения:
		яйцеклетку, оплодотворенную &laquo;в пробирке&raquo;, переносят в
		матку суррогатной матери в течение первых 3-5 дней развития эмбриона.</span>
</p>

<p>
	<span style="font-size: 18px"><strong><u>Требования
				к суррогатным мамам:</u></strong><br /> &bull;Стать суррогатной мамой может женщина в возрасте от 20 до 35
		лет;<br /> &bull; Наличие рожденного здорового ребенка;<br /> &bull;
		Удовлетворительное физическое, психическое и репродуктивное здоровье,
		подтвержденное заключением медицинской организации;<br /> &bull;
		Письменное согласие супруга, удостоверенное в нотариальном порядке;</span>
</p>

<p>
	<span style="font-size: 18px"><u><strong>Алгоритм
				осуществления программы суррогатного материнства:</strong></u><br /> &bull;
		Заполнение анкеты у менеджера.<br /> &bull; Выбор суррогатной мамы
		семейной парой.&nbsp;<br /> &bull; Медицинский осмотр и обследование
		суррогатной мамы.<br /> &bull; Заключение в нотариусе договора с
		суррогатной мамой по письменно оформленной, добровольному согласию о
		вынашивании ребенка.<br /> &bull; Синхронизация менструальных циклов;<br />
		&bull; Перенос эмбриона в полость матки суррогатной мамы;</span>
</p>

<p>
	<span style="font-size: 18px"><strong><u>Компенсация:</u></strong><br />
		Материальная&nbsp;компенсация&nbsp;суррогатным мамам выплачивается по
		договоренности с семейной парой (заказчиками).<br /> Медицинское
		обследование проводится за счет семейной пары (заказчиков).</span>
</p>

<p>
	<span style="font-size: 18px"><strong><u>Как можно
				стать суррогатной мамой?</u></strong><br /> Подойти к менеджерам <a href="<c:url value="/"/>">Криобанка Алматы</a> по донорству и
		заполнить анкету. При себе иметь удостоверение личности, свидетельство
		о рождении ребенка, личное фото или заполнить заявку на нашем сайте и
		менеджеры свяжутся с вами.</span>
</p>
<p>
	<br /> <span style="font-size: 19px"><strong><u>Заполнить
				анкету:</u></strong>
</p>
<form:form method="post" modelAttribute="feedback"
	action="submit?type=SURMOM">
	<div class="row">
		<div class="col-md-9">
			<c:if test="${result.hasErrors()!=null&&!result.hasErrors()}">
				<div class="alert alert-success" role="alert">${successMessage}</div>
			</c:if>
			<c:if test="${result.hasErrors()}">
				<div class="alert alert-danger alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert">
						<span aria-hidden="true">×</span><span class="sr-only">Close</span>
					</button>
					Проверьте правильность заполнения данных
				</div>
			</c:if>
			<div class="col-md-8">
				<div class="form-group">
					<span style="font-size: 18px">Ваш email:</span>
					<form:input path="authorEmail" class="form-control" type="text" />
					<div id="errFirst">
						<span class="req"> <form:errors path="authorEmail"
								cssClass="error" /></span>
					</div>
				</div>
			</div>
			<div class="col-md-8">
				<div class="form-group">
					<span style="font-size: 18px">Ваше имя:</span>
					<form:input path="authorName" class="form-control" type="text" />
				</div>
			</div>
			<div class="col-md-8">
				<div class="form-group">
					<span style="font-size: 18px">Контактный телефон:</span>
					<form:input path="authorPhone" class="form-control" type="text" />
				</div>
			</div>
		</div>
		<div class="col-md-12">
			<div class="col-md-10">
				<div class="form-group">
					<span style="font-size: 18px">Сообщение:</span>
					<form:textarea id="editor1" path="messageBody" />
					<script type="text/javascript">
						CKEDITOR.replace('editor1');
					</script>
				</div>
			</div>
			<div class="col-md-12">
				<button type="submit" value="Send E-mail"
					class="btn btn-template-main">
					<i class="fa fa-envelope-o"></i> Отправить
				</button>
			</div>
		</div>
	</div>
</form:form>