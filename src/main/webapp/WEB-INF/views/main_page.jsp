<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>

<script src="<c:url value="/resources/js/YTPlayer.js"/>"></script>
<section class="content-section video-section" style="margin-top: 10pt">
	<div class="pattern-overlay">
		<a id="bgndVideo" class="player"
			data-property="{videoURL:'https://www.youtube.com/watch?v=8QykxvGZJm0',containment:'.video-section', quality:'large', autoPlay:true, mute:true, opacity:1}">bg</a>
		<div class="container" style="margin-bottom: 50pt; margin-top: -30pt">
			<div class="carousel fade-carousel slide" data-ride="carousel"
				data-interval="3000" id="bs-carousel">
				<!-- Overlay -->
				<div class="overlay"></div>
				<div class="carousel-inner">
					<div class="item slides active">
						<div class="slide-1"></div>
						<div class="hero">
							<h1>Институт Репродуктивной Медицины Криобанк</h1>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="container">
	<div class="row"
		style="margin-bottom: 10pt; text-align: center; margin-top: 20pt">
		<div class="col-md-4 col-sm-6">
			<div class="box-image-text blog">
				<div class="heading">
					<h3>Доноры спермы</h3>
				</div>
				<div class="top">
					<div class="imagecenter">
						<img src="<c:url value="/resources/img/donor_sperm.png"/>"
							width="240" alt="доноры спермы">
					</div>
					<div class="bg"></div>
					<div class="text">
						<p class="buttons">
							<a href="<c:out value='/donor/sperm/list'/>"
								class="btn btn-template-transparent-primary"><i
								class="fa fa-link"></i> Подробнее</a>
						</p>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6">
			<div class="box-image-text blog">
				<div class="heading">
					<h3>Доноры ооцитов</h3>
				</div>
				<div class="top">
					<div class="imagecenter">
						<img src="<c:url value="/resources/img/donor_oocyt.png"/>"
							width="240" alt="доноры ооцитов">
					</div>
					<div class="bg"></div>
					<div class="text">
						<p class="buttons">
							<a href="<c:out value='donor/oocyte/list'/>"
								class="btn btn-template-transparent-primary"><i
								class="fa fa-link"></i> Подробнее</a>
						</p>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-12">
			<div class="box-image-text blog">
				<div class="heading">
					<h3>Суррогатные матери</h3>
				</div>
				<div class="top">
					<div class="imagecenter">
						<img src="<c:url value="/resources/img/donor_sur.png"/>"
							width="240" alt="суррогатные матери">
					</div>
					<div class="bg"></div>
					<div class="text">
						<p class="buttons">
							<a href="<c:out value='donor/surmom/list'/>"
								class="btn btn-template-transparent-primary"><i
								class="fa fa-link"></i> Подробнее</a>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="content"
		style="margin-bottom: 50pt; text-align: center; margin-top: 50pt">
		<div class="heading">
			<h2>Криобанк в цифрах${swat}</h2>
		</div>
		<section class="bar background-pentagon no-mb">
			<div class="container">
				<div class="row showcase">
					<div class="col-md-4 col-sm-6">
						<div class="item">
							<score>
								<a href="<c:out value='donor/sperm/list'/>"> <span
									class="counter">${counts.spermCount}</span></a><br> Доноры
								спермы
							</score>
						</div>
					</div>
					<div class="col-md-4 col-sm-6">
						<div class="item">
							<score>
								<a href="<c:out value='donor/oocyte/list'/>"> <span
									class="counter">${counts.oocyteCount}</span></a><br> Доноры
								ооцитов
							</score>
						</div>
					</div>
					<div class="col-md-4 col-sm-12">
						<div class="item">
							<score>
								<a href="<c:out value='donor/surmom/list'/>"> <span
									class="counter">${counts.surmomCount}</span></a><br>
								Суррогатные матери
							</score>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
	<div class="content"
		style="margin-bottom: 50pt; text-align: center; margin-top: 30pt">
		<div class="col-md-12" role="tabpanel"
			style="margin-bottom: 50pt; text-align: center; margin-top: 20pt">
			<div class="col-xs-4 col-sm-12 ">
				<!-- Nav tabs -->
				<ul class="[ nav nav-justified ]" id="nav-tabs" role="tablist">
					<li role="presentation" class=""><a
						href="<c:out value='form/sperm'/>"> <img class="img-circle"
							src="<c:url value="/resources/img/donor_sperm.png"/>" width="180"
							alt="доноры спермы"> <span class="quote"><i
								class="fa fa-quote-left"></i></span>
					</a>
						<div class="heading">
							<h4>
								Как стать <br>донором спермы
							</h4>
						</div></li>
					<li role="presentation" class=""><a
						href="<c:out value='form/oocyt'/>"> <img class="img-circle"
							src="<c:url value="/resources/img/donor_oocyt.png"/>" width="180"
							alt="доноры ооцитов"> <span class="quote"><i
								class="fa fa-quote-left"></i></span>
					</a>
						<div class="heading">
							<h4>
								Как стать <br>донором ооцитов
							</h4>
						</div></li>
					<li role="presentation" class=""><a
						href="<c:out value='form/surmom'/>"> <img class="img-circle"
							src="<c:url value="/resources/img/donor_sur.png"/>" width="180"
							alt="суррогатные матери"> <span class="quote"><i
								class="fa fa-quote-left"></i></span>
					</a>
						<div class="heading">
							<h4>
								Как стать <br>суррогатной матерью
							</h4>
						</div></li>
				</ul>
			</div>
		</div>
	</div>
	<script
		src="//cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
	<script
		src="http://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
	<script src="<c:url value="/resources/js/jquery.counterup.min.js"/>"></script>
	<script src="<c:url value="/resources/js/owl.carousel.min.js"/>"></script>
	<script>
		jQuery(document).ready(function($) {
			$('.counter').counterUp({
				delay : 10,
				time : 1000
			});
		});
	</script>
	<script>
		$(document).ready(function() {

			$(".player").mb_YTPlayer();

		});
	</script>
</div>
