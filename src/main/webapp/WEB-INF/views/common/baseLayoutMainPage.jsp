<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" pageEncoding="utf-8"
	contentType="text/html; charset=UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<title>${title}</title>
	<tiles:insertAttribute name="meta" />
<link
	href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,500,700,800'
	rel='stylesheet' type='text/css'>
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<link href="<c:url value="/resources/css/animate.css"/>"
	rel="stylesheet">
<link href="<c:url value="/resources/css/bootstrap.css"/>"
	rel="stylesheet">
<link href="<c:url value="/resources/css/custom.css"/>" rel="stylesheet">
<link href="<c:url value="/resources/css/style.default.css"/>"
	rel="stylesheet">
<link href="<c:url value="/resources/css/login.form.css"/>"
	rel="stylesheet">
<link href="<c:url value="/resources/css/owl.carousel.css"/>"
	rel="stylesheet">
<link href="<c:url value="/resources/css/owl.theme.css"/>"
	rel="stylesheet">
<script src="<c:url value="/resources/js/jquery-1.11.0.min.js"/>"></script>
<script src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>
<script
	src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>
<script src="<c:url value="/resources/js/gmaps.js"/>"></script>

	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
			new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
			j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
			'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-N65NCRM');</script>
	<!-- End Google Tag Manager -->
</head>
<body>
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N65NCRM"
				  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<div id="wrapper">
		<div id="top">
			<tiles:insertAttribute name="top" />
		</div>
		<div id="header">
			<tiles:insertAttribute name="header" />
		</div>
		
		<c:if test="${not empty breadcrumb}">
		<div id="heading-breadcrumbs">
			<div class="container">
				<ul class="breadcrumb">
					<li><a href="<c:url value="/" />">Криобанк</a>
					</li>
					<li>${breadcrumb.name}</li>
				</ul>
			</div>
		</div>
		</c:if>
		<div class="row" style="margin-bottom: 10pt; margin-top: 0pt">
			<tiles:insertAttribute name="body" />
		</div>

		<div id="footer">
			<tiles:insertAttribute name="footer" />
		</div>

	</div>
</body>
</html>