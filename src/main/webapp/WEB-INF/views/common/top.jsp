<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8" %>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="container">
    <div class="row">
        <div class="col-xs-5 hidden-sm">
            <a target="_blank" href="http://irm.kz">Клиника ИРМ</a>
            <a target="_blank" href="http://forum.irm.kz">Форум ИРМ</a>
            <a target="_blank" href="https://www.youtube.com/channel/UCAbe0COSUw7D0h2BR8ggvXQ">Вебинары</a>
        </div>
        <div class="col-xs-7">
            <sec:authorize access="isAnonymous()">
                <div class="login">
                    <a href="<c:url value="/login/loginpage" />"> <i
                            class="fa fa-sign-in"></i> <span class="hidden-xs text-uppercase">Войти</span></a>
                    <a href="<c:url value="/registration/signup" />"><i
                            class="fa fa-user"></i> <span class="hidden-xs text-uppercase">Регистрация</span></a>
                    <sec:authorize access="isAuthenticated()">
                        <a href="<c:url value="/login/logout" />"><span
                                class="hidden-xs text-uppercase">Log off</span></a>
                    </sec:authorize>
                </div>
            </sec:authorize>
            <sec:authorize access="isAuthenticated()">
                <div class="login">
                    <sec:authorize access="hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER')">
                        <a href="<c:url value="/admin/user/list" />"><span
                                class="glyphicon glyphicon-th-list"></span></a>
                    </sec:authorize>
                    <sec:authorize access="hasAnyRole('ROLE_ADMIN', 'ROLE_LAB')">
                        <a href="<c:url value="/order/list" />"><span
                                class="fa fa-plus-square"></span></a>
                    </sec:authorize>
                    <a href="<c:url value="/profile/user/view" />"><span
                            class="glyphicon glyphicon-user"></span></a> <strong> <sec:authentication
                        property="principal.username"/> <span>|</span>
                </strong> <a href="<c:url value="/login/logout" />"><span
                        class="hidden-xs text-uppercase">Выход</span></a>
                </div>
            </sec:authorize>
        </div>
    </div>
</div>