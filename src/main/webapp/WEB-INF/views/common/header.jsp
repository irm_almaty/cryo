<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=utf8"
	pageEncoding="utf8"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<div class="navbar-affixed-top" data-spy="affix" data-offset-top="200">
	<div class="navbar navbar-default yamm" role="navigation" id="navbar">
		<div class="container">
			<div class="navbar-header">
				<div class="logo_irm"
					style="margin-bottom: 3px; margin-top: 3px; margin-left: 0px">
					<a href="<c:url value="/" />"> <img
						src="<c:url value="/resources/img/logo.png"/>" width="75"
						alt="cryobank.kz" class="hidden-xs hidden-sm">
					</a>
				</div>
				<div class="navbar-buttons">
					<button type="button" class="navbar-toggle btn-template-main"
						data-toggle="collapse" data-target="#navigation">
						<span class="sr-only">Toggle navigation</span> <i
							class="fa fa-align-justify"></i>
					</button>
				</div>
			</div>
			<div class="navbar-collapse collapse" id="navigation">

				<c:set var = "first" scope = "session" value = "${menu.menuMap['1']}"/>
				<c:set var = "second" scope = "session" value = "${menu.menuMap['2']}"/>
				<c:set var = "third" scope = "session" value = "${menu.menuMap['3']}"/>
				<c:set var = "fourth" scope = "session" value = "${menu.menuMap['4']}"/>

				<ul class="nav navbar-nav navbar-right">
					<li class="${first ? "active" : "dropdown-toggle"}">
						<a href="<c:url value="/" />">Главная</a></li>
					<li class="${second ? "active" : "dropdown-toggle"}"><a href="javascript: void(0)"
						class="dropdown-toggle" data-toggle="dropdown">Информация <b
							class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><a target="_blank" href="<c:url value="/resources/pdf/624rus.pdf" />">
									Правила проведения донорства <br>и хранения половых клеток
							</a></li>
						</ul></li>
					<li class="${third ? "active dropdown use-yamm yamm-fw" : "dropdown use-yamm yamm-fw"}"><a href="#"
						class="dropdown-toggle" data-toggle="dropdown">Криобанк<b
							class="caret"></b></a>
						<ul class="dropdown-menu">
							<li>
								<div class="yamm-content">
									<div class="row">
										<div class="col-sm-1"></div>
										<div class="col-sm-3">
											<div class="imageheader">
												<a href="<c:url value="/donor/sperm/list"/>"> <img
													src="<c:url value="/resources/img/donor_sperm.png"/>"
													width="100" alt="доноры спермы" class="img-responsive">
												</a>
											</div>
											<ul>
												<li><a href="<c:url value="/form/sperm" />"> Cтать
														донором спермы</a></li>
												<li><a href="<c:url value="/donor/sperm/list"/>"><strong>Доноры
															спермы</strong></a></li>
											</ul>
										</div>
										<div class="col-sm-3">
											<div class="imageheader">
												<a href="<c:url value="/donor/oocyte/list"/>"> <img
													src="<c:url value="/resources/img/donor_oocyt.png"/>"
													width="100" alt="доноры ооцитов" class="img-responsive">
												</a>
											</div>
											<ul>
												<li><a href="<c:url value="/form/oocyt" />"> Cтать
														донором ооцитов</a></li>
												<li><a href="<c:url value="/donor/oocyte/list"/>"><strong>Доноры
															ооцитов</strong></a></li>
											</ul>
										</div>
										<div class="col-sm-3">
											<div class="imageheader">
												<a href="<c:url value="/donor/surmom/list"/>"> <img
													src="<c:url value="/resources/img/donor_sur.png"/>"
													width="100" alt="суррогатные матери" class="img-responsive">
												</a>
											</div>
											<ul>
												<li><a href="<c:url value="/form/surmom" />"> Cтать
														сур. мамой</a></li>
												<li><a href="<c:url value="/donor/surmom/list"/>"><strong>Суррогатные
															матери</strong></a></li>
											</ul>
										</div>
									</div>
								</div>
							</li>
						</ul></li>
					<li class="${fourth ? "active" : "dropdown-toggle"}">
						<a href="<c:url value="/contact" />">Контакты</a></li>
					<sec:authorize access="hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER')">
						<li class="dropdown"><a href="javascript: void(0)"
							class="dropdown-toggle" data-toggle="dropdown">Менеджер<b
								class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="<c:url value="/admin/user/list" />">Пользователи</a></li>
								<li><a href="<c:url value="/orders/list" />">Все заявки</a></li>
							</ul></li>
					</sec:authorize>

					<sec:authorize access="hasAnyRole('ROLE_ADMIN', 'ROLE_LAB')">
						<li class="dropdown"><a href="javascript: void(0)"
							class="dropdown-toggle" data-toggle="dropdown">Лаборатория<b
								class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="<c:url value="/orders/list" />">Все заявки</a></li>
							</ul></li>
					</sec:authorize>

					<sec:authorize access="hasAnyRole('ROLE_ADMIN')">
						<li class="dropdown"><a href="javascript: void(0)"
							class="dropdown-toggle" data-toggle="dropdown">Админ<b
								class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="<c:url value="/admin/user/list" />">Пользователи</a></li>
								<li><a href="<c:url value="/orders/list" />">Все заявки</a></li>
							</ul></li>
					</sec:authorize>
				</ul>
			</div>
			<div class="collapse clearfix" id="search">
				<form class="navbar-form" role="search">
					<div class="input-group">
						<input type="text" class="form-control" placeholder="Search">
						<span class="input-group-btn">
							<button type="submit" class="btn btn-template-main">
								<i class="fa fa-search"></i>
							</button>
						</span>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>