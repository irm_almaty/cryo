<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8" %>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<c:forEach items="${metaDataList}" var="meta">
    <meta name="${meta.metaDataName.value}" content="${meta.content}">
</c:forEach>

<meta property="og:locale" content="ru_RU" />
<meta property="og:type" content="website" />
<meta property="og:title" content="Криобанк генетического материала клиники ИРМ в Алматы" />
<meta property="og:description" content="Криобанк от клиники ИРМ в Казахстане ✔Хранение донорского биоматериала ✔Доноры яйцеклетки и спермы, суррогатное материнство в Алматы" />
<meta property="og:url" content="http://cryobank.kz/"/>
<meta property="og:site_name" content="Криобанк ИРМ" />
<meta property="og:image" content="http://cryobank.kz/resources/img/logo.png" />

<meta name="twitter:card" content="summary" />
<meta name="twitter:description" content="Криобанк от клиники ИРМ в Казахстане ✔Хранение донорского биоматериала ✔Доноры яйцеклетки и спермы, суррогатное материнство в Алматы" />
<meta name="twitter:title" content="Криобанк генетического материала клиники ИРМ в Алматы" />

<meta name="google-site-verification" content="9yl8ebbHCVaKnej8bNxV_Mf9i1_VqMJZyUGCUEov-jE"/>
<meta name="yandex-verification" content="90878699b4885c7c"/>
<c:if test="${not empty canonical}">
    <link rel="canonical" href="http://${pageContext.request.serverName}${canonical}"/>
</c:if>
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Organization",
  "url": "http://cryobank.kz",
  "logo": "http://cryobank.kz/resources/img/logo.png",
   "contactPoint": [{
    "@type": "ContactPoint",
    "telephone": "+7 (727) 234-34-34",
    "contactType": "customer service"
  },
  { "@type": "ContactPoint",
    "telephone": "+7 (771) 934-34-34",
    "contactType": "customer service"
  }]
}

</script>