<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<footer id="footer">
	<div class="container">
		<div class="col-md-3 col-sm-6">
			<h4>О нас</h4>
			<p>Криобанк</p>
			<hr>
			<h4>Подписаться на новости</h4>
			<form:form id="SubscriptForm" cssClass="form-horizontal"
				modelAttribute="subscript" method="post" action="saveSubscript">
				<div class="input-group">
					<form:input type="email" placeholder="Введите ваш email"
						cssClass="form-control" path="email" value="${subscript.email}" />
					<span class="input-group-btn">
						<button class="btn btn-default" type="submit">
							<i class="fa fa-send"></i>
						</button>
					</span>
				</div>
			</form:form>
			<hr class="hidden-md hidden-lg hidden-sm">
		</div>
		<div class="col-md-1 col-sm-6"></div>
		<div class="col-md-3 col-sm-6 ">
			<h4>Полезное</h4>
			<div class="blog-entries">
				<div class="item same-height-row clearfix">
					<div class="name same-height-always">
						<h5>
							<a href="#">Как стать донором</a>
						</h5>
					</div>
				</div>
				<div class="item same-height-row clearfix">
					<div class="name same-height-always">
						<h5>
							<a href="#">Все новости</a>
						</h5>
					</div>
				</div>
				<div class="item same-height-row clearfix">
					<div class="name same-height-always">
						<h5>
							<a href="#">Все статьи</a>
						</h5>
					</div>
				</div>
			</div>
			<hr class="hidden-md hidden-lg">
		</div>
		<div class="col-md-2 col-sm-6"></div>
		<div class="col-md-3 col-sm-6 ">
			<h4>Контакты</h4>
			<p>
				<strong>Алматы</strong> <br>Толе би 99 </br> <strong>Казахстан</strong>
			</p>
			<a href="<c:url value="/mail" />"
				class="btn btn-small btn-template-main">Обратная связь</a>
			<hr class="hidden-md hidden-lg hidden-sm">
		</div>
	</div>
</footer>