<%@ page language="java" contentType="text/html; charset=utf8"
	pageEncoding="utf8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="container">
	<div class="col-md-12  col-sm-12" >
		<div class="col-md-3  col-sm-12">
			<h4>&copy; 2017.Криобанк</h4>
			<p>Все права защищены</p>
			<hr class="hidden-md hidden-lg hidden-sm">
		</div>
		<div class="col-md-4  col-sm-12">
			<div class="footer-item">
			<h4>Доноры</h4>

						<p>
							<a href="<c:out value='/form/sperm'/>">Как стать донором
								спермы</a>
						</p>

						<p>
							<a href="<c:out value='/form/oocyt'/>">Как стать донором
								ооцитов</a>
							</p>

						<p>
							<a href="<c:out value='/form/surmom'/>">Как стать суррогатной
								матерью</a>

			</div>
		</div>
		<div class="col-md-4  col-sm-12">
			<h4>Контакты</h4>
			<p>
				<strong>г.Алматы</strong> <br>ул.Толе би, 99 <br> <strong>Казахстан</strong>
			</p>
			<a href="<c:url value="/feedback/form" />">
				<div class="btn btn-small btn-template-main">Обратная связь</div>
			</a>
			<hr class="hidden-md hidden-lg hidden-sm">
		</div>

		<div class="col-md-1  col-sm-12" align="right">
			<!-- ZERO.kz -->
		<span id="_zero_67949">
			<noscript>
				<a href="http://zero.kz/?s=67949" target="_blank"> <img
						src="http://c.zero.kz/z.png?u=67949" width="88" height="31"
						alt="ZERO.kz" >
				</a>
			</noscript>
		</span>
			<script type="text/javascript">
				<!--
				var _zero_kz_ = _zero_kz_ || [];
				_zero_kz_.push([ "id", 67949 ]);
				_zero_kz_.push([ "type", 1 ]);

				(function() {
					var a = document.getElementsByTagName("script")[0], s = document
							.createElement("script");
					s.type = "text/javascript";
					s.async = true;
					s.src = (document.location.protocol == "https:" ? "https:"
									: "http:")
							+ "//c.zero.kz/z.js";
					a.parentNode.insertBefore(s, a);
				})();
				//-->
			</script>
			<!-- End ZERO.kz -->
		</div>

	</div>
</div>