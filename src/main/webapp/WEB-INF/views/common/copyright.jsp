<%@ page language="java" contentType="text/html; charset=utf8"
	pageEncoding="utf8"%>
<div class="container">
	<div class="col-md-12">
		<p class="pull-left">&copy; 2017. Криобанк</p>
		<p class="pull-right">
			Все права защищены. <a target="_blank" href="http://irm.kz">irm.kz</a>
		</p>
	</div>
	<div class="pull-right">
		<!-- ZERO.kz -->
		<span id="_zero_67949">
			<noscript>
				<a href="http://zero.kz/?s=67949" target="_blank"> <img
					src="http://c.zero.kz/z.png?u=67949" width="88" height="31"
					alt="ZERO.kz" >
				</a>
			</noscript>
		</span>
		<script type="text/javascript">
		<!--
			var _zero_kz_ = _zero_kz_ || [];
			_zero_kz_.push([ "id", 67949 ]);
			_zero_kz_.push([ "type", 1 ]);

			(function() {
				var a = document.getElementsByTagName("script")[0], s = document
						.createElement("script");
				s.type = "text/javascript";
				s.async = true;
				s.src = (document.location.protocol == "https:" ? "https:"
						: "http:")
						+ "//c.zero.kz/z.js";
				a.parentNode.insertBefore(s, a);
			})();
		//-->
		</script>
		<!-- End ZERO.kz -->
	</div>
</div>
