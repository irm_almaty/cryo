<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" pageEncoding="utf-8"
	contentType="text/html; charset=UTF-8"%>

<html>
<head>
<%-- <tiles:insertAttribute name="title" ignore="true" /> --%>
	<title>${title}</title>
    <tiles:insertAttribute name="meta" />
<link
	href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,500,700,800'
	rel='stylesheet' type='text/css'>

<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

<link href="<c:url value="/resources/css/animate.css"/>"
	rel="stylesheet">
<link href="<c:url value="/resources/css/bootstrap.css"/>"
	rel="stylesheet">
<link href="<c:url value="/resources/css/custom.css"/>" rel="stylesheet">

<link href="<c:url value="/resources/css/style.default.css"/>"
	rel="stylesheet">

<link href="<c:url value="/resources/css/login.form.css"/>"
	rel="stylesheet">

<link href="<c:url value="/resources/css/owl.carousel.css"/>"
	rel="stylesheet">
<link href="<c:url value="/resources/css/owl.theme.css"/>"
	rel="stylesheet">
<script src="<c:url value="/resources/js/jquery-1.11.0.min.js"/>"></script>
<script src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>

</head>

<body>

	<div id="wrapper">
		<div id="top">
			<tiles:insertAttribute name="top" />
		</div>
		<div id="header">
			<tiles:insertAttribute name="header" />
		</div>


		<div id="heading-breadcrumbs">
			<div class="container">
				<ul class="breadcrumb">
					<li><a href="<c:url value="/" />">Криобанк</a>
					</li>
					<li>${breadcrumb.name}</li>
				</ul>
			</div>
		</div>

		<div id="wrapper">
			<tiles:insertAttribute name="body" />
		</div>

		<div id="footer">
			<tiles:insertAttribute name="footer" />
		</div>

	</div>

</body>
</html>