<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" pageEncoding="utf-8"
	contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<head>
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<script>
	$(function() {
		$("#datepicker").datepicker({
			changeMonth : true,
			changeYear : true,
			yearRange : "1960:2010"
		});
	});
	(function(factory) {
		if (typeof define === "function" && define.amd) {
			// AMD. Register as an anonymous module.
			define([ "../widgets/datepicker" ], factory);
		} else {
			// Browser globals
			factory(jQuery.datepicker);
		}
	}
			(function(datepicker) {
				datepicker.regional.ru = {
					closeText : "Закрыть",
					prevText : "&#x3C;Пред",
					nextText : "След&#x3E;",
					currentText : "Сегодня",
					monthNames : [ "Январь", "Февраль", "Март", "Апрель",
							"Май", "Июнь", "Июль", "Август", "Сентябрь",
							"Октябрь", "Ноябрь", "Декабрь" ],
					monthNamesShort : [ "Янв", "Фев", "Мар", "Апр", "Май",
							"Июн", "Июл", "Авг", "Сен", "Окт", "Ноя", "Дек" ],
					dayNames : [ "воскресенье", "понедельник", "вторник",
							"среда", "четверг", "пятница", "суббота" ],
					dayNamesShort : [ "вск", "пнд", "втр", "срд", "чтв", "птн",
							"сбт" ],
					dayNamesMin : [ "Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб" ],
					weekHeader : "Нед",
					dateFormat : "dd/mm/yy",
					firstDay : 1,
					isRTL : false,
					showMonthAfterYear : false,
					yearSuffix : ""
				};
				datepicker.setDefaults(datepicker.regional.ru);
				return datepicker.regional.ru;
			}));
</script>

</head>

<style>
fieldset {
	border: thin solid #ccc;
	border-radius: 1px;
	padding: 10px;
	padding-left: 10px;
	background: #fbfbfb;
}

legend {
	color: #678;
}

.form-control {
	width: 95%;
}

label small {
	color: #678 !important;
}

span.req {
	color: maroon;
	font-size: 112%;
}
</style>
<body>
	<div class="heading">
		<h2>Регистрация</h2>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<c:if test="${result.hasErrors()}">
					<div class="alert alert-danger alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert">
							<span aria-hidden="true">×</span><span class="sr-only">Close</span>
						</button>
						Проверьте правильность заполнения данных
					</div>
				</c:if>
				<legend class="left">Все поля обяательны к заполнению <span class="req"><small>
					*</small></span></legend>
				<form:form method="post" modelAttribute="patient" action="save">
					<fieldset>

						<div class="col-md-12">
							<div class="col-md-4">
								<div class="form-group">
									<label for="firstname"><span class="req">* </span>
										Фамилия: </label>
									<form:input path="lastname" class="form-control" type="text" />
									<div id="errFirst">
										<span class="req"> <form:errors path="lastname"
												cssClass="error" /></span>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label for="firstname"><span class="req">* </span> Имя:
									</label>
									<form:input path="firstname" class="form-control" type="text" />
									<div id="errFirst">
										<span class="req"> <form:errors path="firstname"
												cssClass="error" /></span>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label for="firstname"><span class="req">* </span>
										Отчество: </label>
									<form:input path="patroname" class="form-control" type="text" />
									<div id="errFirst">
										<span class="req"> <form:errors path="lastname"
												cssClass="error" />
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-4">
								<div class="form-group">
									<label for="country"><span class="req">* </span>
										Страна: </label>
									<form:input path="country" class="form-control" type="text" />
									<div id="errFirst">
										<span class="req"> <form:errors path="country"
												cssClass="error" />
										</span>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label for="country"><span class="req">* </span> Город:
									</label>
									<form:input path="city" class="form-control" type="text" />
									<div id="errFirst">
										<span class="req"> <form:errors path="city"
												cssClass="error" />
										</span>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label for="country"><span class="req">* </span> Дата рождения:
									</label>
									<form:input path="birthday" id="datepicker"
										class="form-control" />
									<div id="errFirst">
										<span class="req"> <form:errors path="city"
												cssClass="error" />
										</span>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<div class="form-group">
									<label for="firstname"><span class="req">* </span>
										E-mail: </label>
									<form:input path="email" class="form-control" type="email" />
									<div id="errFirst">
										<span class="req"> <form:errors path="email"
												cssClass="error" /></span>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<div class="form-group">
									<label for="phone"><span class="req">* </span> Телефон:
									</label>
									<form:input path="phone" class="form-control" type="text" />
									<div id="errFirst">
										<span class="req"> <form:errors path="phone"
												cssClass="error" /></span>
									</div>
								</div>
							</div>
						</div>

						<div class="col-md-12">
							<div class="col-md-6">
								<div class="form-group">
									<label for="access"><span class="req">* </span> Доступ
										к разделу: </label>
									<form:select path="access" class="form-control">
										<form:options items="${list}" />
									</form:select>
								</div>
							</div>
						</div>

						<div class="col-md-12">
							<div class="form-group">
								<hr>
								<input type="checkbox" required name="terms"
									onchange="this.setCustomValidity(validity.valueMissing ? 'Please indicate that you accept the Terms and Conditions' : '');"
									id="field_terms">   <label for="terms">Я
									согласен с <a href="terms.php" data-toggle="modal"
									data-target="#myModal">условиями</a> для регистрации.
								</label><span class="req">* </span>
							</div>
							<div class="form-group">
								<input class="btn btn-submit" type="submit"
									value="Регистрация">
							</div>
						</div>
					</fieldset>
				</form:form>
				<!-- ends register form -->
			</div>
			<!-- ends col-6 -->
		</div>
	</div>


	<div class="row">

		<!-- Modal -->
		<div class="modal fade" id="myModal" tabindex="1" role="dialog"
			aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog" style="width: 900px;">

				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">
							<span aria-hidden="true">×</span><span class="sr-only">Close</span>
						</button>
						<h4 class="modal-title" id="myModalLabel">
							<i class="fa fa-file-text-o"></i> Соглашение
						</h4>
					</div>
					<div class="modal-body">


						<p class="lead">
							Проходя регистрацию на сайте cryobank (в дальнейшем «мы», «наш»,
							«Сайт CRYOBANK », «http://www.cryobank.kz»), </br>Вы подтверждаете
							своё согласие со следующими условиями. </br>Мы оставляем за собой
							право изменять эти правила в любое время и сделаем всё возможное,
							чтобы уведомить вас об этом, однако с вашей стороны было бы
							разумным регулярно просматривать этот текст на предмет изменений,
							так как использование Сайта CRYOBANK после обновления/исправления
							условий означает ваше согласие с ними. </br>Как пользователь Вы
							согласны с тем, что введённая вами информация будет храниться в
							базе данных. Эта информация не будет открыта третьим лицам без
							вашего разрешения, но мы не можем быть ответственны за действия
							хакеров, которые могут привести к несанкционированному доступу к
							ней.
						</p>

					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
					</div>
				</div>
			</div>

		</div>
	</div>





</body>
