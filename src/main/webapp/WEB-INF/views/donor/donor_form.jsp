<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://ckeditor.com" prefix="ckeditor"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Donor form</title>
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

</head>
<body>


	<a href="<c:url value="/donor/allMaleDonors" />"><div
			class="heading">
			<h3>Редактирование донора:</h3>


		</div></a>
	<h4>Донор: ${donor.donorId}</h4>


	<div class="panel-body">
		<form:form id="donorRegisterForm" cssClass="form-horizontal"
			modelAttribute="donor" method="post" action="saveDonor"
			enctype="multipart/form-data">


			<div class="form-group">
				<div class="control-label col-xs-2">
					<form:label path="donorId">ID донора:</form:label>
				</div>
				<div class="col-md-6">
					<form:input cssClass="form-control" path="donorId"
						value="${donor.donorId}" />
				</div>
			</div>


			<div class="form-group">
				<div class="control-label col-xs-2">
					<form:label path="donorName">Загрузить фото:</form:label>
				</div>
				<div class="col-md-6">
					<tr>
						<td><input type="file" name="fileUpload" size="50" /></td>
					</tr>
				</div>

			</div>

			<div class="form-group">
				<div class="col-md-6">
					<c:forEach items="${listPhoto}" var="photo">
						<div class="col-md-3">
							<p>Фото: ${photo.photoId}</p>
							<img src="<c:url value="${photo.photoPath}"/>" height="95"
								height="" alt="Universal logo" class="hidden-xs hidden-sm">
							<a
								href="<c:out value='deletePhoto?id=${photo.photoId}&donorId=${donor.donorId}'/>">
								<span class="glyphicon glyphicon-remove"> </span>Удалить
							</a>
						</div>
					</c:forEach>
				</div>
			</div>


			<div class="form-group">

				<div class="control-label col-xs-2">
					<form:label path="donorName">Загрузить голос:</form:label>
				</div>
				<div class="col-md-6">
					<tr>
						<td><input type="file" name="fileUploadVoice" size="50" /></td>
					</tr>
				</div>

			</div>

			<div class="form-group">
				<div class="col-md-6">
					<c:forEach items="${listVoice}" var="voice">
						<div class="col-md-5">
							<audio controls> <source
								src="<c:url value="${voice.voicePath}"/>" type="audio/mpeg">
							Your browser does not support the audio element. </audio>

							<a href="<c:out value='deleteVoice?id=${voice.voiceId}'/>"> <span
								class="glyphicon glyphicon-remove"> </span>Удалить
							</a>
						</div>
					</c:forEach>
				</div>
			</div>


			<div class="row">
				<input type="submit" class="btn btn-primary" value="Сохранить" />
			</div>

		</form:form>
</body>
</html>