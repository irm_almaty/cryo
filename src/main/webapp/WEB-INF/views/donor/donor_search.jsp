<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="joda" uri="http://www.joda.org/joda/time/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<script>
	$(function() {
		$("#genderTypeInt").selectmenu();

		$("#nationIdInt").selectmenu();

		$("#bloodIdInt").selectmenu();

		$("#eyesIdInt").selectmenu();

		$("#number").selectmenu().selectmenu("menuWidget").addClass("overflow");
	});
</script>


<style>
.ui-accordion .ui-accordion-header {
	display: block;
	cursor: pointer;
	position: relative;
	margin: 2px 0 0 0;
	padding: .em .5em .5em .7em;
	min-height: 0; /* support: IE7 */
	font-size: 130%;
}
</style>


<style>
fieldset {
	border: 0;
}

label {
	display: block;
	margin: 30px 0 0 0;
}

select {
	width: 200px;
}

.overflow {
	height: 200px;
}
</style>


<title>Donor Search</title>
</head>
<body>

	<h2>Поиск донора</h2>

	<form:form id="newsRegisterForm" cssClass="form-horizontal"
		method="post" action="searchDonorAction">

		<div class="row">
			<div class="col-md-12">



				<div class="col-md-3">
					<select name="genderTypeInt" id="genderTypeInt">
						<option value="">Тип донора</option>
						<option value="1">донор спермы</option>
						<option value="2">донор ооцитов</option>

					</select>
				</div>




				<div class="col-md-3">
					<select name="nationIdInt" id="nationIdInt">
						<option value="1">казах</option>
						<option value="2">русский</option>
						<option value="3">украинец</option>
						<option value="4">киргиз</option>
						<option value="5">китаец</option>
						<option value="6">немец</option>
					</select>
				</div>


				<div class="col-md-3">
					<select name="bloodIdInt" id="bloodIdInt">
						<option value="">группа крови</option>
						<option value="1">O(I) Rh-</option>
						<option value="2">O(I) Rh+</option>
						<option value="3">A(II) Rh-</option>
						<option value="4">A(II) Rh+</option>

						<option value="5">B(III) Rh−</option>
						<option value="6">B(III) Rh+</option>

						<option value="7">AB(IV) Rh-</option>
						<option value="8">AB(IV) Rh+</option>

					</select>
				</div>


				<div class="col-md-3">
					<select name="eyesIdInt" id="eyesIdInt">
						<option value="">цвет глаз</option>
						<option value="1">синий</option>
						<option value="2">голубой</option>
						<option value="3">серый</option>
						<option value="4">зеленый</option>
						<option value="5">янтарный</option>
						<option value="6">болотный</option>
						<option value="7">карий</option>
						<option value="8">черный</option>
						<option value="9">желтый</option>
						<option value="10">фиолетовый</option>

					</select>

				</div>


			</div>
			<div class="col-md-12  col-xs-offset-10"
				style="margin-bottom: 10pt; margin-top: 10pt";>

				<div class="col-md-1">
					<input class="btn btn-success" type='submit' value='Поиск' />
				</div>
			</div>

		</div>
	</form:form>




	<h3>${intCountSearch}</h3>

	<table class="table table-hover">
		<thead>
			<tr>
				<th>Имя донора</th>
				<th>Пол донора</th>
				<th>Группа крови</th>
				<th>Цвет глаз</th>
				<th>Национальность</th>


			</tr>
		</thead>
		<c:forEach items="${searchList}" var="donor">
			<tr>
				<td><a href="<c:out value='viewDonor?id=${donor.donorId}'/>">${donor.donorName}
						<span class="glyphicon glyphicon-eye-open"> </span>
				</a></td>
				<td>${donor.getGender().getGenderType()}</td>
				<td>${donor.getBloodType().getBloodType()}</td>
				<td>${donor.getEyes().getEyesColor()}</td>
				<td>${donor.getNation().getNationName()}</td>


			</tr>
		</c:forEach>
	</table>





</body>
</html>