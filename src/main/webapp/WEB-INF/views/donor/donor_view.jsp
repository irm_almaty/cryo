<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://ckeditor.com" prefix="ckeditor"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Donor view</title>
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<link href="<c:url value="/resources/css/lightbox.css"/>"
	rel="stylesheet">

<style>
button.accordion {
	background-color: #eee;
	color: #444;
	cursor: pointer;
	padding: 18px;
	width: 100%;
	border: none;
	text-align: left;
	outline: none;
	font-size: 15px;
	transition: 0.0s;
	outline: none;
}

button.accordion.active, button.accordion:hover {
	background-color: #70BDFF;
}

div.panel {
	padding: 0 10px;
	background-color: white;
	max-height: 0;
	overflow: hidden;
	transition: 0.1s ease-in-out;
	opacity: 0;
}

div.panel.show {
	opacity: 1;
	max-height: 5000px;
}

.btn-lg {
	padding: 18px 16px;
	font-size: 16px;
	line-height: 1.33;
	border-radius: 0;
}

.imagecenterlist {
	margin-top: -20px;
	margin-left: 0px;
	margin-bottom: 20px;
}
</style>


</head>

<div class="heading">
	<h3>Просмотр донора:</h3>
</div>

<body>

	<div class="col-md-3" style="margin-bottom: 10pt;">

		<div class="imagecenterlist">
			<a href="<c:url value="/donor/sperm/list"/>"> <img
				src="<c:url value="/resources/img/donor_sperm.png"/>" width="100"
				class="img-responsive">
			</a>
		</div>

		<h4>Код донора: ${donor.donorCode}</h4>
		<p>
			нация: <strong>${donor.nation}</strong>
		</p>

		<c:if test="${donor.rod!=null}">
			<p>
				род: <strong>${donor.rod}</strong>
			</p>
		</c:if>

		<p>
			знак зодиака:<strong> ${donor.zodiak}</strong>
		</p>

		<p>
			группа крови: <strong>${donor.gruppaKrovi}</strong>
		</p>
		<p>
			резус фактор: <strong>${donor.rezusFaktor}</strong>
		</p>
		
		
		<a href="order/?id=${donor.donorManId}">
			<div class="btn btn-small btn-template-main"
				style="margin-top: 10pt;">Зарезервировать</div>
		</a>
	</div>

	<div class="col-md-9 " style="margin-bottom: 10pt; margin-top: 10pt";>
		<button type="button" class="btn btn-lg btn-info" style="width: 100%">
			Информация о доноре</button>
		<div class="panel">
			<div class="col-md-10" style="margin-bottom: 10pt; margin-top: 10pt";>
				<p>
					Национальность: <strong>${donor.nation}</strong>
				</p>

				<c:if test="${donor.rod!=null }">
					<p>
						Род: <strong>${donor.rod}</strong>
					</p>
				</c:if>

				<p>
					Знак зодиака:<strong> ${donor.zodiak}</strong>
				</p>

				<p>
					Группа крови: <strong>${donor.gruppaKrovi}</strong>
				</p>
				<p>
					Резус фактор: <strong>${donor.rezusFaktor}</strong>
				</p>
				<p>
					Образование: <strong>${donor.obrazovanie}</strong>
				</p>
				<p>
					Профессия: <strong>${donor.profession}</strong>
				</p>
				<p>
					О доноре: <strong>${donor.infoDonor}</strong>
				</p>
			</div>
		</div>

		<button type="button" class="btn btn-lg btn-info" style="width: 100%">
			Фенотипические данные:</button>
		<div class="panel">
			<div class="col-md-10" style="margin-bottom: 10pt; margin-top: 10pt";>
				<p>
					Рост: <strong>${donor.rost}</strong>
				</p>
				<p>
					Вес: <strong>${donor.ves}</strong>
				</p>
				<p>
					Размер обуви: <strong>${donor.razmerObuvi}</strong>
				</p>
				<p>
					Размер одежды: <strong>${donor.razmerOdejdi}</strong>
				</p>
				<p>
					Цвет кожи: <strong>${donor.cvetKoji}</strong>
				</p>
				<p>
					Волосы: <strong>${donor.tipVolos}</strong>
				</p>
				<p>
					Цвет волос: <strong>${donor.cvetVolos}</strong>
				</p>
				<p>
					Форма лица: <strong>${donor.lico}</strong>
				</p>
				<p>
					Лоб: <strong>${donor.lob}</strong>
				</p>
				<p>
					Разрез глаз: <strong>${donor.razrezGlaz}</strong>
				</p>
				<p>
					Цвет глаз: <strong>${donor.cvetGlaz}</strong>
				</p>
				<p>
					Скулы: <strong>${donor.skuli}</strong>
				</p>
				<p>
					Форма носа: <strong>${donor.nos}</strong>
				</p>
				<p>
					Форма губ: <strong>${donor.gubi}</strong>
				</p>
			</div>
		</div>




		<button type="button" class="btn btn-lg btn-info" style="width: 100%">
			Опрос донора:</button>
		<div class="panel">
			<div class="col-md-10" style="margin-bottom: 10pt; margin-top: 10pt";>


				<p>
					Состояние зрения: <strong>${donor.sostZreniya}</strong>
				</p>

				<p>
					Ношение очков: <strong>${donor.noshenieOchkov}</strong>
				</p>

				<p>
					Правша/Левша: <strong>${donor.glavRuka}</strong>
				</p>

				<p>
					Служба в армии: <strong>${donor.sluzhbaArmiya}</strong>
				</p>

				<p>
					Кто вы по натуре?: <strong>${donor.ktoPoNature}</strong>
				</p>

				<p>
					Охарактеризовать себя: <strong>${donor.slovaOSebe}</strong>
				</p>

				<p>
					Сильные стороны характера: <strong>${donor.harakterSilnii}</strong>
				</p>

				<p>
					Слабые стороны характера: <strong>${donor.harakterSlabii}</strong>
				</p>

				<p>
					Каким спортом занимались в детстве?: <strong>${donor.sportDetstvo}</strong>
				</p>

				<p>
					Хобби: <strong>${donor.hobbi}</strong>
				</p>

				<p>
					Любимый цвет: <strong>${donor.lubimCvet}</strong>
				</p>

				<p>
					Играете ли вы на музыкальных инструментах?: <strong>${donor.igraMuz}</strong>
				</p>
				<p>
					Любимое животное: <strong>${donor.lubimAnimal}</strong>
				</p>
				<p>
					Любимое блюдо: <strong>${donor.lubimBlyudo}</strong>
				</p>
				<p>
					Любимое направление в музыке: <strong>${donor.lubimNapravMuz}</strong>
				</p>

				<p>
					Ценности в жизни: <strong>${donor.cennostiLive}</strong>
				</p>
				<p>
					Аллергоанамнез: <strong>${donor.allergiya}</strong>
				</p>

			</div>
		</div>



		<button type="button" class="btn btn-lg btn-info" style="width: 100%">
			Фамильное дерево:</button>
		<div class="panel">

			<div class="p" style="margin: 0 0 5px;">

				<div class="col-md-12"
					style="margin-bottom: 10pt; margin-top: 10pt; font-size: 11px";>

					<c:if test="${familyTreeMother!=null}">
						<div class="col-md-4">
							<h4>Мать</h4>
							<div class="thumbnail">
								<div class="caption">
									<p>
										Возраст: <strong>${familyTreeMother.age}</strong>
									</p>
									<p>
										Жива?: <strong>${familyTreeMother.alive}</strong>
									</p>
									<p>
										Нац-cть: <strong>${familyTreeMother.nation}</strong>
									</p>
									<p>
										Образование: <strong>${familyTreeMother.obrazovanie}</strong>
									</p>
									<p>
										Профессия: <strong>${familyTreeMother.profession}</strong>
									</p>
									<p>
										Рост: <strong>${familyTreeMother.rost}</strong>
									</p>
									<p>
										Вес: <strong>${familyTreeMother.ves}</strong>
									</p>
									<p>
										Цв. волос: <strong>${familyTreeMother.cvetVolos}</strong>
									</p>
									<p>
										Цв. глаз: <strong>${familyTreeMother.cvetGlaz}</strong>
									</p>
									<p>
										Сост. здоровья:<strong> ${familyTreeMother.zdorovie}</strong>
									</p>
									<c:if test="${familyTreeMother.prichSmerti!=null}">
										<p>
											Причина смерти:<strong>
												${familyTreeMother.prichSmerti}</strong>
										</p>
									</c:if>

								</div>
							</div>
						</div>
					</c:if>

					<c:if test="${motherGrandma!=null}">
						<div class="col-md-4">
							<h4>Бабушка по матери</h4>
							<div class="thumbnail">
								<div class="caption">
									<p>
										Возраст: <strong>${motherGrandma.age}</strong>
									</p>
									<p>
										Жива?: <strong>${motherGrandma.alive}</strong>
									</p>
									<p>
										Нац-cть: <strong>${motherGrandma.nation}</strong>
									</p>
									<p>
										Образование: <strong>${motherGrandma.obrazovanie}</strong>
									</p>
									<p>
										Профессия: <strong>${motherGrandma.profession}</strong>
									</p>
									<p>
										Рост: <strong>${motherGrandma.rost}</strong>
									</p>
									<p>
										Вес: <strong>${motherGrandma.ves}</strong>
									</p>
									<p>
										Цв. волос: <strong>${motherGrandma.cvetVolos}</strong>
									</p>
									<p>
										Цв. глаз: <strong>${motherGrandma.cvetGlaz}</strong>
									</p>
									<p>
										Сост. здоровья:<strong> ${motherGrandma.zdorovie}</strong>


									</p>

									<c:if test="${motherGrandma.prichSmerti!=null}">
										<p>
											Причина смерти:<strong> ${motherGrandma.prichSmerti}</strong>
										</p>
									</c:if>
								</div>
							</div>
						</div>
					</c:if>

					<c:if test="${motherGrandma!=null}">
						<div class="col-md-4">
							<h4>Дедушка по матери</h4>
							<div class="thumbnail">
								<div class="caption">
									<p>
										Возраст: <strong>${motherGrandpa.age}</strong>
									</p>
									<p>
										Жив?: <strong>${motherGrandpa.alive}</strong>
									</p>
									<p>
										Нац-cть: <strong>${motherGrandpa.nation}</strong>
									</p>
									<p>
										Образование: <strong>${motherGrandpa.obrazovanie}</strong>
									</p>
									<p>
										Профессия: <strong>${motherGrandpa.profession}</strong>
									</p>
									<p>
										Рост: <strong>${motherGrandpa.rost}</strong>
									</p>
									<p>
										Вес: <strong>${motherGrandpa.ves}</strong>
									</p>
									<p>
										Цв. волос: <strong>${motherGrandpa.cvetVolos}</strong>
									</p>
									<p>
										Цв. глаз: <strong>${motherGrandpa.cvetGlaz}</strong>
									</p>
									<p>
										Сост. здоровья:<strong> ${motherGrandpa.zdorovie}</strong>
									</p>

									<c:if test="${motherGrandpa.prichSmerti!=null}">
										<p>
											Причина смерти:<strong> ${motherGrandpa.prichSmerti}</strong>
										</p>
									</c:if>

								</div>
							</div>
						</div>
					</c:if>
				</div>


				<div class="col-md-12"
					style="margin-bottom: 10pt; margin-top: 10pt; font-size: 11px";>

					<c:if test="${father!=null}">
						<div class="col-md-4">
							<h4>Отец</h4>
							<div class="thumbnail">
								<div class="caption">
									<p>
										Возраст: <strong>${father.age}</strong>
									</p>
									<p>
										Жив?: <strong>${father.alive}</strong>
									</p>
									<p>
										Нац-cть: <strong>${father.nation}</strong>
									</p>
									<p>
										Образование: <strong>${father.obrazovanie}</strong>
									</p>
									<p>
										Профессия: <strong>${father.profession}</strong>
									</p>
									<p>
										Рост: <strong>${father.rost}</strong>
									</p>
									<p>
										Вес: <strong>${father.ves}</strong>
									</p>
									<p>
										Цв. волос: <strong>${father.cvetVolos}</strong>
									</p>
									<p>
										Цв. глаз: <strong>${father.cvetGlaz}</strong>
									</p>
									<p>
										Сост. здоровья:<strong> ${father.zdorovie}</strong>
									</p>

									<c:if test="${father.prichSmerti!=null}">
										<p>
											Причина смерти:<strong> ${father.prichSmerti}</strong>
										</p>
									</c:if>
								</div>
							</div>
						</div>
					</c:if>

					<c:if test="${fatherGrandma!=null}">
						<div class="col-md-4">
							<h4>Бабушка по отцу</h4>
							<div class="thumbnail">
								<div class="caption">
									<div class="caption">
										<p>
											Возраст: <strong>${fatherGrandma.age}</strong>
										</p>
										<p>
											Жива?: <strong>${fatherGrandma.alive}</strong>
										</p>
										<p>
											Нац-cть: <strong>${fatherGrandma.nation}</strong>
										</p>
										<p>
											Образование: <strong>${fatherGrandma.obrazovanie}</strong>
										</p>
										<p>
											Профессия: <strong>${fatherGrandma.profession}</strong>
										</p>
										<p>
											Рост: <strong>${fatherGrandma.rost}</strong>
										</p>
										<p>
											Вес: <strong>${fatherGrandma.ves}</strong>
										</p>
										<p>
											Цв. волос: <strong>${fatherGrandma.cvetVolos}</strong>
										</p>
										<p>
											Цв. глаз: <strong>${fatherGrandma.cvetGlaz}</strong>
										</p>
										<p>
											Сост. здоровья:<strong> ${fatherGrandma.zdorovie}</strong>
										</p>

										<c:if test="${fatherGrandma.prichSmerti!=null}">
											<p>
												Причина смерти:<strong>
													${fatherGrandma.prichSmerti}</strong>
											</p>
										</c:if>
									</div>
								</div>
							</div>
						</div>
					</c:if>

					<c:if test="${fatherGrandpa!=null}">
						<div class="col-md-4">
							<h4>Дедушка по отцу</h4>
							<div class="thumbnail">
								<div class="caption">
									<p>
										Возраст: <strong>${fatherGrandpa.age}</strong>
									</p>
									<p>
										Жив?: <strong>${fatherGrandpa.alive}</strong>
									</p>
									<p>
										Нац-cть: <strong>${fatherGrandpa.nation}</strong>
									</p>
									<p>
										Образование: <strong>${fatherGrandpa.obrazovanie}</strong>
									</p>
									<p>
										Профессия: <strong>${fatherGrandpa.profession}</strong>
									</p>
									<p>
										Рост: <strong>${fatherGrandpa.rost}</strong>
									</p>
									<p>
										Вес: <strong>${fatherGrandpa.ves}</strong>
									</p>
									<p>
										Цв. волос: <strong>${fatherGrandpa.cvetVolos}</strong>
									</p>
									<p>
										Цв. глаз: <strong>${fatherGrandpa.cvetGlaz}</strong>
									</p>
									<p>
										Сост. здоровья:<strong> ${fatherGrandpa.zdorovie}</strong>
									</p>
									<c:if test="${fatherGrandpa.prichSmerti!=null}">
										<p>
											Причина смерти:<strong> ${fatherGrandpa.prichSmerti}</strong>
										</p>
									</c:if>
								</div>
							</div>
						</div>
					</c:if>
				</div>

				<div class="col-md-12"
					style="margin-bottom: 10pt; margin-top: 10pt; font-size: 11px";>

					<c:if test="${brother!=null}">
						<div class="col-md-4">
							<h4>Брат</h4>
							<div class="thumbnail">
								<div class="caption">
									<p>
										Родной/Сводный: <strong>${brother.tidRodstva}</strong>
									</p>

									<p>
										Возраст: <strong>${brother.age}</strong>
									</p>
									<p>
										Жив?: <strong>${brother.alive}</strong>
									</p>

									<p>
										Образование: <strong>${brother.obrazovanie}</strong>
									</p>
									<p>
										Профессия: <strong>${brother.profession}</strong>
									</p>
									<p>
										Рост: <strong>${brother.rost}</strong>
									</p>
									<p>
										Вес: <strong>${brother.ves}</strong>
									</p>
									<p>
										Цв. волос: <strong>${brother.cvetVolos}</strong>
									</p>
									<p>
										Цв. глаз: <strong>${brother.cvetGlaz}</strong>
									</p>
									<p>
										Сост. здоровья:<strong> ${brother.zdorovie}</strong>
									</p>

									<c:if test="${brother.prichSmerti!=null}">
										<p>
											Причина смерти:<strong> ${brother.prichSmerti}</strong>
										</p>
									</c:if>

									<p>
								</div>
							</div>
						</div>
					</c:if>

					<c:if test="${sister!=null}">
						<div class="col-md-4">
							<h4>Сестра</h4>
							<div class="thumbnail">
								<div class="caption">
									<p>
										Родной/Сводный: <strong>${sister.tidRodstva}</strong>
									</p>

									<p>
										Возраст: <strong>${sister.age}</strong>
									</p>
									<p>
										Жива?: <strong>${sister.alive}</strong>
									</p>
									<p>
										Образование: <strong>${sister.obrazovanie}</strong>
									</p>
									<p>
										Профессия: <strong>${sister.profession}</strong>
									</p>
									<p>
										Рост: <strong>${sister.rost}</strong>
									</p>
									<p>
										Вес: <strong>${sister.ves}</strong>
									</p>
									<p>
										Цв. волос: <strong>${sister.cvetVolos}</strong>
									</p>
									<p>
										Цв. глаз: <strong>${sister.cvetGlaz}</strong>
									</p>
									<p>
										Сост. здоровья:<strong> ${sister.zdorovie}</strong>
									</p>
									<c:if test="${sister.prichSmerti!=null}">
										<p>
											Причина смерти:<strong> ${sister.prichSmerti}</strong>
										</p>
									</c:if>
									<p>
								</div>
							</div>
						</div>
					</c:if>
				</div>
			</div>
		</div>

		<button type="button" class="btn btn-lg btn-info" style="width: 100%">
			Детские фотографии:</button>
		<div class="panel">
			<div class="col-md-10" style="margin-bottom: 10pt; margin-top: 10pt";>
				<c:forEach items="${list3}" var="photo">
					<div>
						<div class="col-md-3" style="margin-bottom: 5pt; margin-top: 5pt";>
							<a class="example-image-link"
								href="<c:url value="${photo.photoPath}"/>"
								data-lightbox="example-set" data-title="${photo.photoId}"><img
								class="example-image" src="<c:url value="${photo.photoPath}"/> "
								height="100" alt="" /></a>
						</div>
					</div>
				</c:forEach>
			</div>
		</div>

		<script>
			var acc = document.getElementsByClassName("btn btn-lg btn-info");
			var i;

			acc[0].onclick = function() {
				this.classList.toggle("active");
				this.nextElementSibling.classList.toggle("show");
			}

			for (i = 0; i < acc.length; i++) {

				acc[i].onclick = function() {
					this.classList.toggle("active");
					this.nextElementSibling.classList.toggle("show");
				}
			}
		</script>

	</div>
</body>

<script>
	lightbox.option({
		'resizeDuration' : 2000,
		'wrapAround' : true
	})
</script>

<script src="<c:url value="/resources/js/lightbox-plus-jquery.js"/>"></script>

</html>