<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/css/bootstrap-select.min.css">

<script
	src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/js/bootstrap-select.min.js"></script>

<style>
.col-md-12 {
	width: 100%;
}
</style>
<style>
.btn {
	text-align: left;
}

button.accordion {
	background-color: #eee;
	color: #444;
	cursor: pointer;
	padding: 18px;
	width: 100%;
	border: none;
	text-align: right;
	outline: none;
	font-size: 15px;
	transition: 0.0s;
	outline: none;
}

button.accordion.active, button.accordion:hover {
	background-color: #1daae0;
}

div.panel {
	padding: 0 10px;
	background-color: white;
	max-height: 0;
	overflow: hidden;
	transition: 0.1s ease-in-out;
	opacity: 0;
}

div.panel.show {
	opacity: 1;
	max-height: 5000px;
}

.btn-lg {
	padding: 10px 10px;
	font-size: 12px;
	line-height: 1.33;
	border-radius: 0;
}

.panel {
	margin-bottom: 0px;
}

.modal-content {
	-webkit-box-shadow: 0 5px 15px rgba(0, 0, 0, 0);
	-moz-box-shadow: 0 5px 15px rgba(0, 0, 0, 0); -0
	-box-shadow: 0 5px 15px rgba(0, 0, 0, 0);
	box-shadow: 0 5px 15px rgba(0, 0, 0, 0);
}

.imagecenterlist {
	display: table;
	margin: 0 auto;
	margin-top: -25px;
	margin-bottom: 10px;
}
</style>

<div class="col-md-12" style="margin-top: 10pt">
	<div class="col-md-2" style="margin-top: 0pt">
		<div class="heading" style="margin-top: 0pt;text-align: center;">
			<h1 class="donor">${msg}</h1>
		</div>
		<div class="imagecenterlist">
			<a href="<c:url value="/donor/sperm/list"/>"> <img
				src="<c:url value="/resources/img/donor_sperm.png"/>" width="100"
				class="img-responsive">
			</a>
		</div>
		<p class="donor">Всего доноров: ${count}</p>
		${searchInfo}
		<div class="box" S>
			<div class="box-header">
				<h2 class="donor">Поиск</h2>
			</div>
		</div>
		<form:form method="post" modelAttribute="search_man" action="search">

			<sec:authorize access="hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER')">
				<div class="col-xs-6 col-sm-12 ">
					<h4>ЭМК донора:</h4>
					<div class="input-group">
						<form:input path="donorId" class="form-control"
							placeholder="Введите ЭМК донора" />
					</div>
				</div>
				<div class="col-xs-6 col-sm-12 ">
					<h4>Код донора:</h4>
					<div class="input-group">
						<form:input path="donorCode" class="form-control"
							placeholder="Введите код донора" />
					</div>
				</div>
			</sec:authorize>

			<div class="col-xs-6 col-sm-12 ">
				<h3 class="donor">Национальность:</h3>
				<form:select class="selectpicker" data-live-search="true"
					path="nation" data-width="200px">
					<form:option value="" label="Не выбрано" />
					<form:options items="${nations}" />
				</form:select>
			</div>
			<div class="col-xs-6 col-sm-12 ">
				<h3 class="donor">Цвет глаз:</h3>
				<form:select path="eyesColor" class="selectpicker"
					data-live-search="true" data-width="200px">
					<form:option value="" label="Не выбрано" />
					<form:options items="${eyes}" />
				</form:select>
			</div>
			<div class="col-xs-6 col-sm-12 ">
				<h3 class="donor">Цвет волос:</h3>
				<form:select path="hairColor" class="selectpicker"
					data-live-search="true" data-width="200px">
					<form:option value="" label="Не выбрано" />
					<form:options items="${hairs}" />
				</form:select>
			</div>
			<div class="col-xs-6 col-sm-12 " style="margin-bottom: 20pt">
				<h3 class="donor">Группа крови:</h3>
				<form:select path="bloodType" class="selectpicker"
					data-live-search="true" data-width="200px">
					<form:option value="" label="Не выбрано" />
					<form:options items="${blood}" />
				</form:select>
			</div>
			<div class="col-md-12">
				<button class="btn btn-default btn-sm btn-template-main">
					<i class="fa fa-search"></i>Поиск
				</button>
				<button class="btn btn-default btn-sm btn-template-main">
						<a href="<c:url value="/donor/sperm/search/refresh"/>" rel="nofollow">
						<i class="fa fa-refresh"></i>Сброс
						</a>
				</button>
			</div>
			<div class="col-xs-12" style="margin-top: 10pt"></div>
		</form:form>
	</div>
	<div class="col-md-10" style="margin-top: 0pt">
		<div class="col-md-12">
		<div class="col-md-9">
			<c:if test="${showPag==true}">
				<ul class="pagination">
					<li><a href="<c:out value='list?page=${pag.previousPage}'/>">«</a></li>
					<c:forEach items="${pags}" var="pags">
						<li class="${pags.buttoneDescription}">
							<c:choose>
								<c:when test="${pags.pageNumber==1 && not empty canonical}">
									<a href="<c:out value='${canonical}'/>">${pags.pageNumber}</a>
								</c:when>
								<c:otherwise>
									<a href="<c:out value='?page=${pags.pageNumber}'/>">${pags.pageNumber}</a>
								</c:otherwise>
							</c:choose>
						</li>
					</c:forEach>
					<li><a href="<c:out value='list?page=${pag.nextPage}'/>">»</a></li>
				</ul>
			</c:if>
		</div>

		<div class="col-md-3">
		<form:form method="post" modelAttribute="search_man" action="search">
				<form role="search">
					<div class="input-group">
						<form:input path="donorCode" class="form-control"
							placeholder="Введите код донора" />
						<span class="input-group-btn">
							<button type="submit" class="btn btn-template-main">
								<i class="fa fa-search"></i>
							</button>
						</span>
					</div>
				</form>
		</form:form>
	  </div>
		</div>
		<div class="col-md-12" style="margin-top: 10pt">
			<c:forEach items="${donorList}" var="donor">
				<div class="col-md-3" style="padding-bottom: 0px">
					<div class="thumbnail">
						<div class="caption">
							<p class="donor">Код: ${donor.donorCode}</p>
							<sec:authorize
								access="hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER','ROLE_LAB')">
								<h4>ЭМК Медиалог: ${donor.donorManId}</h4>
								<h4>Имя: ${donor.donorName}</h4>
							</sec:authorize>
							<sec:authorize
								access="hasAnyRole('ROLE_ADMIN','ROLE_MANAGER', 'ROLE_SPERM','ROLE_SPERM_OOCYT','ROLE_LAB')">
								<p ng-controller="donorController">
									<a href="<c:out value='view?id=${donor.donorManId}'/>">
										просмотр <span class="glyphicon glyphicon-eye-open"> </span>
									</a>
								</p>
							</sec:authorize>

							<p ng-controller="donorController">
								<sec:authorize access="isAnonymous()">
									<a href="<c:out value='view?id=${donor.donorManId}'/>">
										полный просмотр <span class="glyphicon glyphicon-eye-open">
									</span>
									</a>
								</sec:authorize>
							</p>
							<p>
								национальность: <br> <strong>${donor.nation}</strong>
							</p>
							<p>
								цвет волос: <strong>${donor.cvetVolos}</strong>
							</p>
							<p>
								цвет глаз: <strong>${donor.cvetGlaz}</strong>
							</p>
							<p>
								гр.крови:<strong> ${donor.gruppaKrovi}</strong>
							</p>
							<p>
								резус фактор: <strong>${donor.rezusFaktor}</strong>
							</p>

							<sec:authorize access="hasRole('ROLE_ADMIN')">
								<p ng-controller="donorController">
									<a
										href="<c:url value='/admin/editDonor?id=${donor.donorManId}'/>">
										<span class="glyphicon glyphicon-edit"> </span> Изменить
									</a>
								</p>
							</sec:authorize>
						</div>
					</div>
				</div>
			</c:forEach>
		</div>
		<div class="col-md-10" style="margin-top: 10pt">
			<c:if test="${showPag==true}">
				<ul class="pagination">
					<li><a href="<c:out value='list?page=${pag.previousPage}'/>">«</a></li>
					<c:forEach items="${pags}" var="pags">
						<li class="${pags.buttoneDescription}">
							<c:choose>
								<c:when test="${pags.pageNumber==1 && not empty canonical}">
									<a href="<c:out value='${canonical}'/>">${pags.pageNumber}</a>
								</c:when>
								<c:otherwise>
									<a href="<c:out value='?page=${pags.pageNumber}'/>">${pags.pageNumber}</a>
								</c:otherwise>
							</c:choose>
						</li>
					</c:forEach>
					<li><a href="<c:out value='list?page=${pag.nextPage}'/>">»</a></li>
				</ul>
			</c:if>
		</div>
	</div>
</div>
<div class="col-md-12" style="margin-top: 40pt"></div>
<div class="container" style="margin-top: 0pt"></div>
<script>
	var acc = document.getElementsByClassName("btn btn-lg btn-info");
	var i;

	acc[0].onclick = function() {
		this.classList.toggle("active");
		this.nextElementSibling.classList.toggle("show");
	}

	for (i = 0; i < acc.length; i++) {

		acc[i].onclick = function() {
			this.classList.toggle("active");
			this.nextElementSibling.classList.toggle("show");
		}
	}
</script>
<script>
	lightbox.option({
		'resizeDuration' : 2000,
		'wrapAround' : true
	})
</script>

<script src="<c:url value="/resources/js/lightbox-plus-jquery.js"/>"></script>
<script src="<c:url value="/resources/js/jquery.cookie.js"/>"></script>
<script src="<c:url value="/resources/js/owl.carousel.min.js"/>"></script>
<script src="<c:url value="/resources/js/front.js"/>"></script>