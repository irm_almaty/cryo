<%@ page language="java" contentType="text/html; charset=utf8"
	pageEncoding="utf8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Вход</title>
</head>

<body>


	<form:form name='loginForm' id="loginForm"
		action="../j_spring_security_check" method='POST'>

		<div class="row">


			<div class="col-md-5 col-md-offset-3">
				<div class="panel panel-default">
					<div class="panel-heading">
						<span class="glyphicon glyphicon-lock"></span> Вход${er}
					</div>
					<div class="panel-body">
						<form class="form-horizontal" role="form">
							<div class="form-group">
								<label for="inputEmail3" class="col-sm-3 control-label">
									Логин</label>
								<div class="col-sm-9">
									<input class="form-control" id="j_username" name="j_username"
										type="text" placeholder="Введите логин" size="30"
										autocomplete="on" autofocus="autofocus">

								</div>
							</div>
							<div class="form-group">
								<label for="inputPassword3" class="col-sm-3 control-label">
									Пароль</label>
								<div class="col-sm-9">
									<input class="form-control" id="j_password" name="j_password"
										type="password" placeholder="Введите пароль" size="30"
										autocomplete="on">


								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-offset-3 col-sm-9">
									<div class="checkbox">
										<label> <input type="checkbox" /> Запомнить меня
										</label>
									</div>
								</div>
							</div>
							<div class="form-group last">
								<div class="col-sm-offset-3 col-sm-9">
									<button id="subbtn" type="submit"
										class="btn btn-success btn-sm">Войти</button>
									<button type="reset" class="btn btn-default btn-sm">
										Сброс</button>
								</div>
							</div>
						</form>
					</div>
					<div class="panel-footer">
						ИРМ Криобанк <a href="<c:url value="/registration/signup" />">регистрация<i
							class="fa fa-user"></i></a>
						
					</div>
				</div>
			</div>
		</div>
	</form:form>
</body>

















</html>